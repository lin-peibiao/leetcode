package cache;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xiaohu
 * @date 2023/06/19/ 14:26
 * @description
 */
public class LRUCache {
    // 双向链表节点定义
    class DLinkedNode {
        int key;
        int value;
        DLinkedNode prev;
        DLinkedNode next;
    }

    // 缓存容量
    private int capacity;
    // 当前缓存大小
    private int size;
    // 链表头部表示最近使用的数据项，链表尾部表示最久未使用的数据项
    private DLinkedNode head, tail;
    // 哈希表用来快速查找数据项在链表中的位置
    private Map<Integer, DLinkedNode> cache = new HashMap<>();

    public LRUCache(int capacity) {
        this.capacity = capacity;
        size = 0;
        head = new DLinkedNode();
        tail = new DLinkedNode();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        // 查询哈希表，判断是否存在该数据项
        DLinkedNode node = cache.get(key);
        if (node == null) {
            return -1;
        }
        // 将查询到的数据项移动到链表头部
        moveToHead(node);
        return node.value;
    }

    public void put(int key, int value) {
        // 查询哈希表，判断是否存在该数据项
        DLinkedNode node = cache.get(key);
        if (node == null) {
            // 哈希表中不存在，需要插入新数据项
            DLinkedNode newNode = new DLinkedNode();
            newNode.key = key;
            newNode.value = value;
            // 将新数据项插入到链表头部
            addToHead(newNode);
            cache.put(key, newNode);
            // 如果缓存已满，则淘汰链表尾部的数据项
            size++;
            if (size > capacity) {
                DLinkedNode tail = removeTail();
                cache.remove(tail.key);
                size--;
            }
        } else {
            // 哈希表中已存在该数据项，需要更新其值，并将其移动到链表头部
            node.value = value;
            moveToHead(node);
        }
    }

    private void addToHead(DLinkedNode node) {
        node.prev = head;
        node.next = head.next;
        head.next.prev = node;
        head.next = node;
    }

    private void removeNode(DLinkedNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    private void moveToHead(DLinkedNode node) {
        removeNode(node);
        addToHead(node);
    }

    private DLinkedNode removeTail() {
        DLinkedNode node = tail.prev;
        removeNode(node);
        return node;
    }
}

