import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaohu
 * @date 2023/02/10/ 10:49
 * @description
 */
public class StudentDao {
    /**
     * 存储学生信息的类
     */
    static class Student {
        String student_id;
        String name;
        String class_id;
    }

    /**
     * 数据库连接 url、用户名、密码信息
     */
    private String url = "jdbc:mysql://localhost:3306/test";
    private String username = "test";
    private String password = "test";

    /**
     * 获取学生信息
     */
    public void getStudentInfo() {
        // 创建一个存储学生信息的列表
        List<Student> students = new ArrayList<>();
        Connection connection = null;
        try {
            // 连接数据库
            connection = DriverManager.getConnection(url, username, password);
            // 创建一个 Statement 对象
            Statement statement = connection.createStatement();
            // 执行查询语句，返回查询结果
            ResultSet resultSet = statement.executeQuery(
                    "SELECT student_id, name, class_id FROM Student");
            // 处理查询结果
            while (resultSet.next()) {
                // 创建一个存储学生信息的对象
                Student student = new Student();
                student.student_id = resultSet.getString("student_id");
                student.name = resultSet.getString("name");
                student.class_id = resultSet.getString("class_id");
                // 将该学生信息添加到列表中
                students.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // 输出查询到的学生信息列表
        System.out.println("xuehao\txingming\tbanji");
        for (Student student : students) {
            System.out.println(student.student_id + " " + student.name + " " + student.class_id);
        }
    }
}
