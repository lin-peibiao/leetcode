package inter;

/**
 * @author xiaohu
 * @date 2023/04/24/ 18:27
 * @description
 */

public class ThreadTest{
    private static volatile int count = 0;
    private static final Object lock = new Object();

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            while (count <= 100) {
                synchronized (lock) {
                    if (count % 3 == 0) {
                        System.out.println(Thread.currentThread().getName() + ": " + count++);
                    }
                }
            }
        }, "Thread-1");

        Thread thread2 = new Thread(() -> {
            while (count <= 100) {
                synchronized (lock) {
                    if (count % 3 == 1) {
                        System.out.println(Thread.currentThread().getName() + ": " + count++);
                    }
                }
            }
        }, "Thread-2");

        Thread thread3 = new Thread(() -> {
            while (count <= 100) {
                synchronized (lock) {
                    if (count % 3 == 2) {
                        System.out.println(Thread.currentThread().getName() + ": " + count++);
                    }
                }
            }
        }, "Thread-3");

        thread1.start();
        thread2.start();
        thread3.start();
    }
}

