package designmode.strategy;

/**
 * @author xiaohu
 * @date 2023/03/10/ 12:23
 * @description
 */
public class User {
    LoginBehavior loginBehavior;
    public User(){

    }

    public void login(LoginVo loginVo){
        loginBehavior.login(loginVo);
    }

    public void setLoginBehavior(LoginBehavior loginBehavior){
        this.loginBehavior = loginBehavior;
    }
}
