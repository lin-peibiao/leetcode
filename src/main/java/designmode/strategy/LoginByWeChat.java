package designmode.strategy;

/**
 * @author xiaohu
 * @date 2023/03/10/ 12:22
 * @description
 */
public class LoginByWeChat implements LoginBehavior{
    @Override
    public void login(LoginVo loginVo) {
        // 通过微信登录
        // 判断登录逻辑等
    }
}
