package designmode.strategy;

/**
 * @author xiaohu
 * @date 2023/03/10/ 12:14
 * @description 登录行为接口
 */
public interface LoginBehavior {
    /**
     * 登录
     */
    void login(LoginVo loginVo);
}
