package designmode.strategy;

/**
 * @author xiaohu
 * @date 2023/03/10/ 12:28
 * @description
 */
public class Test {
    public static void main(String[] args) {
        User user = new PhoneUser();
        user.login(new LoginVo());
    }
}
