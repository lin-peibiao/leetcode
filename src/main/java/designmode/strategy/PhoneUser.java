package designmode.strategy;

/**
 * @author xiaohu
 * @date 2023/03/10/ 12:24
 * @description
 */
public class PhoneUser extends User{
    public PhoneUser(){
        this.loginBehavior = new LoginByPhone();
    }
    @Override
    public void login(LoginVo loginVo){
        loginBehavior.login(loginVo);
    }
}
