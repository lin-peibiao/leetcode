package designmode.strategy;

/**
 * @author xiaohu
 * @date 2023/03/10/ 12:17
 * @description
 */
public class LoginByPhone implements LoginBehavior{
    @Override
    public void login(LoginVo loginVo) {
        // 通过手机号登录
        // 接收手机、验证码等参数
        // 判断登录逻辑等
    }
}
