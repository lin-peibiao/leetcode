package byCategory.binarytree;

/**
 * @author xiaohu
 * @date 2022/08/22/ 23:38
 * @description 617、合并二叉树
 */
public class MergeTrees_617 {
    // 分析好情况就简单了  毕竟是简单题目
    public TreeNode mergeTrees(TreeNode root1, TreeNode root2){
        if (root1 == null && root2 == null)
            return null;
        if (root1 != null && root2 != null){
            TreeNode root = new TreeNode(root1.val + root2.val);
            root.left = mergeTrees(root1.left, root2.left);
            root.right = mergeTrees(root1.right, root2.right);
            return root;
        }
        return root1 == null ? root2 : root1;
    }
}
