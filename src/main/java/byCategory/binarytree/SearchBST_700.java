package byCategory.binarytree;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author xiaohu
 * @date 2022/08/23/ 16:56
 * @description 二叉搜索树
 * 700、二分查找 98、判断是都为合法的二叉搜索树
 * 530、求任意两节点之间的最小差
 * 501、求二叉搜索树的众数，就是可以等于的
 */
public class SearchBST_700 {
    public TreeNode searchBST(TreeNode root, int val){
        if (root == null) return null;
        if (root.val == val) return root;
        if (root.val > val)
            return searchBST(root.left, val);
        else
            return searchBST(root.right, val);
    }

    // 不对，因为只能判断当前根和一代字数是否合法
    public boolean isValidBST(TreeNode root){
        if (root == null) return true;
        if (root.left != null ){
            if (root.val < root.left.val)
                return false;
        }

        if (root.right != null) {
            if (root.val > root.right.val)
                return false;
        }

        return isValidBST(root.left) && isValidBST(root.right);
    }

    // 线性判断
    public boolean isValidBST1(TreeNode root, long lower, long upper){
        if (root == null) return true;
        if (root.val >= upper || root.val <= lower) return false;

        return isValidBST1(root.left, lower, root.val) && isValidBST1(root.right, root.val, upper);
    }

    // 530 我直接将二叉搜索树换成一个有序数组
    public int getMinimumDifference(TreeNode root){
        if (root == null) return 0;
        List<Integer> array = new ArrayList<>();
        toArray1(root, array);
        int res = Integer.MAX_VALUE;
        for (int i = 0; i < array.size()-1; i++) {
            res = Math.min(res, array.get(i+1) - array.get(i));
        }

        return res;
    }

    private void toArray1(TreeNode root, List<Integer> array){
        if (root == null) return;
        // 中序遍历
        array.add(root.val);
        toArray1(root.left, array);
        toArray1(root.right, array);
    }

    // 使用递归的话也是利用了中序遍历的规律，就是一个有序数组嘛，都是比较相邻节点差值
    class solution{
        TreeNode pre;// 记录上一个遍历的结点
        int result = Integer.MAX_VALUE;
        public int getMinimumDifference(TreeNode root) {
            if(root == null) return 0;
            traversal(root);
            return result;
        }
        public void traversal(TreeNode root){
            if(root==null)return;
            //左
            traversal(root.left);
            //中
            if(pre != null){
                result = Math.min(result, root.val-pre.val);
            }
            pre = root;
            //右
            traversal(root.right);
        }
    }

    /**
     * 思路很简单，就是实现起来要考虑比较多的细节 可恶，花了太多时间。
     * 1、如果当前频率大于最大频率，则更新最大频率，先将集合清空再加入元素
     * 2、如果当前频率等于最大频率，添加当前元素到集合
     * 否则不添加任何元素。
     *
     * 我知道了。是因为·1我没有把逻辑写好，逻辑要搞好啊！！！再去编码。
     */
    class FindMode{

        TreeNode pre;
        int num;
        int curNum;
        public int[] findMode(TreeNode root){
            if (root == null) return null;
            List<Integer> list = new ArrayList<>();
            traversal(root, list);
            int[] res = new int[list.size()];
            for (int i = 0; i < list.size(); i++) {
                res[i] = list.get(i);
            }
            return res;
        }

        public void traversal(TreeNode root, List<Integer> list){
            if(root == null)  return;
            //左
            traversal(root.left, list);
            //计数
            if(pre == null || pre.val != root.val){
                curNum = 1;
            }else
                curNum++;

            // 更新相关参数
            if(curNum == num){
                list.add(root.val);
            }
            if (curNum > num){
                num = curNum;
                list.clear();
                list.add(root.val);
            }
            pre = root;
            //右
            traversal(root.right, list);
        }



        // 大佬题解
        ArrayList<Integer> resList;
        int maxCount;
        int count;
//        TreeNode pre;
        public int[] findMode2(TreeNode root) {
            resList = new ArrayList<>();
            maxCount = 0;
            count = 0;
            pre = null;
            findMode1(root);
            int[] res = new int[resList.size()];
            for (int i = 0; i < resList.size(); i++) {
                res[i] = resList.get(i);
            }
            return res;
        }

        public void findMode1(TreeNode root) {
            if (root == null) {
                return;
            }
            findMode1(root.left);

            int rootValue = root.val;
            // 计数
            if (pre == null || rootValue != pre.val) {
                count = 1;
            } else {
                count++;
            }
            // 更新结果以及maxCount
            if (count > maxCount) {
                resList.clear();
                resList.add(rootValue);
                maxCount = count;
            } else if (count == maxCount) {
                resList.add(rootValue);
            }
            pre = root;

            findMode1(root.right);
        }

    }



    @Test
    public void test(){
        TreeNode root = new TreeNode(1,new TreeNode(-2),new TreeNode(3));
        System.out.println(getMinimumDifference(root));
    }
}
