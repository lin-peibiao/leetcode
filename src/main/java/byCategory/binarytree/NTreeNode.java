package byCategory.binarytree;

import java.util.List;

/**
 * @author xiaohu
 * @date 2022/08/10/ 0:29
 * @description n叉树
 */
public class NTreeNode {
    public int val;
    public List<NTreeNode> children;

    public NTreeNode() {}

    public NTreeNode(int _val) {
        val = _val;
    }

    public NTreeNode(int _val, List<NTreeNode> _children) {
        val = _val;
        children = _children;
    }
}