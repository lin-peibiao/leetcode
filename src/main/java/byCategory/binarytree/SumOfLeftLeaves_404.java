package byCategory.binarytree;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author xiaohu
 * @date 2022/08/20/ 21:24
 * @description 404、返回二叉树所有左子树值的和
 */
public class SumOfLeftLeaves_404 {
    // 层次
    public int sumOfLeftLeaves(TreeNode root){
        if (root == null) return 0;
        int res = 0;
        Deque<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int curSize = queue.size();
            for (int i = 0; i < curSize; i++) {
                TreeNode node = queue.pop();
                if (node.left != null){
                    queue.offer(node.left);
                    if (node.left.left == null && node.left.right == null)
                        res += node.left.val;
                }
                if (node.right != null){
                    queue.offer(node.right);
                }
            }
        }
        return res;
    }

    // 深度递归
    public int dfs(TreeNode root){
        if (root == null) return 0;
        int cur = 0;
        if (root.left != null && root.left.left == null && root.left.right == null)
            cur = root.left.val;
        return cur + dfs(root.left) + dfs(root.right);
    }

}
