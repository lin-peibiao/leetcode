package byCategory.binarytree;

/**
 * @author xiaohu
 * @date 2022/08/20/ 22:06
 * @description 513、返回二叉树最底层最左边节点的值
 */
public class FindBottomLeftValue_513 {
    private int depth = 0;
    private int res;
    public int findBottomLeftValue(TreeNode root){
        if (root == null) return 0;
        res = root.val;
        dfs(root, 0);
        return res;
    }

    // 递归
    private void dfs(TreeNode root, int curDepth){
        if (root == null) return;
        if (root.left == null && root.right == null){
            // 这里比较的前后位置很有考究
            // res = depth < curDepth ? root.val : res;
            if (depth < curDepth){
                depth = curDepth;
                res = root.val;
            }
        }

        if (root.left != null){
            dfs(root.left, curDepth + 1);
        }
        if (root.right != null) {
            dfs(root.right, curDepth + 1);
        }
    }

    // 层次遍历呢，大概写个思路吧，就是一层一层遍历，每遍历一层，将队列第一个节点的值刷新，知道最后一层。

    // 竟然抛出异常不用捕获 才知道。
    private static void test(){
        try{
            int i = 0;
            if (i % 2 == 0)
                throw new NullPointerException();

        }finally {

        }
    }

}
