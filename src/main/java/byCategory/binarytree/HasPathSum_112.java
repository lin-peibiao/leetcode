package byCategory.binarytree;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * @author xiaohu
 * @date 2022/08/21/ 14:35
 * @description 112、判断是否有路径和等于指定数值
 */
public class HasPathSum_112 {

    public boolean hasPathSum(TreeNode root, int targetSum) {
        return dfs(root, targetSum);
    }

    // 递归回溯（是个隐式回溯）
    private boolean dfs(TreeNode root, int target){
        if(root == null) return false;
        if(root.left == null && root.right == null){
            return target == root.val;
        }

        return dfs(root.left, target - root.val) || dfs(root.right, target - root.val);
    }

    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        if (root == null)
            return new ArrayList<>();
        List<List<Integer>> res = new ArrayList<>();
        Deque<Integer> path = new LinkedList<>();
        getPath(root, targetSum, path, res);
        return res;
    }

    private void getPath(TreeNode root, int targetSum, Deque<Integer> path, List<List<Integer>> res) {
        if (root == null) return;
        path.add(root.val);
        if (root.left == null && root.right == null){
            if (targetSum == root.val)
                res.add(new ArrayList<>(path));
            return;
        }

        if (root.left != null){
            getPath(root.left, targetSum - root.val, path, res);
            // 回溯
            path.removeLast();
        }

        if (root.right != null){
            getPath(root.right, targetSum - root.val, path, res);
            // 回溯
            path.removeLast();
        }
    }

}
