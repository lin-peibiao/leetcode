package byCategory.binarytree;

import java.util.ArrayDeque;

/**
 * @author xiaohu
 * @date 2022/08/13/ 18:03
 * @description 翻转二叉树
 */
public class InvertTree_226 {

    /**
     * dfs
     * @param root
     * @return
     */
    public TreeNode invertTree(TreeNode root){
        if (root == null) return null;
        TreeNode tem = root.left;
        root.left = root.right;
        root.right = tem;
        invertTree(root.left);
        invertTree(root.right);
        return root;
    }


    /**
     * bfs
     * @param root
     * @return
     */
    public TreeNode invertTreeLevel(TreeNode root) {
        if (root == null) {return null;}
        ArrayDeque<TreeNode> deque = new ArrayDeque<>();
        deque.offer(root);
        while (!deque.isEmpty()) {
            int size = deque.size();
            while (size-- > 0) {
                TreeNode node = deque.poll();
                swap(node);
                if (node.left != null) {deque.offer(node.left);}
                if (node.right != null) {deque.offer(node.right);}
            }
        }
        return root;
    }

    private void swap(TreeNode node){
        // ····
    }
}
