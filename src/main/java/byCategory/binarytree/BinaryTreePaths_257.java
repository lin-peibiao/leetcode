package byCategory.binarytree;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaohu
 * @date 2022/08/16/ 23:14
 * @description 257、获取二叉树的所有路径
 */
public class BinaryTreePaths_257 {

    /**
     * 勉勉强强ac 回溯的话 还是得使用集合来辅助，否则不好操作
     * @param root
     * @return
     */
    private String path ="";
    public List<String> binaryTreePaths(TreeNode root) {
        if (root == null)
            return new ArrayList<>();
        ArrayList<String> res = new ArrayList<>();
        dfs(root, "", res);
        return res;
    }

    // 问题所在就是我竟然忽略了字符串的长度啊，妈的，一个长度一个字符的减那肯定是不行的，所以说回溯这东西还是得集合来干比较妥当。
    private void dfs1(TreeNode root, List<String> res){
        path += root.val + "->";
        if (root.left == null && root.right == null) {
            res.add(path.substring(0, path.length() - 2));
            return;
        }
        if (root.left != null){
            dfs1(root.left, res);
            // 回溯
            path = path.substring(0, path.length() - 3);
        }

        if (root.right != null) {
            dfs1(root.right, res);
            path = path.substring(0, path.length() - 3);
        }
    }
    @Test
    public void test(){
        TreeNode root = new TreeNode(
                37,
                new TreeNode(-34, null, new TreeNode(-100)),
                new TreeNode(-48)
        );
        List<String> list = new ArrayList<>();
        dfs1(root, list);
    }

    // 好像也没有用到回溯啊
    // 用到回溯的话，是否会快很多
    private void dfs(TreeNode root, String path, List<String> res){
        if (root == null){
            return;
        }else{
            path += root.val + "->";
            if (root.left == null && root.right == null) {
                res.add(path.substring(0, path.length() - 2));
                return;
            }
            dfs(root.left, path, res);
            dfs(root.right, path, res);
        }
    }




    /**
     * 递归法
     */
    public List<String> binaryTreePaths1(TreeNode root) {
        List<String> res = new ArrayList<>();
        if (root == null) {
            return res;
        }
        List<Integer> paths = new ArrayList<>();
        traversal1(root, paths, res);
        return res;
    }

    // 大佬的题解 回溯
    private void traversal1(TreeNode root, List<Integer> paths, List<String> res) {
        paths.add(root.val);
        // 叶子结点
        if (root.left == null && root.right == null) {
            // 输出
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < paths.size() - 1; i++) {
                sb.append(paths.get(i)).append("->");
            }
            sb.append(paths.get(paths.size() - 1));
            res.add(sb.toString());
            return;
        }
        if (root.left != null) {
            traversal1(root.left, paths, res);
            paths.remove(paths.size() - 1);// 回溯
        }
        if (root.right != null) {
            traversal1(root.right, paths, res);
            paths.remove(paths.size() - 1);// 回溯
        }
    }
}
