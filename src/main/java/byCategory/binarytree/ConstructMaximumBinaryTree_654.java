package byCategory.binarytree;

/**
 * @author xiaohu
 * @date 2022/08/22/ 23:07
 * @description 654、根据数组构建二叉树
 * 要求：最大的元素作为根节点，用最大元素左边的子数组构建左子树，最大元素右边的子数组构建右子树
 */
public class ConstructMaximumBinaryTree_654 {

    // 我直接复制大佬的题解

    /**
     * 思路：和105、106的思路差不多感觉。只要逻辑思维理清晰，其实也不难。
     * 使用左开右闭数组，以最大元素为边界分割左右子树，然后递归构建二叉树
     * @param nums
     * @return
     */
    public TreeNode constructMaximumBinaryTree(int[] nums) {
        return constructMaximumBinaryTree1(nums, 0, nums.length);
    }

    public TreeNode constructMaximumBinaryTree1(int[] nums, int leftIndex, int rightIndex) {
        if (rightIndex - leftIndex < 1) {// 没有元素了
            return null;
        }
        if (rightIndex - leftIndex == 1) {// 只有一个元素
            return new TreeNode(nums[leftIndex]);
        }
        int maxIndex = leftIndex;// 最大值所在位置
        int maxVal = nums[maxIndex];// 最大值
        for (int i = leftIndex + 1; i < rightIndex; i++) {
            if (nums[i] > maxVal){
                maxVal = nums[i];
                maxIndex = i;
            }
        }
        TreeNode root = new TreeNode(maxVal);
        // 根据maxIndex划分左右子树
        root.left = constructMaximumBinaryTree1(nums, leftIndex, maxIndex);
        root.right = constructMaximumBinaryTree1(nums, maxIndex + 1, rightIndex);
        return root;
    }
}
