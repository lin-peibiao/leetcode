package byCategory.binarytree;

import java.util.LinkedList;
import java.util.List;

/**
 * @author xiaohu
 * @date 2022/08/06/ 2:01
 * @description 二叉树前、中、后序遍历
 * 递归三个步骤：
 * 1、确定递归函数的参数和返回值
 * 2、确定终止条件
 * 3、确定单层递归的逻辑
 */
public class PreorderTraversal_144 {
    public List<Integer> preorderTraversal(TreeNode root) {
//        if (root == null) return null;
        List<Integer> res = new LinkedList<>();
        dfsPre(root,res);
        return res;
    }

    private void dfsPre(TreeNode root, List<Integer> res) {
        if (root == null) return;
        res.add(root.val);
        dfsPre(root.left, res);
        dfsPre(root.right, res);
    }

    private void dfsPost(TreeNode root, List<Integer> res){
        if (root == null) return;
        dfsPost(root.left, res);
        dfsPost(root.right, res);
        res.add(root.val);
    }

    private void dfsMid(TreeNode root, List<Integer> res){
        if (root == null) return;
        dfsMid(root.left, res);
        res.add(root.val);
        dfsMid(root.right, res);
    }
}
