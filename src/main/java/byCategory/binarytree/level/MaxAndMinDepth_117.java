package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/08/11/ 22:37
 * @description 层次遍历获取二叉树的最大最小深度
 */
public class MaxAndMinDepth_117 {
    public int minDepth(TreeNode root){
        if (root == null) {
            return 0;
        }
        int curDepth = 1;
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int currentLevelSize = queue.size();
            for (int i = 1; i <= currentLevelSize; ++i) {
                TreeNode node = queue.poll();

                //在此处判断，如果左右子树为空，直接返回depth
                if (node.left == null && node.right == null)
                    return curDepth;

                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            ++curDepth;
        }
        return curDepth;
    }

    public int maxDepth(TreeNode root) {
        if (root == null)   return 0;
        Queue<TreeNode> que = new LinkedList<>();
        que.offer(root);
        int depth = 0;
        while (!que.isEmpty())
        {
            int len = que.size();
            while (len > 0) {
                TreeNode node = que.poll();
                if (node.left != null)  que.offer(node.left);
                if (node.right != null) que.offer(node.right);
                len--;
            }
            depth++;
        }
        return depth;
    }

    // 还有递归解法，比较简单，也写过，就不在此再写一遍了。
}
