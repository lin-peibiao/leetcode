package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/08/11/ 20:33
 * @description 找到二叉树每一层的最大值
 */
public class LargestValues_515 {
    public List<Integer> largestValues(TreeNode root) {
        if (root == null) return new ArrayList<>();
        List<Integer> res = new ArrayList();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int curSize = queue.size();
            int max = queue.peek().val;
            for(int i = 0; i < curSize; i++){
                TreeNode node = queue.poll();
                max = Math.max(max, node.val);
                if (node.left != null)
                    queue.offer(node.left);
                if (node.right != null)
                    queue.offer(node.right);
            }
            res.add(max);
        }
        return res;
    }
}
