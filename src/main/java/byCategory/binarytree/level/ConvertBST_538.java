package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

/**
 * @author xiaohu
 * @date 2022/08/28/ 15:00
 * @description 538、将BST转换为累加树
 * 操他妈的 我把BST放到了层次遍历里······
 */
public class ConvertBST_538 {
    TreeNode pre = new TreeNode();
    public TreeNode convertBST(TreeNode root){
        if (root == null) return null;
        myConvert(root);
        return root;
    }

    private void myConvert(TreeNode root) {
        if (root == null) return;
        myConvert(root.right);
        root.val += pre.val;
        pre = root;
        myConvert(root.left);
    }
}
