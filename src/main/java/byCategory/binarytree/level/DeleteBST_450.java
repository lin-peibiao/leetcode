package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

/**
 * @author xiaohu
 * @date 2022/08/27/ 0:18
 * @description 删除二叉搜索树的指定节点
 */
public class DeleteBST_450 {
    public TreeNode deleteNode(TreeNode root, int key) {
        root = delete(root,key);
        DeleteBST_450 d = new DeleteBST_450();
        return root;
    }

    private TreeNode delete(TreeNode root, int key) {
        // 分别对应五种情况
        if (root == null) return null;


        if (root.val > key) {
            root.left = delete(root.left,key);
        } else if (root.val < key) {
            root.right = delete(root.right,key);
        } else {
            if (root.left == null) return root.right; // left 是null
            if (root.right == null) return root.left; // right 是null
            TreeNode tmp = root.right; // 左右都不是null
            while (tmp.left != null) {
                tmp = tmp.left;
            }
            root.val = tmp.val;
            root.right = delete(root.right,tmp.val);
        }
        // 等于，即要删除的就是根节点，或者说是叶子节点
        return root;
    }
}
