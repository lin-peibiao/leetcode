package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/08/08/ 23:10
 * @description 求二叉树每一层的值平均值
 */
public class AverageOfLevels_637 {
    public List<Double> averageOfLevels(TreeNode root) {
        List<Double> res = new LinkedList<>();
        if (root == null) return res;
        // 开始层次遍历
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            double aveOfLevel = 0.0;
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                // WOTAMASALE
                aveOfLevel += node.val;
                if (node.left != null)
                    queue.offer(node.left);
                if (node.right != null)
                    queue.offer(node.right);
            }
            res.add(aveOfLevel / size);
        }
        return res;
    }
}
