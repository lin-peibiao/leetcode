package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/08/08/ 22:55
 * @description 二叉树的右视图
 */
public class RightSideView_199 {
    public List<Integer> rightSideView(TreeNode root) {
        List<Integer> res = new LinkedList<>();
        if (root == null) return res;
        // 开始层次遍历
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                if (node.left != null)
                    queue.offer(node.left);
                if (node.right != null)
                    queue.offer(node.right);
                if (i == size - 1)
                    res.add(node.val);
            }
        }
        return res;
    }
}
