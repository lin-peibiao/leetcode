package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;
import org.junit.Test;

/**
 * @author xiaohu
 * @date 2022/08/28/ 1:34
 * @description 669、修剪二叉搜索树
 *
 */
public class TrimBST_669 {
    // 大佬的题解，思路几乎是和我一样，我不会实现。
    // 仔细理解了一会，就是从后边往前，有个回溯的过程。也就是我想实现的pre节点。
    public TreeNode trimBST(TreeNode root, int low, int high) {
        if (root == null) {
            return null;
        }
        if (root.val < low) {
            return trimBST(root.right, low, high);
        }
        if (root.val > high) {
            return trimBST(root.left, low, high);
        }
        // root在[low,high]范围内
        root.left = trimBST(root.left, low, high);
        root.right = trimBST(root.right, low, high);
        return root;
    }
}
