package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

/**
 * @author xiaohu
 * @date 2022/08/28/ 13:55
 * @description 将有序数组转换成高度平衡的BST
 */
public class SortedArrayToBST_108 {
    public TreeNode sortedArrayToBST(int[] nums){
        if (nums == null || nums.length == 0) return null;
        return  buildBST(nums, 0, nums.length - 1);
    }

    private TreeNode buildBST(int[] nums, int left, int right) {
        if (left > right) return null;
        int mid = (right - left) / 2 + left;
        TreeNode root = new TreeNode(nums[mid]);
        root.left = buildBST(nums, left, mid - 1);
        root.right = buildBST(nums, mid + 1, right);
        return root;
    }
}
