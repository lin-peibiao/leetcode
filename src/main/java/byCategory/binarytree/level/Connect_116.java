package byCategory.binarytree.level;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/08/11/ 20:58
 * @description 116、117 填充每一个节点的下一个节点指针
 * 116给出的是完美二叉树
 * 117给出的是普通的二叉树
 * 使用层次遍历时，解法是一模一样。使用性质递归有些许不同。
 */
public class Connect_116 {
    // 层次遍历
    public Node connect(Node root){
        if (root == null) return null;
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int curSize = queue.size();
            for (int i = 0; i < curSize; i++) {
                Node node = queue.poll();
                node.next = i == curSize - 1 ? null : queue.peek();
                if (node.left != null)
                    queue.offer(node.left);
                if (node.right != null)
                    queue.offer(node.right);
            }
        }
        return root;
    }

    // 利用完美二叉树以及右侧指针的性质 递归
    public Node connect1(Node root){
        if (root == null) return null;
        if (root.left != null) {
            root.left.next = root.right;
            if (root.next != null) {
                root.right.next = root.next.left;
            }
        }
        connect1(root.left);
        connect1(root.right);
        return root;
    }

    public Node connect2(Node root) {
        if(root==null) return root;
        if(root.left!=null && root.right!=null){
            root.left.next=root.right;
        }
        if(root.left!=null && root.right==null){
            root.left.next=getNext(root.next);
        }
        if(root.right!=null)
            root.right.next=getNext(root.next);
        connect2(root.right);
        connect2(root.left);
        return root;
    }

    public Node getNext(Node root){
        if(root==null) return null;
        if(root.left!=null) return root.left;
        if(root.right!=null) return root.right;
        if(root.next!=null) return getNext(root.next);
        return null;
    }
}

/**
 * 带有右侧指针的二叉树定义
 */
class Node {
    public int val;
    public Node left;
    public Node right;
    public Node next;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, Node _left, Node _right, Node _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
}
