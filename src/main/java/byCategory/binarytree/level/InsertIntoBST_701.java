package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

/**
 * @author xiaohu
 * @date 2022/08/26/ 23:27
 * @description 在二叉搜索树中插入指定数值节点
 */
public class InsertIntoBST_701 {
    public TreeNode insertIntoBST(TreeNode root, int value){
        if (root == null) return null;
        dfs(root, value);
        return root;
    }

    private void dfs(TreeNode root, int value) {
        if (root == null) return;
        // 中序遍历
        dfs(root.left, value);
        // 找到第一个比 value 大的节点
        if (root.val > value){
            TreeNode node = new TreeNode(value);
            if (root.left == null)
                root.left = node;
            else{
                dfs(root.left, value);
            }
            return;
        }
        dfs(root.right, value);
    }

    // 大佬的题解 找到空节点，然后插入。
    public TreeNode insertIntoBST1(TreeNode root, int val) {
        if (root == null) // 如果当前节点为空，也就意味着val找到了合适的位置，此时创建节点直接返回。
            return new TreeNode(val);

        if (root.val < val){
            root.right = insertIntoBST1(root.right, val); // 递归创建右子树
        }else if (root.val > val){
            root.left = insertIntoBST1(root.left, val); // 递归创建左子树
        }
        return root;
    }

    // 大佬迭代法
    public TreeNode insertIntoBST2(TreeNode root, int val) {
        if (root == null) return new TreeNode(val);
        TreeNode newRoot = root;
        TreeNode pre = root;
        while (root != null) {
            pre = root;
            if (root.val > val) {
                root = root.left;
            } else if (root.val < val) {
                root = root.right;
            }
        }
        if (pre.val > val) {
            pre.left = new TreeNode(val);
        } else {
            pre.right = new TreeNode(val);
        }

        return newRoot;
    }


}
