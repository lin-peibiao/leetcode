package byCategory.binarytree.level;

import byCategory.binarytree.NTreeNode;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/08/10/ 0:30
 * @description n叉树的层次遍历
 */
public class NLevelOrder {
    public List<List<Integer>> levelOrder(NTreeNode root) {
        List<List<Integer>> res = new LinkedList<>();
        if (root == null) return res;
        Queue<NTreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            List<Integer> level = new LinkedList<>();
            for (int i = 0; i < size; i++) {
                NTreeNode node = queue.poll();
                for (NTreeNode child : node.children) {
                    if (child != null)
                        queue.offer(child);
                }
                level.add(node.val);
            }
            res.add(level);
        }
        return res;
    }
}
