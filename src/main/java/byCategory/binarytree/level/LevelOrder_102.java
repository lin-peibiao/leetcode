package byCategory.binarytree.level;

import byCategory.binarytree.TreeNode;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/08/06/ 20:32
 * @description 二叉树的层次遍历
 */
public class LevelOrder_102 {
    public List<List<Integer>> levelOrder(TreeNode root) {

//        ThreadLocal  用于线程间的数据隔离
        List<List<Integer>> res = new LinkedList<>();
        if (root == null)
            return res;
        Queue<TreeNode> queue = new LinkedList<>();
//      Deque<TreeNode> queue = new LinkedList<>();
        // 两者有点区别。Queue貌似性能更快。
        queue.offer(root);
        while (!queue.isEmpty()){
            int size = queue.size();
            List<Integer> level = new LinkedList<>();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                if (node.left != null)
                    queue.offer(node.left);
                if (node.right != null)
                    queue.offer(node.right);
                level.add(node.val);
            }
            res.add(level);
        }
        return res;
    }
}
