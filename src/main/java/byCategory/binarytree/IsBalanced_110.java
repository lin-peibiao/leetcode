package byCategory.binarytree;

/**
 * @author xiaohu
 * @date 2022/08/16/ 23:01
 * @description 110、判断是否是平衡二叉树
 */
public class IsBalanced_110 {
    public boolean isBalanced(TreeNode root){
        return getDepth(root, 0) != -1;
    }

    private int getDepth(TreeNode root, int depth){
        if (root == null)
            return depth;
        int leftLength = getDepth(root.left, depth + 1);
        if (leftLength == -1)
            return -1;
        int rightLength = getDepth(root.right, depth + 1);
        if (rightLength == -1)
            return -1;

        // 注意了，左右子树已经 + 1，结果就不用再加1了
//        return Math.abs(rightLength - leftLength) > 1 ? -1 : Math.max(rightLength, leftLength) + 1;
        return Math.abs(rightLength - leftLength) > 1 ? -1 : Math.max(rightLength, leftLength);
    }
}
