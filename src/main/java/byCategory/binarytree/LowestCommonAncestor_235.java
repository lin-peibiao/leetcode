package byCategory.binarytree;

/**
 * @author xiaohu
 * @date 2022/08/28/ 15:59
 * @description BST的最近公共祖先，不同于236，普通二叉树的最近公共祖先
 */
public class LowestCommonAncestor_235 {
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) { // 递归结束条件
            return root;
        }
        if (p.val < root.val && q.val < root.val) // 都在根节点左边
            return lowestCommonAncestor(root.left, p, q);
        if (p.val > root.val && q.val > root.val) // 都在根节点右边
            return lowestCommonAncestor(root.right, p, q);
        return root; // 在根节点两边
    }
}
