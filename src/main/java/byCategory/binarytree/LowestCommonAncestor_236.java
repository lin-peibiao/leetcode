package byCategory.binarytree;

/**
 * @author xiaohu
 * @date 2022/08/24/ 22:38
 * @description 236、找到最近公共祖先
 */
public class LowestCommonAncestor_236 {
    /**
     * 思路嘛 理解成本较高
     * 但是代码很好理解，很容易记住！
     * 利用回溯的特点
     * @param root
     * @param p
     * @param q
     * @return
     */
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        if (root == null || root == p || root == q) { // 递归结束条件
            return root;
        }
        // 从左右子树找 p、q 节点
        // 后序遍历
        TreeNode left = lowestCommonAncestor(root.left, p, q);
        TreeNode right = lowestCommonAncestor(root.right, p, q);

        if(left == null && right == null) { // 若未找到节点 p 或 q
            return null;
        }else if(left == null && right != null) { // 若找到一个节点
            return right;
        }else if(left != null && right == null) { // 若找到一个节点
            return left;
        }else { // 若找到两个节点
            return root;
        }
    }
}
