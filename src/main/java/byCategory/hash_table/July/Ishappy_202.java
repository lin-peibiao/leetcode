package byCategory.hash_table.July;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * 判断一个数是否为快乐数
 * 快乐数的定义：「快乐数」 定义为：
 *
 * 对于一个正整数，每一次将该数替换为它每个位置上的数字的平方和。
 * 然后重复这个过程直到这个数变为 1，也可能是 无限循环 但始终变不到 1。
 * 如果这个过程 结果为 1，那么这个数就是快乐数。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode.cn/problems/happy-number
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 *
 * @author xiaohu
 */
public class Ishappy_202 {
    /**
     * 递归
     * @param num
     * @return
     */
    public boolean isHappy(int num){
        if (num == 1 || num == 7) return true;
        else if (num < 10) return false;
        else{
            int newNum = getNextNumber(num);
            return isHappy(newNum);
        }
    }

    private int getNextNumber(int n) {
        int res = 0;
        while (n > 0) {
            int temp = n % 10;
            res += temp * temp;
            n = n / 10;
        }
        return res;
    }

    /**
     * 大佬的题解，转换为如果判断是否有重复数，没有的话找到1为止。
     * @param n
     * @return
     */
    public boolean isHappy1(int n) {
        Set<Integer> record = new HashSet<>();
        while (n != 1 && !record.contains(n)) {
            record.add(n);
            n = getNextNumber(n);
        }
        return n == 1;
    }

    @Test
    public void tets(){
        System.out.println(isHappy(19));
    }
}
