package byCategory.hash_table.July;

import java.util.HashMap;
import java.util.Map;

/**
 * 454、四数之和
 * @author xiaohu 
 */
public class FourSumCount_454 {
    public int fourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        if (nums1 == null || nums1.length == 0) return 0;
        int n = nums1.length;
        
        Map<Integer, Integer> map1 = new HashMap<>();
        int res = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                int num1 = nums1[i] + nums2[j];
                if (map1.containsKey(num1))
                    map1.put(num1, map1.get(num1) + 1);
                else
                    map1.put(num1, 1);
            }
        }

        //统计剩余的两个元素的和，在map中找是否存在相加为0的情况，同时记录次数
        for (int i : nums3) {
            for (int j : nums4) {
                int temp = i + j;
                if (map1.containsKey(-temp)) {
                    res += map1.get(-temp);
                }
            }
        }

        return res;
    }


    /**
     * 大佬题解
     * @param nums1
     * @param nums2
     * @param nums3
     * @param nums4
     * @return
     */
    public int fourSumCount1(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {
        Map<Integer, Integer> map = new HashMap<>();
        int temp;
        int res = 0;
        //统计两个数组中的元素之和，同时统计出现的次数，放入map
        for (int i : nums1) {
            for (int j : nums2) {
                temp = i + j;
                if (map.containsKey(temp)) {
                    map.put(temp, map.get(temp) + 1);
                } else {
                    map.put(temp, 1);
                }
            }
        }
        //统计剩余的两个元素的和，在map中找是否存在相加为0的情况，同时记录次数
        for (int i : nums3) {
            for (int j : nums4) {
                temp = i + j;
                if (map.containsKey(-temp)) {
                    res += map.get(-temp);
                }
            }
        }
        return res;
    }
}
