package byCategory.hash_table.July;

import java.util.HashMap;
import java.util.Map;

/**
 * 383、赎金信
 * 判断一个字符串是否由另一个字符串的字符组成
 */
public class CanConstruct_383 {
    public boolean canConstruct(String ransomNote, String magazine) {
        if (ransomNote.length() > magazine.length()) return false;
        Map<Character, Integer> map = new HashMap<>();
        for (int i = 0; i < magazine.length(); i++) {
            char ch = magazine.charAt(i);
            if (map.containsKey(ch))
                map.put(ch, map.get(ch) + 1);
            else
                map.put(ch, 1);
        }

        for (int i = 0; i < ransomNote.length(); i++) {
            char ch = ransomNote.charAt(i);
            if (!map.containsKey(ch) || map.get(ch) <= 0) return false;
            map.put(ch, map.get(ch) - 1);
        }

        return true;
    }

    /**
     * 优化， 使用一维数组
     * @param r
     * @param m
     * @return
     */
    public boolean canConstruct1(String r, String m){
        if (r.length() > m.length()) return false;
        int[] record = new int[26];
        for (int i = 0; i < m.length(); i++) {
            int ch = m.charAt(i) - 'a';
            record[ch]++;
        }

        for (int i = 0; i < r.length(); i++) {
            int ch = r.charAt(i) - 'a';
            record[ch]--;
        }

//        for (char c : r.toCharArray()) {
//
//        }

        for (int i : record) {
            if (i < 0)
                return false;
        }

        return true;
    }


    /**
     * 大佬题解
     * @param ransomNote
     * @param magazine
     * @return
     */
    public boolean canConstruct2(String ransomNote, String magazine) {
        // 定义一个哈希映射数组
        int[] record = new int[26];

        // 遍历
        for(char c : magazine.toCharArray()){
            record[c - 'a'] += 1;
        }

        for(char c : ransomNote.toCharArray()){
            record[c - 'a'] -= 1;
        }

        // 如果数组中存在负数，说明ransomNote字符串总存在magazine中没有的字符
        for(int i : record){
            if(i < 0){
                return false;
            }
        }

        return true;
    }

}
