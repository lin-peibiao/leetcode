package byCategory.hash_table.July;

import java.util.HashMap;
import java.util.Map;

/**
 * 242、有效的字母异位词
 * @author xiaohu 
 */
public class IsAnagram_242 {

    /**
     * 比较容易想到的题解
     * @param s
     * @param t
     * @return
     */
    public boolean isAnagram(String s, String t){
        if (s.length() != t.length())
            return false;
        int len = s.length();
        Map<Character,Integer> map1 = new HashMap<>();
        for (int i = 0; i < len; i++) {
            char ch = s.charAt(i);
            if (map1.containsKey(ch))
                map1.put(ch,map1.get(ch) + 1);
            else
                map1.put(ch,1);
        }

        for (int i = 0; i < len; i++) {
            char ch = t.charAt(i);
            if (!map1.containsKey(ch))
                return false;
            else
                map1.put(ch,map1.get(ch) - 1);

            if (map1.get(ch) == 0)
                map1.remove(ch);
        }
        return true;
    }

    /**
     * 优化，因为本题目是以查询为主，所以将map换大小为26的一维数组
     * 降低查询时间复杂度
     * @param s
     * @param t
     * @return
     */
    public boolean isAnagram1(String s, String t){
        if (s.length() != t.length())
            return false;
        int len = s.length();
        int[] record = new int[26];
        for (int i = 0; i < len; i++) {
            int alp = s.charAt(i) - 'a';
            record[alp] += 1;
        }

        for (int i = 0; i < len; i++) {
            int alp = t.charAt(i) - 'a';
            record[alp] -= 1;
        }

        for (int i = 0; i < 26; i++) {
            if (record[i] != 0)
                return false;
        }

        return true;
    }

    /**
     * 大佬的题解
     * @param s
     * @param t
     * @return
     */
    public boolean isAnagram2(String s, String t) {

        int[] record = new int[26];
        for (char c : s.toCharArray()) {
            record[c - 'a'] += 1;
        }
        for (char c : t.toCharArray()) {
            record[c - 'a'] -= 1;
        }
        for (int i : record) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
    }

}
