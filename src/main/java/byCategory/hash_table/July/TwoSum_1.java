package byCategory.hash_table.July;

import java.util.HashMap;
import java.util.Map;

/**
 * 1、两数之和
 * 有人相爱，有人夜里开车看海，有人leetcode第一题都做不出来
 * 虽然已经做过了，这次使用哈希表再做一次
 */
public class TwoSum_1 {

    /**
     * 暴力枚举。40毫秒多些，不写了。
     */

    /**
     * 四毫秒
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum(int nums[], int target){
        if (nums == null || nums.length <= 2) return null;
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            map.put(nums[i], i);
        }

        for (int i = 0; i < nums.length; i++) {

            if (map.containsKey(target - nums[i])) {
                int index = map.get(target - nums[i]);
                if (i == index)
                    continue;
                return new int[]{i, index};
            }
        }

        return null;
    }

    /**
     * 官方题解。一毫秒
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum2(int[] nums, int target) {
        Map<Integer, Integer> hashtable = new HashMap<Integer, Integer>();
        for (int i = 0; i < nums.length; ++i) {
            if (hashtable.containsKey(target - nums[i])) {
                return new int[]{hashtable.get(target - nums[i]), i};
            }
            hashtable.put(nums[i], i);
        }
        return new int[0];
    }

    /**
     * 大佬题解，比我的慢一毫秒
     * 区别就是判断在添加之前，所以不用再加一步会不会使用同一个下标的判断
     * @param nums
     * @param target
     * @return
     */
    public int[] twoSum1(int[] nums, int target) {
        int[] res = new int[2];
        if(nums == null || nums.length == 0){
            return res;
        }
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++){
            int temp = target - nums[i];
            if(map.containsKey(temp)){
                res[1] = i;
                res[0] = map.get(temp);
            }
            map.put(nums[i], i);
        }
        return res;
    }
}
