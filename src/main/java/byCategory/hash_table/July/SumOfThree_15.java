package byCategory.hash_table.July;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * 15、三数之和
 * 旧题重做，上次做使用的是三指针，这次使用递归试试
 * @author xiaohu
 */
public class SumOfThree_15 {
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums == null || nums.length == 0) return null;
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList<>();
        LinkedList<Integer> path = new LinkedList<>();
        dfs(0,0, nums, path, res);
        return res;
    }


    /**
     * 使用递归就会有重复的答案，不符合题目要求
     * @param start
     * @param target
     * @param nums
     * @param path
     * @param res
     */
    private void dfs(int start, int target, int[] nums, LinkedList<Integer> path, List<List<Integer>> res) {
        // 递归结束条件
        if (path.size() == 3){
            if (target == 0)
                res.add(new ArrayList<>(path));
            return;
        }

        for (int i = start; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i-1]) continue;
            path.addLast(nums[i]);
            dfs(i+1,target+nums[i],nums,path,res);
            path.removeLast();
        }
    }
}
