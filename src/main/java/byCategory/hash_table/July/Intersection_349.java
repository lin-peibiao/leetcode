package byCategory.hash_table.July;

import java.util.HashSet;
import java.util.Set;

/**
 * 349、2个数组的交集
 * @author xiaohu
 */
public class Intersection_349 {

    /**
     * 没有优化途径，没有撤退可言
     * 因为没有直接将set集合转换为int数组类型的api
     * @param nums1
     * @param nums2
     * @return
     */
    public int[] intersection(int[] nums1, int[] nums2){
        if (nums1.length == 0 || nums2.length == 0)
            return null;
        Set<Integer> set1 = new HashSet<>();
        for (int i : nums1) {
            set1.add(i);
        }
        Set<Integer> set2 = new HashSet<>();
        for (int i : nums2) {
            if (set1.contains(i))
                set2.add(i);
        }
        int[] res = new int[set2.size()];
        int i = 0;
        for (Integer integer : set2) {
            res[i++] = integer;
        }
        return res;

    }
}
