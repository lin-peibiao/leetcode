package byCategory.dp.June;

/**
 * 583、两个字符串的删除操作
 * @author xiaohu
 */
public class MinDistance_583 {
    public int minDistance(String text1,String text2){
        int m = 0,n = 0;
        int maxLen = 0;
        int len1 = text1.length(),len2 = text2.length();
        int[][] dp = new int[len1][len2];
        for (int i = 0; i < len1; i++) {
            for (int j = n; j < len2; j++) {
                if (text1.charAt(i) == text2.charAt(j)) {
                    dp[i][j] = dp[m][n] + 1;
                    m = i;
                    n = j;
                    maxLen = Math.max(maxLen,dp[i][j]);
                }
            }
        }
        return len1 + len2 - 2 * maxLen;
    }

    /**
     * 复制正确的代码
     * 找重复子串，然后计算两个 `重复子串与原串的差值` 之和
     * @return
     */
    public int try02(String text1,String text2){
        int len1 = text1.length(),len2 = text2.length();
        int[][] dp = new int[len1 + 1][len2 + 1]; // 先对dp数组做初始化操作
        for (int i = 1 ; i <= len1 ; i++) {
            char char1 = text1.charAt(i - 1);
            for (int j = 1; j <= len2; j++) {
                char char2 = text2.charAt(j - 1);
                if (char1 == char2) { // 开始列出状态转移方程
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        return len1 + len2 - 2 * dp[len1][len2];
    }
}
