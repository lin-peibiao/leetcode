package byCategory.dp.June;

import org.junit.Test;

/**
 * LCS、下载插件
 * 每一分钟有两个选择：下载插件，加速下载速度
 * 返回下载n个插件需要至少多少分钟
 * @author xiaohu
 */
public class LeastMinutes_LCS {

    /**
     * 虽然但是，dp不会，直接用算法也····
     * 只是一个方法：开始的时间用来加速，直到插件小于等于下载的速度
     * @return
     */
    public int leastMinutes(int n){
        int speed = 1;
        int time = 0;
        while(speed < n){
            ++time;
            speed <<= 1;
        }
        return time + 1;
    }

    @Test
    public void test(){
        String a = "abc";
        String b = "abc";
        String c = new String("abc");
        String d = new String("abc");
        System.out.println(a==c);
        System.out.println(c.equals(d));
    }

}
