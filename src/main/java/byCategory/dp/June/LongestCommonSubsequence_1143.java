package byCategory.dp.June;

import org.junit.Test;

/**
 * 1143、最长公共序列 不连续重复
 * @author xiaohu
 */
public class LongestCommonSubsequence_1143 {
    public int longestCommonSubsequence(String text1,String text2){
        int m = 0,n = 0;
        int maxLen = 0;
        int[][] dp = new int[text1.length()][text2.length()];
        for (int i = 0; i < text1.length(); i++) {
            for (int j = n; j < text2.length(); j++) {
                if (text1.charAt(i) == text2.charAt(j)) {

                    dp[i][j] = dp[m][n] + 1;
                    m = i;
                    n = j;
                    maxLen = Math.max(maxLen,dp[i][j]);
                }
            }
        }
        return maxLen;
    }

    /**
     * 第二次尝试  哎，还是超出时间限额了
     * @param text1
     * @param text2
     * @return
     */
    public int try02(String text1,String text2){
        if (text1 == null || text2 == null)
            return 0;
        int len1 = text1.length(),len2 = text2.length();
        int maxLen = 0;
        int dp[][] = new int[len1][len2];
        for (int i = 0; i < len1; i++) {
            for (int j = 0; j < len2; j++) {
                if (text1.charAt(i) == text2.charAt(j)) {
                    int temMax = 0;
                    for (int k = 0; k < i; k++) {
                        for (int l = 0; l < j; l++) {
                            temMax = Math.max(temMax,dp[k][l]);
                        }
                    }
                    dp[i][j] = temMax +1;
                    maxLen = Math.max(maxLen,dp[i][j]);
                }
            }
        }
        for (int[] ints : dp) {
            for (int anInt : ints) {
                System.out.print(anInt+"\t");
            }
            System.out.println();
        }




        return maxLen;
    }

    public int try03(String text1,String text2){
        if (text1 == null || text2 == null)
            return 0;
        int len1 = text1.length(),len2 = text2.length();
        int dp[][] = new int[len1+1][len2+1];
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (text1.charAt(i-1) == text2.charAt(j-1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        for (int[] ints : dp) {
            for (int anInt : ints) {
                System.out.print(anInt+"\t");
            }
            System.out.println();
        }




        return dp[len1][len2];
    }

    /**
     * 大神题解
     * @param text1
     * @param text2
     * @return
     */
    public int longestCommonSubsequence1(String text1, String text2) {
        int[][] dp = new int[text1.length() + 1][text2.length() + 1]; // 先对dp数组做初始化操作
        for (int i = 1 ; i <= text1.length() ; i++) {
            char char1 = text1.charAt(i - 1);
            for (int j = 1; j <= text2.length(); j++) {
                char char2 = text2.charAt(j - 1);
                if (char1 == char2) { // 开始列出状态转移方程
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                }
            }
        }
        return dp[text1.length()][text2.length()];
    }

    @Test
    public void test(){
        String s1 = "bl";
        String s2 = "byb";
        System.out.println(try03(s1, s2));
    }
}
