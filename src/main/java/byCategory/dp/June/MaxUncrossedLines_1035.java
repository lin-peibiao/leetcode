package byCategory.dp.June;

/**
 * 1035、不相交的线
 * @author xiaohu
 */
public class MaxUncrossedLines_1035 {

    /**
     * 第一次尝试 使用used[]   解题不对
     * @param nums1
     * @param nums2
     */
    public int try0(int[] nums1, int[]nums2){
        if (nums1 == null || nums2 == null)
            return 0;
        int len1 = nums1.length;
        int len2 = nums2.length;
        int[][] dp = new int[len1+1][len2+1];
        boolean[] used = new boolean[len1];
        if (nums1[0] == nums2[0]){
            used[0] = true;
            dp[0][0] = 1;
        }
        for (int i = 1; i < len1; i++) {
            for (int j = 1; j < len2; j++) {
                if (nums1[i] == nums2[j] && !used[i]){
                    dp[i][j] = dp[i-1][j-1]+1;
                    used[i] = true;
                }else
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
            }
        }
        return dp[len1-1][len2-1];
    }

    public int maxUncrossedLines(int[] nums1,int[] nums2){
        if (nums1 == null || nums2 == null)
            return 0;
        int len1 = nums1.length;
        int len2 = nums2.length;
        int[][] dp = new int[len1+1][len2+1];
        for (int i = 1; i <= len1; i++) {
            for (int j = 1; j <= len2; j++) {
                if (nums1[i-1] == nums2[j-1]){
                    dp[i][j] = dp[i-1][j-1]+1;
                }else
                    dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
            }
        }
        return dp[len1][len2];
    }

}
