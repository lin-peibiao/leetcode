package byCategory.dp.June;

/**
 * 按照给定的规则构造数组，并给返回最大值
 *
 * @author xiaohu
 */
public class GetMax_1646 {

    /**
     * 实在是大受震撼，只是将题目翻译了一下
     * 简单题果然是找回成就感和怀疑自己的题目
     * @param n
     * @return
     */
    public int getMaximumGenerated(int n){
        if (n == 0)
            return 0;
        int[] dp = new int[n+1];
        dp[1] = 1;
        int max = 0;
        for (int i = 2; i <= n; i++) {
            if (2 * i <= n)
                dp[2 * i] = dp[i];
            if (2 * i <= n - 1)
                dp[2 * i + 1] = dp[i] + dp[i + 1];
            max = Math.max(max,dp[i]);
        }
        return max;
    }

    /*
        分奇偶，如果是偶数，i = i/2,如果是奇数，i = i/2 + 1
        nums[i] = nums[i / 2] + i % 2 * nums[i / 2 + 1];
     */
}
