package byCategory.dp.June;

import org.junit.Test;

/**
 * 寻找最长连续子序列
 * @author xiaohu
 */
public class FindLengthOfLCIS_674 {
    /**
     * 找回一点点自信的简单题
     * @param nums
     * @return
     */
    public int findLengthOfLCIS(int nums[]){
        if (nums == null || nums.length == 0)
            return 0;
        int maxLen = 1;
        int dp[] = new int[nums.length+1];
        dp[0] = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] > nums[i-1]){
                dp[i] = dp[i-1]+1;
                maxLen = Math.max(dp[i],maxLen);
            }else
                dp[i] = 1;
        }
        return maxLen;
    }

    /**
     * 优化 滚动数组
     * @param nums
     * @return
     */
    public int findLengthLCI(int nums[]){
        if (nums==null || nums.length == 0)
            return 0;
        int maxLen = 1;
        int pre = nums[0] ,cur = 1;
        for (int num : nums) {
            if (num > pre ){
                cur += 1;
                maxLen = Math.max(maxLen,cur);
            }else
                cur = 1;
            pre = num;
            System.out.println(cur);
        }
        return maxLen;
    }


    /**
     * 718、寻找最长重复数组
     * @param nums1
     * @param nums2
     * @return
     */
    public int findLength(int[] nums1,int[] nums2){
        if (nums1 == null || nums2 == null)
            return 0;
        int maxLen = 0;
        // 虽然给出的数组长度是相等的，但是以防以后出现长度不相等，先这么写，先循环长的
        int len1 = nums1.length,len2 = nums2.length;
        int[][] dp = new int[len1+1][len2+1];
        for (int i = 0; i < len1; i++) {
            for (int j = 0; j < len2; j++) {
                if (nums1[i] == nums2[j]){
                    dp[i][j] = dp[i-1][j-1]+1;
                    maxLen = Math.max(maxLen,dp[i][j]);
                }
            }
        }
        return maxLen;
    }

    /**
     * 优化，滚动数组
     * @param nums1
     * @param nums2
     * @return
     */
    public int findLength2(int[] nums1, int[] nums2) {
        int[] dp = new int[nums2.length + 1];
        int result = 0;

        /*
        为什么必须从右往左计算：

        因为 计算当前值 依赖的是 其左上角的值，如果从左向右计算

        会在计算到元素左边值时，把左上角的值 覆盖掉
         */
        for (int i = 1; i <= nums1.length; i++) {
            for (int j = nums2.length; j > 0; j--) {
                if (nums1[i - 1] == nums2[j - 1]) {
                    dp[j] = dp[j - 1] + 1;
                } else {
                    dp[j] = 0;
                }
                result = Math.max(result, dp[j]);
            }
        }
        return result;
    }


    @Test
    public void test(){
        int nums[] = new int[]{
                10,9,2,5,3,7,101,18
        };
        System.out.println(findLengthOfLCIS(nums));
    }
}

abstract class G{
    // 抽象方法可以有构造方法
    public G(){}
    int plus(int a,int b){
        return a+b;
    }
}

class GC {
    // 接口和抽象类不能new
    G g = new G() {
        // 匿名内部类继承G
        @Override
        int plus(int a, int b) {
            return super.plus(a, b);
        }

    };

}

interface D{
}
