package byCategory.dp.June;

import org.junit.Test;

/**
 * 516、最长回文子序列
 * @author xiaohu
 */
public class LongestPalindromeSubseq_516 {
    public int longestPalindromeSubseq(String s){
        if (s == null || s.length() == 0)
            return 0;
        int len = s.length();
        int[][] dp = new int[len][len];
        for (int i = len-1; i >= 0; i--) {
            // dp初始化
            dp[i][i] = 1;
            char ch = s.charAt(i);
            for (int j = i + 1; j < len ; j++) {
                // 永远不能出现 i 和 j相等的情况,保证不会ArrayOutOfIndex
                if (ch == s.charAt(j)){
                    dp[i][j] = dp[i+1][j-1] + 2;
                }
                else{
                    // 对应分别加入i 或 j的情况，取大者
                    dp[i][j] = Math.max(dp[i+1][j],dp[i][j-1]);
                }
            }
        }

        return dp[0][len-1];
    }

    @Test
    public void test(){
        System.out.println(-16 >>> 1);
    }

}
