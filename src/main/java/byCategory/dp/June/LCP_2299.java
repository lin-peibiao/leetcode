package byCategory.dp.June;

/**
 * 2299、玩信息传递的游戏，返回传递方案数
 * 简单题
 *
 * @author xiaohu
 */
public class LCP_2299 {

    /**
     * 动态规划 （可以优化为一维数组）
     * byCategory.dp[i][j]表示通过i步传达到j的总方案数
     * @param n
     * @param relation
     * @param k
     * @return
     */
    public int numWays(int n, int[][] relation, int k){
        int[][] dp = new int[k+1][n];
        dp[0][0] = 1;
        for (int i = 1; i <= k; i++) {
            for (int[] r : relation) {
                int a = r[0], b = r[1];
                dp[i][b] += dp[i-1][a];
            }
        }
        return dp[k][n-1];
    }
}
