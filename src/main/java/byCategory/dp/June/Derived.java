package byCategory.dp.June;

import org.junit.Test;

interface A{
    int a = 0;
    public  void a();
}

class Base extends Object{
    public Base(String s){
        System.out.print("B");
    }

    @Test
    public void test(){
        byte a = 0;
        byte b = 1;
        int c = a+b;
        b = (byte)(a+b);
        // byte类型在进行运算时会自动转换成int类型，如果不强制转换类型会报错
        // java语句中用的如果是 += 的语句，此语句会将被赋值的变量自动强制转化为相对应的类型。
    }
}
public final class Derived extends Base{
    public Derived (String s) {
        super(s);
        // 当子类调用构造方法会首先默认调用父类的无参构造方法，如果父类没有无参构造方法，则需要在子类中显示调用父类的有参构造
        System.out.print("D");
    }
    public static void main(String[] args){
//        new Derived("C");
    }

    public void A(){

    }
    // 方法可以被重载，但是类的层面上，不能被继承
    public void A(int a){

    }
}