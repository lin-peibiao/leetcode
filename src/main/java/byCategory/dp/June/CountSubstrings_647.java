package byCategory.dp.June;

/**
 * 647、回文子串
 * @author xiaohu
 */
public class CountSubstrings_647 {

    public int countSubstrings(String s){
        if (s == null || s.length() == 0)
            return 0;
        int len = s.length();
        // byCategory.dp[i][j]表示以字符串s[i][j]为首尾的子串是否为回文 如果是便是true
        /* 总共有两种大情况
            1、s[i] != s[j],不用操作，不用讨论，false
            2、s[i] = s[j],分三种情况讨论
                1、i j相邻
                2、i j只隔一个字符
                3、i j之间字符数 > 1

         */

        boolean[][] dp = new boolean[len+1][len+1];
        int res = 0;
        for (int i = len-1; i >= 0 ; i--) {
            char ch = s.charAt(i);
            for (int j = i; j < len; j++) {
                if (ch == s.charAt(j)){
                    // 相等的第一二种情况，左右相差数 <= 1
                    if (Math.abs(i-j) <= 1){
                        res++;
                        dp[i][j] = true;
                    }
                    // 第三种情况
                    else if (dp[i+1][j-1]){
                        res++;
                        dp[i][j] = true;
                    }
                }
                // 如果不相等，则为false,不用操作
            }
        }
        return res;
    }
}
