package byCategory.dp.June;

/**
 * 392、判断子序列
 * @author xiaohu
 */
public class IsSubsequence_392 {
    public boolean isSubsequence(String s,String t){
        int len1 = s.length(),len2 = t.length();
        if (len1 > len2)
            return false;
        int[][] dp = new int[len1+1][len2+1];
        for (int i = 1; i <= len1; i++) {
            char c1 = s.charAt(i-1);
            for (int j = 1; j <= len2; j++) {
                char c2 = t.charAt(j-1);
                if (c1 == c2)
                    dp[i][j] = dp[i-1][j-1] + 1;
                else
                    dp[i][j] = Math.max(dp[i-1][j],dp[i][j-1]);
            }
        }
        return dp[len1][len2] == len1;
    }
}
