package byCategory.dp.June;

import org.junit.Test;

/**
 * 72、编辑距离，将一个字符串修改为另一个字符串的最少步骤
 * @author xiaohu
 */
public class MinDistance_72 {
    public int minDistance(String word1,String word2){
        int len1 = word1.length();
        int len2 = word2.length();
        int[][] dp = new int[len1+1][len2+1];
        // 初始化 如果看见时不理解，那么想想dp[i][0],byCategory.dp[0][j]的含义
        for (int i = 0; i <= len1; i++) {
            dp[i][0] = i;
        }
        for (int i = 0; i <= len2; i++) {
            dp[0][i] = i;
        }

        for (int i = 1; i <= len1; i++) {
            char c1 = word1.charAt(i-1);
            for (int j = 1; j <= len2; j++) {
                if (c1 == word2.charAt(j-1))
                    dp[i][j] = dp[i-1][j-1];
                else
                    // 分别对应三种操作
                    dp[i][j] = Math.min(Math.min(dp[i-1][j-1],dp[i-1][j]),dp[i][j-1]) + 1;
            }
        }
        return dp[len1][len2];
    }

    @Test
    public void test(){
        String word = "";
        System.out.println(word == null);
        System.out.println(word.length());
    }
}
