package byCategory.dp.June;

/**
 * 1137、泰波那契数
 *
 * @author xiaohu
 */
public class Tribonacci_1137 {
    public int tribonacci(int n){
        int dp[] = new int[n+1];
        dp[1] = dp[2] = 1;
        for (int i = 3; i < n+1; i++) {
            dp[i] = dp[i-1] + dp[i-2] + dp[i-3];
        }

        return dp[n];
    }

    /**
     * 优化一下，使用滚动数组
     * @param n
     * @return
     */
    public int optimizeTriB(int n){
        int pre1 = 0,pre2 = 1,pre3;
        int ans = 1;
        for (int i = 3; i <= n; i++) {
            pre3 = ans;
            ans = pre1 + pre2 + pre3;
            pre1 = pre2;
            pre2 = pre3;
        }
        return n < 2 ? n : ans;
    }
}
