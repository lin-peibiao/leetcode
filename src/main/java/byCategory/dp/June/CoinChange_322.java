package byCategory.dp.June;

import org.junit.Test;

import java.util.Arrays;

/**
 * 322、面额兑换
 *
 * @author xiaohu
 */
public class CoinChange_322 {

    /**
     * 我他妈还是直接复制吧
     * @param coins
     * @param amount
     * @return
     */
    public int coinChange(int[] coins, int amount) {
        // 判断金额凑不出的小技巧：先初始化DP table各个元素为amount + 1（代表不可能存在的情况），在遍历时如果金额凑不出则不更新，于是若最后结果仍然是amount + 1，则表示金额凑不出
        int max = amount + 1;
        int[] dp = new int[amount + 1];
        Arrays.fill(dp, max);
        dp[0] = 0;
        for (int i = 1; i <= amount; i++) {
            for (int j = 0; j < coins.length; j++) {
                if (coins[j] <= i) {
                    dp[i] = Math.min(dp[i], dp[i - coins[j]] + 1);
                }
            }
        }
        return dp[amount] > amount ? -1 : dp[amount];
    }



    /**
     * 动态规划
     * @param coins
     * @param amount
     * @return
     */
    public int coinChange2(int[] coins,int amount){
        int[] rem = new int[amount+1];
        Arrays.fill(rem, amount+1);
        rem[0] = 0;
        for (int i = 1; i <= amount; i++) {
            for (int coin : coins) {
                if (i < coin)
                    continue;
                rem[i] = Math.min(rem[i-coin]+1,rem[i]);
            }
        }
        return rem[amount] > amount ? -1 : rem[amount];
    }
    @Test
    public void test(){
        int[] coins = new int[]{1,3,5};
        System.out.println(coinChange2(coins, 11));
    }

















    public int coinChange1(int[] coins,int amount){
        if (coins.length == 0)
            return -1;
        int res = 0;
        // 先给数组排序
        Arrays.sort(coins);
        /*
        for (int i = coins.length-1; i >= 0; i--) {
            if (amount == 0)
                break;
            if (amount < coins[i])
                continue;
            res += amount / coins[i];
            amount %= coins[i];
        }

         */
        return dfs(coins,amount,coins.length-1,0);
    }

    public int dfs(int[] coins,int amount,int right, int res){
        if (amount == 0){
            return res;
        }
        for (int i = right; i >= 0 ; i--) {
            if (amount < coins[i])
                continue;
            int tem = amount;
            res += amount / coins[i];
            amount %= coins[i];
            dfs(coins,amount,i-1,res);
            // 回溯
            res -= tem / coins[i];
            amount = tem;

        }
        return -1;
    }



}
