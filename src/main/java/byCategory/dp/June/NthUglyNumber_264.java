package byCategory.dp.June;

import org.junit.Test;

/**
 * 263、确定第n个丑数
 *
 * @author xiaohu
 */
public class NthUglyNumber_264 {
    /**
     * 这个题解是完全不一样的动态规划，byCategory.dp+三指针
     * @param n
     */
    public int nthUglyNumber(int n){
        int[] ugly = new int[n+1];
        ugly[1] = 1;
        // 维护三个指针
        int p2 = 1,p3 = 1,p5 = 1;
        for (int i = 2; i < n+1; i++) {
            // 虚拟数组中的三个头部元素，取出较小者
            int num2 = ugly[p2] * 2, num3 = ugly[p3] * 3, num5 = ugly[p5] * 5;
            ugly[i] = Math.min(num2, Math.min(num3,num5));
            // 当元素被添加到ugly数组或有重复元素时，相应指针右移
            if (ugly[i] == num2)
                ++p2;
            if (ugly[i] == num3)
                ++p3;
            if (ugly[i] == num5)
                p5++;
        }
        System.out.println(p2+p3+p5);
        return ugly[n];
    }
    @Test
    public void test(){
        nthUglyNumber(23);
    }
}
