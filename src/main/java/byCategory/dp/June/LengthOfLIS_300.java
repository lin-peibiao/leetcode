package byCategory.dp.June;

import org.junit.Test;

import java.util.*;

/**
 * 最长递增子序列
 * @author xiaohu
 */
public class LengthOfLIS_300 {


    /**
     * 我是傻逼系列
     * @param nums
     * @return
     */
    public int lengthOfLIS1(int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int[] dp = new int[nums.length];
        dp[0] = 1;
        int maxans = 1;
        // 分别比对
        for (int i = 1; i < nums.length; i++) {
            dp[i] = 1;
            for (int j = 0; j < i; j++) {
                if (nums[i] > nums[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            maxans = Math.max(maxans, dp[i]);
        }
        return maxans;
    }


    public int lengthOfLIS(int nums[]){
        if (nums == null || nums.length == 0)
            return 0;
        int[] dp = new int[nums.length];
        Arrays.fill(dp,Integer.MIN_VALUE);
        dp[0] = nums[0];
        for (int i = 0; i < nums.length-1; i++) {
            if (nums[i+1] > dp[i]){
                dp[i+1] = nums[i+1];
            }else
                dp[i] = nums[i+1];
        }
        int res = 0;
        for (int i : dp) {
            if (i != Integer.MIN_VALUE)
                ++res;
        }
        return res;

    }

    @Test
    public void test(){
        List<Integer> list = new ArrayList<>();
        list.add(null);
        list.add(null);
        System.out.println(list.size());
        for (Integer integer : list) {
            System.out.println(integer);
        }

        Set set = new HashSet();
        set.add(null);
        set.add( null);
        System.out.println(set.size());
        Object obj = new Object();

    }

    int i = 0;
    static class BC{
        // 只有静态内部类才可以使用静态方法
        public static int aa(){
            return 12;
        }
    }
}


