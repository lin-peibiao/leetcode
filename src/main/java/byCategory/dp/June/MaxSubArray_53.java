package byCategory.dp.June;

import org.junit.Test;

/**
 * 53、最大子数组和
 * @author xiaohu
 */
public class MaxSubArray_53 {
    public int maxSubArray(int[] nums){
        if (nums == null || nums.length == 0)
            return 0;
        int[] dp = new int[nums.length+1];
        // 初始化数组为0
        int res = dp[0];
        for (int i = 1; i <= nums.length; i++) {
            dp[i] = Math.max(nums[i-1],dp[i-1]+nums[i-1]);
            res = Math.max(res,dp[i]);
        }
        return res;
    }

    /**
     * 优化
     * @return
     */
    public int enhance(int[] nums){
        if (nums == null || nums.length == 0)
            return 0;
        int dp = 0,pre;
        int res = nums[0];
        for (int num : nums) {
            pre = dp;
            dp = Math.max(pre+num, num);
            res = Math.max(dp,res);
        }
        return res;
    }

    @Test
    public void test(){
        int[] nums = new int[]{-2,1,-3,4,-1,2,1,-5,4};
        System.out.println(enhance(nums));
    }

}
