package byCategory.dp.sept;

import org.junit.Test;

/**
 * @author xiaohu
 * @date 2022/09/20/ 11:48
 * @description
 */
public class change_518 {
    public int change(int amount, int[] coins) {
        // 多重背包问题 容量是amount,coins 是物品
        // dp[i] 表示装满容量为i的背包的方法数量
        int[] dp = new int[amount + 1];
        dp[0] = 1;
        for (int j = 0; j <= amount; j++) { // 遍历背包容量
            for (int i = 0; i < coins.length; i++) { // 遍历物品
                if (j - coins[i] >= 0){
                    dp[j] += dp[j - coins[i]];
                    System.out.print(dp[j]);
                }
            }
            System.out.println();
        }

        return dp[amount];
    }

    @Test
    public void test(){
        change(5, new int[]{1,2,5});
    }
}
