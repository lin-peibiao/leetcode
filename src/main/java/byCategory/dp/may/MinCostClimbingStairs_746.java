package byCategory.dp.may;

/**
 * 746、用最小的花费爬楼梯
 *
 * @author xiaohu
 */
public class MinCostClimbingStairs_746 {
    /**
     * 动态规划 每一次遍历都要最小的那个花费，byCategory.dp[i]表示到达该级阶梯需要的最小花费
     * 则dp[i] = min: byCategory.dp[i-1] + cost[i-1], byCategory.dp[i-2] + cost[i-2]
     * @param cost
     * @return
     */
    public int minCostClimbingStairs(int[] cost){
        if (cost.length < 2 )
            return 0;
        int len = cost.length;
        int[] dp = new int[len+1];
        for (int i = 2; i <= len; i++) {
            dp[i] = Math.min(dp[i-1] + cost[i-1], dp[i-2] + cost[i-2]);
        }
        return dp[len];
    }

    /**
     * 动态规划优化
     * 由于当前阶梯花费只与前两个阶梯有关，所以可以使用滚动数组
     * @param cost
     */
    public int minCost(int[] cost){
        int pre1, pre2 = 0;
        int cur = 0;
        for (int i = 2; i <= cost.length; i++) {
            pre1 = pre2;
            pre2 = cur;
            cur = Math.min(pre1+cost[i-2],pre2+cost[i-1]);
        }

        return cur;
    }
}
