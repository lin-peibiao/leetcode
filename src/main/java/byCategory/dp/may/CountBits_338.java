package byCategory.dp.may;

import org.junit.Test;

/**
 * 338、比特位计数
 *
 * @author xiaohu
 */
public class CountBits_338 {

    /**
     * byCategory.dp
     * @param n
     * @return
     */
    public int[] countBits(int n){
        int[] ans = new int[n+1];
        ans[0] = 0;
        for (int i = 1; i <= n; i++) {
            if (i % 2 == 0)
                ans[i] = ans[i/2];
            else
                ans[i] = ans[i/2]+1;
        }
        return ans;
    }

    /**
     * dp2
     * 分为奇偶 也就是二进制位最低位是 0 or 1
     * @param num
     * @return
     */
    public int[] countBits1(int num) {
        int dp[] = new int[num+1];
        for (int i = 0; i <= num/2; i++) {
            dp[i*2] = dp[i]; // 偶次位
            if(i*2+1 <= num) // 奇次位
                dp[i*2+1] = dp[i] + 1;
        }
        return dp;
    }

    @Test
    public void test(){
        System.out.println(7^0);
        System.out.println(4 >> 3);
    }
}
