package byCategory.dp.may;

/**
 * 509、斐波那契数列
 *
 * @author xiaohu
 */
public class Fib_509 {
    public int fib(int n){
        int[] dp = new int[n+1];
        dp[0] = 0;
        dp[1] = 1;
        for (int i = 2; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }

    /**
     * 动态规划优化
     * 使用滚动数组
     * @return
     */
    public int optimizeFib(int n){

        int ans = 0;
        int pre1 = 0,pre2 = 1;
        for (int i = 2; i <= n; i++) {
            ans = pre1 + pre2;
            pre1 = pre2;
            pre2 = ans;
        }
        // 麻烦了，直接这样不更好
        // return n <= 1 ? n : ans;
        return n <= 1 ? (n == 0 ? ans : 1) : ans;
    }
}
