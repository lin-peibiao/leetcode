package byCategory.dp.may;

/**
 * 动态规划213、寻找最大的正方形，返回面积
 *
 * @author xiaohu
 */
public class MaximalSquare_213 {
    public int maximalSquare(char[][] matrix) {
        if (null == matrix || matrix.length == 0 || matrix[0].length == 0) return 0;
        int ans = 0, m = matrix.length, n = matrix[0].length;
        // 代表以目前当前点为最大正方形的最大变长
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                // 如果当前位置是1，则当前中最大边长应该是左，上，左上中的最小值加1
                if (matrix[i - 1][j - 1] == '1') {
                    dp[i][j] = Math.min(dp[i - 1][j], Math.min(dp[i][j - 1], dp[i - 1][j - 1])) + 1;
                    ans = Math.max(ans, dp[i][j] * dp[i][j]);
                }
            }
        }
        return ans;
    }
}

