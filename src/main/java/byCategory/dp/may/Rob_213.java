package byCategory.dp.may;

import java.util.Arrays;

/**
 * 213、打家劫舍2 这次打劫的小区是环状的
 *
 * @author xiaohu
 */
public class Rob_213 {

    public int rob(int[] nums){
        if (nums == null && nums.length == 0)
            return 0;
        int len = nums.length;
        if (len == 2)
            return Math.max(nums[0],nums[1]);
        int[] dp = new int[len];
        boolean[] isRob = new boolean[len];
        isRob[0] = true;
        dp[0] = nums[1];
        dp[1] = Math.max(nums[0],nums[1]);
        isRob[1] = dp[1] == nums[0];
        for (int i = 2; i < len-1; i++) {
            boolean robCur = dp[i-2] + nums[i] > dp[i-1];
            if (robCur) {
                dp[i] = dp[i - 2] + nums[i];
                isRob[i] = isRob[i-2];
            }else{
                dp[i] = dp[i-1];
                isRob[i] = isRob[i-1];
            }
        }
        // 如果偷到倒数第2个时，发现偷第一间房子有更多钱，就得判断偷第一家还是最后一家更多钱
        // TODO 在此处不知道倒数第二家被偷了没有 知道的话就比较容易 所以这个思路不对
        if (isRob[len-2])
            return Math.max(dp[len-1] - nums[0],dp[len-1] - nums[len-1]);
        // 到达倒数第二不偷第一家更多钱，直接返回
        return dp[len-2] + nums[len-1];

    }

    /**
     * 其实就是将环拆解为0~len-1,1~len两个队列，分别求最大值，在返回两者较大的
     * @param nums
     * @return
     */
    public int rob1(int[] nums) {
        if(nums.length == 0) return 0;
        if(nums.length == 1) return nums[0];
        return Math.max(myRob(Arrays.copyOfRange(nums, 0, nums.length - 1)),
                myRob(Arrays.copyOfRange(nums, 1, nums.length)));
    }

    /**
     * 精辟
     * @param nums
     * @return
     */
    private int myRob(int[] nums) {
        int pre = 0, cur = 0, tmp;
        for(int num : nums) {
            tmp = cur;
            cur = Math.max(pre + num, cur);
            pre = tmp;
        }
        return cur;
    }
}
