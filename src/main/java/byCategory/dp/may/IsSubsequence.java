package byCategory.dp.may;

import org.junit.Test;

/**
 * 392 判断子序列
 *
 * @author xiaohu
 */
public class IsSubsequence {
    public boolean isSubsequence(String s, String t) {
        if (s.length() == 0)
            return true;
        if (s.length() > t.length())
            return false;
        int sLen = s.length();
        int tLen = t.length();
        boolean[] dp = new boolean[sLen];
        int index = -1;
        for (int i = 0; i < sLen; i++) {
            char ch = s.charAt(i);
            while (++index < tLen) {
                if (ch == t.charAt(index)){
                    dp[i] = true;
                    // 这个break就特别精妙 有关顺序
                    break;
                }
            }
        }

        return dp[sLen-1];
    }


    /**
     * 双指针解法
     */


    /**
     * 动态规划解法
     * @param s
     * @param t
     * @return
     */
    boolean isSubsequence1(String s, String t){
        int n = s.length(),m = t.length();
        //dp数组dp[i][j]表示字符串t以i位置开始第一次出现字符j的位置
        int[][] dp = new int[m+1][26];
        //初始化边界条件，byCategory.dp[i][j] = m表示t中不存在字符j
        for(int i=0;i<26;i++){
            dp[m][i] = m;
        }

        //从后往前递推初始化dp数组
        for(int i = m - 1;i>=0;i--) {
            for(int j=0;j<26;j++){
                if(t.charAt(i)== 'a' + j){
                    dp[i][j] = i;
                }else {
                    dp[i][j] = dp[i + 1][j];
                }
            }
        }

        int add = 0;
        for(int i = 0;i<n;i++){
            //t中没有s[i] 返回false
            if(dp[add][s.charAt(i) - 'a'] == m){
                return false;
            }
            //否则直接跳到t中s[i]第一次出现的位置之后一位
            add = dp[add][s.charAt(i) - 'a'] + 1;
        }
        return true;
    }

    @Test
    public void test(){
        String s = "asdf";
        String t = "ahhskkjf";
        System.out.println(isSubsequence(s, t));
    }

}
