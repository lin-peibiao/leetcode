package byCategory.mul_road;

import org.junit.Test;

import java.util.PriorityQueue;
import java.util.Stack;

/**
 * @author xiaohu
 * @date 2022/10/22/ 15:11
 * @description
 */
public class KthSmallest_786 {

    @Test
    public void test() {
        int arr[] = new int[]{1,3,5,7,11};
        System.out.println(smallestDistancePair(arr, 4));
    }

    public int smallestDistancePair(int[] nums, int k) {
        // 简直和786题一模一样
        // 优先队列 + 多路并归
        int n = nums.length;
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> {
            int dis1 = Math.abs(nums[a[1]] - nums[a[0]]);
            int dis2 = Math.abs(nums[b[1]] - nums[b[0]]);
            return dis1 - dis2;
        });
        for (int i = 1; i < n; i++){
            pq.add(new int[]{0, i});
        }
        while (k-- > 1){
            int[] idx = pq.poll();
            int i = idx[0], j = idx[1];
            if (i + 1 < j) {
                pq.add(new int[]{i + 1, j});
            }
        }
        int[] res = pq.poll();
        return nums[res[1]] - nums[res[0]];
    }


    public int[] kthSmallestPrimeFraction(int[] arr, int k) {
        // 优先队列 + 多路并归
        // 总的组合数为Cn取2,n 为数组的长度
        // 双指针
        int n = arr.length;
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> Double.compare((arr[a[0]] * 1.0)/arr[a[1]], (arr[b[0]] * 1.0) / arr[b[1]]));
        for (int i = n - 1; i >= 0; i--){
            for (int j = 0; j < i; j++){
                int[] idx = new int[]{j, i};
                pq.add(idx);
            }
        }
        int[] res = new int[2];
        while (k > 0 && !pq.isEmpty()){
            int[] idx = pq.poll();
            res[0] = arr[idx[0]];
            res[1] = arr[idx[1]];
        }
        return res;


    }


}
