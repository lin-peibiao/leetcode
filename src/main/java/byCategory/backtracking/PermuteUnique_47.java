package byCategory.backtracking;

import java.util.*;

/**
 * @author xiaohu
 * @date 2022/09/06/ 22:42
 * @description 47、全排列2 与46题的差别就是数组中会有重复的元素
 */
public class PermuteUnique_47 {
    public List<List<Integer>> permuteUnique(int[] nums) {
        if (nums == null || nums.length == 0){
            return new ArrayList();
        }
        List<List<Integer>> res = new ArrayList();
        Deque<Integer> path = new LinkedList();
        boolean[] used = new boolean[nums.length];
        Arrays.sort(nums);
        backTracking(nums, 0, used, path, res);
        return res;
    }

    private void backTracking(int[] nums, int index, boolean[] used, Deque<Integer> path, List<List<Integer>> res){
        if (path.size() == nums.length){
            res.add(new ArrayList(path));
            return;
        }

        for(int i = 0; i < nums.length; i++){

            // 横向判重 跳过，横向判重是需要先排序的
            if (i > 0 && nums[i] == nums[i - 1] && !used[i-1]){
                continue;
            }
            // 纵向判重 跳过
            if (used[i]){
                continue;
            }

            path.addLast(nums[i]);
            used[i] = true;
            backTracking(nums, 0, used, path, res);
            used[i] = false;
            path.removeLast();
        }
    }
}
