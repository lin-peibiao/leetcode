package byCategory.backtracking;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * @author xiaohu
 * @date 2022/09/04/ 13:59
 * @description 491、寻找递增子序列
 */
public class FindSubsequences_491 {
    // 也可以使用map来去重

    public List<List<Integer>> findSubsequences(int[] nums) {
        if (nums == null || nums.length == 0){
            return new ArrayList<>();
        }
        List<List<Integer>> res= new ArrayList<>();
        Deque<Integer> path = new LinkedList<>();
        int[] used = new int[201];
        backTracking(nums, 0, used, path, res);
        return res;
    }

    private void backTracking(int[] nums, int index, int[] used, Deque<Integer> path, List<List<Integer>> res) {
        // 添加策略
        if (path.size() > 1){
            res.add(new ArrayList<>(path));
        }

        // 遍历一遍，不需要回溯条件也可以
        for (int i = index; i < nums.length; i++) {
            // 判断是否递增、去重
            boolean up = !path.isEmpty() && nums[i] >= path.peekLast();
            boolean repeated = used[nums[i] + 100] == 1; // 防止下标为负数，所以加100
            if (!up || repeated){
                continue;
            }
            path.addLast(nums[i]);
            used[nums[i] + 100] = 1;
            backTracking(nums, i + 1, used, path, res);
            path.removeLast();
        }
    }

    @Test
    public void test(){
        int[] nums = new int[]{4,6,7,7};
        findSubsequences(nums);
    }
}
