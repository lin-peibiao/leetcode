package byCategory.backtracking;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * @author xiaohu
 * @date 2022/09/01/ 22:08
 * @description 17、电话号码字母的组合
 */

/**
 * 认识多了一个StringBuilder的方法————> deleteChartAt(index)
 */
public class LetterCombinations_17 {
    // 初始化String数组，映射电话号码上的字母
    String[] letters = new String[10];
    private void init(){
        letters[0] = "";
        letters[1] = "";
        letters[2] = "abc";
        letters[3] = "def";
        letters[4] = "ghi";
        letters[5] = "jkl";
        letters[6] = "mno";
        letters[7] = "pqrs";
        letters[8] = "tuv";
        letters[9] = "wxyz";
    }

    public List<String> letterCombinations(String digits) {
        if (digits == null || digits.length() == 0){
            return new ArrayList<>();
        }
        init();
        int index = 0;
        StringBuilder path = new StringBuilder();
        List<String> res = new ArrayList<>();
        backTracking(digits, path, index, res);
        System.out.println(res);
        return res;
    }

    private void backTracking(String digits, StringBuilder path, int index, List<String> res) {
        if (index >= digits.length()){

            res.add(path.toString());
            return;
        }
        int init = digits.charAt(index) - '0';
        for (int i = 0; i < letters[init].length(); i++) {
            path.append(letters[init].charAt(i) + "");
            backTracking(digits, path, index + 1, res);
            path.deleteCharAt(path.length()-1);
        }
    }

    @Test
    public void test(){
        letterCombinations("234");
    }
}
