package byCategory.backtracking;

import java.util.*;

/**
 * @author xiaohu
 * @date 2022/09/04/ 11:46
 * @description
 */
public class Subsets_78 {
    class Solution {
        public List<List<Integer>> subsets(int[] nums) {
            if (nums == null || nums.length == 0){
                return new ArrayList(new ArrayList());
            }
            List<List<Integer>> res = new ArrayList();
            res.add(new ArrayList());
            Deque<Integer> path = new LinkedList();
            dfs(nums, 0, path, res);
            return res;
        }

        private void dfs(int[] nums, int index, Deque<Integer> path,
                         List<List<Integer>> res){
            if (index >= nums.length){
                return;
            }

            for(int i = index; i < nums.length; i++){
                path.addLast(nums[i]);
                res.add(new ArrayList<>(path));
                dfs(nums, i + 1, path, res);
                path.removeLast();
            }
        }
    }

    /**
     * 子集2
     */
    class Solution2 {
        public List<List<Integer>> subsetsWithDup(int[] nums) {

            if (nums == null || nums.length == 0){
                return new ArrayList(new ArrayList());
            }
            Arrays.sort(nums);
            List<List<Integer>> res = new ArrayList();
            res.add(new ArrayList());
            Deque<Integer> path = new LinkedList();
            boolean[] used = new boolean[nums.length];
            dfs(nums, 0, used, path, res);
            return res;
        }

        private void dfs(int[] nums, int index, boolean[] used,
                         Deque<Integer> path, List<List<Integer>> res){
            if (index >= nums.length){
                return;
            }
            for(int i = index; i < nums.length; i++){
                // 相等并且已经添加过了。
                if (i > 0 && nums[i] == nums[i - 1] && !used[i - 1]){
                    continue;
                }
                path.addLast(nums[i]);
                used[i] = true;
                res.add(new ArrayList<>(path));
                dfs(nums, i + 1, used, path, res);
                path.removeLast();
                used[i] = false;
            }
        }
    }

}
