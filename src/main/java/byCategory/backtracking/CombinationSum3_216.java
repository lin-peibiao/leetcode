package byCategory.backtracking;

import org.junit.Test;

import java.util.*;

/**
 * @author xiaohu
 * @date 2022/08/30/ 0:05
 * @description 216、组合三
 */
public class CombinationSum3_216 {
    public List<List<Integer>> combinationSum3(int k, int n) {
        if (n < 1 || n > 45) return new ArrayList<>();
        List<List<Integer>> res = new ArrayList<>();
        Deque<Integer> path = new ArrayDeque<>();
        backTracking(1, k, n, path, res);
        return res;
    }

    private void backTracking(int start, int k, int n, Deque<Integer> path, List<List<Integer>> res) {
        // 剪枝
        if (n < 0)
            return;
        if (path.size() == k && n == 0){
            res.add(new ArrayList<>(path));
            return;
        }
        for (int i = start; i <= 9; i++) {
            path.addLast(i);
            backTracking(i + 1, k, n - i, path, res);
            path.removeLast();
        }
    }

    /**
     * 38 组合综合
     * @param candidates
     * @param target
     * @return
     */
    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        if (candidates == null || candidates.length == 0){
            return new ArrayList<>();
        }
        // 排序
        // 不过按照题目描述，数组元素是非负数，不用排序也可
        Arrays.sort(candidates);
        if (target < candidates[0]){
            return new ArrayList<>();
        }
        List<List<Integer>> res = new ArrayList<>();
        Deque<Integer> path = new LinkedList<>();
        dfs(candidates, 0, target, path, res);
        return res;
    }

    /**
     *
     * @param candidates
     * @param index
     * @param target
     * @param path
     * @param res
     */
    private void dfs(int[] candidates, int index, int target, Deque<Integer> path, List<List<Integer>> res) {
        if (target < 0 || index >= candidates.length){
            return;
        }
        if (target == 0){
            res.add(new ArrayList<>(path));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            path.addLast(candidates[i]);
            dfs(candidates, i, target - candidates[i], path, res);
            path.removeLast();
        }
    }


    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        if (candidates == null || candidates.length == 0){
            return new ArrayList<>();
        }
        // 排序
        // 不过按照题目描述，数组元素是非负数，不用排序也可
        Arrays.sort(candidates);
        if (target < candidates[0]){
            return new ArrayList<>();
        }
        boolean[] used = new boolean[candidates.length];
        List<List<Integer>> res = new ArrayList<>();
        Deque<Integer> path = new LinkedList<>();
        dfs2(candidates, 0, target, used, path, res);
        return res;
    }
    /**
     * 组合总和2
     * @param candidates
     * @param index
     * @param target
     * @param path
     * @param res
     */
    private void dfs2(int[] candidates, int index, int target, boolean[] used, Deque<Integer> path, List<List<Integer>> res) {
        if (target < 0 || index > candidates.length){
            return;
        }
        if (target == 0){
            res.add(new ArrayList<>(path));
            return;
        }
        for (int i = index; i < candidates.length; i++) {
            // 出现重复节点，同层的第一个节点已经被访问过，所以直接跳过(已经被赋值过true,再改为false了)
            if (i > 0 && candidates[i] == candidates[i - 1] && !used[i - 1]){
                continue;
            }
            path.addLast(candidates[i]);
            used[i] = true;
            dfs2(candidates, i + 1, target - candidates[i], used, path, res);
            path.removeLast();
            used[i] = false;
        }
    }

    @Test
    public void test(){
        combinationSum2(new int[]{2,5,2,1,2}, 5);
    }

    /**
     * 大佬题解
     */
    class Combination_38{

        List<List<Integer>> lists = new ArrayList<>();
        Deque<Integer> deque = new LinkedList<>();
        int sum = 0;

        public List<List<Integer>> combinationSum2(int[] candidates, int target) {
            //为了将重复的数字都放到一起，所以先进行排序
            Arrays.sort(candidates);
            //加标志数组，用来辅助判断同层节点是否已经遍历
            boolean[] flag = new boolean[candidates.length];
            backTracking(candidates, target, 0, flag);
            return lists;
        }

        public void backTracking(int[] arr, int target, int index, boolean[] flag) {
            if (sum == target) {
                lists.add(new ArrayList(deque));
                return;
            }
            for (int i = index; i < arr.length && arr[i] + sum <= target; i++) {
                //出现重复节点，同层的第一个节点已经被访问过，所以直接跳过
                if (i > 0 && arr[i] == arr[i - 1] && !flag[i - 1]) {
                    continue;
                }
                flag[i] = true;
                sum += arr[i];
                deque.push(arr[i]);
                //每个节点仅能选择一次，所以从下一位开始
                backTracking(arr, target, i + 1, flag);
                int temp = deque.pop();
                flag[i] = false;
                sum -= temp;
            }
        }

        public List<List<Integer>> combinationSum(int[] candidates, int target) {
            List<List<Integer>> res = new ArrayList<>();
            Arrays.sort(candidates); // 先进行排序
            backtracking(res, new ArrayList<>(), candidates, target, 0, 0);
            return res;
        }

        public void backtracking(List<List<Integer>> res, List<Integer> path, int[] candidates, int target, int sum, int idx) {
            // 找到了数字和为 target 的组合
            if (sum == target) {
                res.add(new ArrayList<>(path));
                return;
            }

            for (int i = idx; i < candidates.length; i++) {
                // 如果 sum + candidates[i] > target 就终止遍历
                if (sum + candidates[i] > target) break;
                path.add(candidates[i]);
                backtracking(res, path, candidates, target, sum + candidates[i], i);
                path.remove(path.size() - 1); // 回溯，移除路径 path 最后一个元素
            }
        }
    }
}
