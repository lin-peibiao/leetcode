package byCategory.linked_table.July;

import byCategory.linked_table.ListNode;
import org.junit.Test;

import java.util.*;

/**
 * 23、合并k个升序链表
 * @author xiaohu
 */
public class MergeKLists_23 {
    public ListNode mergeKLists(ListNode[] lists){
        if (lists == null || lists.length == 0)
            return null;
        // 哨兵链表
        ListNode sentry = new ListNode(0);
        ListNode cur = sentry;
        // 创建最小堆队列并重写compareTo()方法
        PriorityQueue<ListNode> pq = new PriorityQueue<>(new Comparator<ListNode>() {
            @Override
            public int compare(ListNode o1, ListNode o2) {
                return o1.val - o2.val;
            }
        });

        // 将所有链表添加到队列（链表头）
        for (ListNode listNode : lists) {
            if (listNode != null)
                pq.add(listNode);
        }

        // 将队列头推出添加到哨兵链表，知道队列为空
        while (!pq.isEmpty()){
            ListNode next = pq.poll();
            cur.next = next;
            cur = cur.next;
            // 注意注意 底层源码，如果添加null，会抛异常
            /*
                if (e == null)
                throw new NullPointerException();
             */
            if (next.next != null){
                pq.add(next.next);
            }
        }

        return sentry.next;
    }

    @Test
    public void test(){
        PriorityQueue<ListNode> pq = new PriorityQueue<>(new Comparator<ListNode>() {
            @Override
            public int compare(ListNode o1, ListNode o2) {
                return o1.val - o2.val;
            }
        });

        pq.add(new ListNode(1));

        List li = new ArrayList();
        li.add(new Object()); // 添加到尾部

        Set<String>  set = new HashSet<>();
        set.add(null);
    }
}
