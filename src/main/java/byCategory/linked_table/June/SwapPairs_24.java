package byCategory.linked_table.June;

import byCategory.linked_table.ListNode;

/**
 * 24、链表节点两两交换
 * @author xiaohu
 */
public class SwapPairs_24 {
    public ListNode swapPairs(ListNode head){
        if (head == null) return null;
        ListNode sentry = new ListNode(0,head);
        ListNode tem = sentry;
        while (tem.next != null && tem.next.next != null){
            ListNode node1 = tem.next;
            ListNode node2 = tem.next.next;
            tem.next = node2;
            node1.next = node2.next;
            node2.next = node1;
            tem = node1;
        }
        return sentry.next;
    }
}
