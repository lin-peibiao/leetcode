package byCategory.linked_table.June;

import byCategory.linked_table.ListNode;

import java.util.HashSet;
import java.util.Set;

/**
 * 142、环形链表2
 * @author xiaohu
 */
public class DetectCycle_142 {

    /**
     * 最容易想到的方法，空间复杂度高达O(n)
     * @param head
     * @return
     */
    public ListNode detectCycle(ListNode head){
        if (head == null)
            return null;
        Set<ListNode> set = new HashSet<>();
        while(head != null && !set.contains(head)){
            set.add(head);
            head = head.next;
        }

        return head;
    }


    /**
     * 空间复杂度好像是下去了，但是空间复杂度提高，提交更是143ms。。。。。。
     * @param head
     * @return
     */
    public ListNode detectCycle1(ListNode head){
        if (head == null)
            return null;
        ListNode sentry = new ListNode(0,head);
        while (head != null){
            ListNode tem = sentry.next;
            while (tem != null){
                if (tem == head.next)
                    return head.next;
                if (tem == head)
                    break;
                tem = tem.next;
            }

            head = head.next;
        }

        return null;
    }


    /**
     * 1、判断是否环形链表
     * 2、寻找环形链表入口
     *
     * 大佬的题解：快慢指针
     * @param head
     * @return
     */
    public ListNode niuBi(ListNode head){
        ListNode slow = head;
        ListNode fast = head;
        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next.next;
            if (slow == fast) {// 有环
                ListNode index1 = fast;
                ListNode index2 = head;
                // 两个指针，从头结点和相遇结点，各走一步，直到相遇，相遇点即为环入口
                while (index1 != index2) {
                    index1 = index1.next;
                    index2 = index2.next;
                }
                return index1;
            }
        }
        return null;
    }
}
