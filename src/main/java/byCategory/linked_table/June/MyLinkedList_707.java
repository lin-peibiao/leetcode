package byCategory.linked_table.June;

/**
 * 707、设计链表
 * @author xiaohu
 */


/**
 * 单链表
 */
class LinkedList{
    int val;
    LinkedList next;

    public LinkedList() {

    }

    public LinkedList(int val){
        this.val = val;
    }

    public LinkedList(int val, LinkedList node){
        this.val = val;
        this.next = node;
    }
}
public class MyLinkedList_707 {



    /**
     * 获取第i个节点值
     * @param index
     * @return
     */
    public int get(LinkedList head, int index) {
        if (head == null || index < 0 || index > length(head))
            return -1;
        int i = 0;
        while(i++ < index)
            head = head.next;
        return head.val;
    }

    /**
     * 添加元素（头插法）
     * @param val
     */
    public void addAtHead(LinkedList head, int val) {
        LinkedList node = new LinkedList(val,head);
        head = node;
    }

    /**
     * 尾插法
     * @param val
     */
    public void addAtTail(LinkedList head, int val) {
        if (head == null)
            head = new LinkedList(val);
        while (head.next != null)
            head = head.next;
        head.next = new LinkedList(val);

    }

    /**
     * 在指定位置添加元素
     * @param index
     * @param val
     */
    public void addAtIndex(LinkedList head, int index, int val) {
        if (index < 0)
            addAtHead(head, val);
        else if (index >= length(head))
            addAtTail(head, val);
        else {
            while (--index > 0)
                head = head.next;
            LinkedList node = new LinkedList(val,head.next);
            head.next = node;
        }


    }

    /**
     * 删除指定位置的元素
     * @param index
     */
    public void deleteAtIndex(LinkedList head, int index) {
        if (index < 0 || index > length(head))
            return;
        while (--index > 0)
            head = head.next;
        head.next = head.next.next;
    }

    /**
     * 获取链表的长度
     * @param head
     * @return
     */
    public int length(LinkedList head){
        if (head == null)
            return 0;
        int len = 1;
        while (head.next != null){
            ++len;
            head = head.next;
        }
        return len;
    }
}

/**
 * Your MyLinkedList object will be instantiated and called as such:
 * MyLinkedList obj = new MyLinkedList();
 * int param_1 = obj.get(index);
 * obj.addAtHead(val);
 * obj.addAtTail(val);
 * obj.addAtIndex(index,val);
 * obj.deleteAtIndex(index);
 */