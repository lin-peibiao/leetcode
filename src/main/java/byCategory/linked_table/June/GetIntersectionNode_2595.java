package byCategory.linked_table.June;

import byCategory.linked_table.ListNode;

/**
 * 2595、寻找共同节点
 * @author xiaohu
 */
public class GetIntersectionNode_2595 {
    public ListNode getIntersectionNode(ListNode headA, ListNode headB){
        if (headA == null || headB == null)  return null;
        while (headA != null) {
            ListNode node = headB;
            while (node != null){
                if (headA == node)
                    return node;
                node = node.next;
            }
            headA = headA.next;
        }
        return null;
    }

    /**
     * 优化 时间复杂度O(m+n)
     * 先计算节点个数，再将长的head移动到短的末尾一致的位置，开始比较A、B两个节点，相同返回，不同都指向next节点
     * @param headA
     * @param headB
     * @return
     */
    public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
        ListNode curA = headA;
        ListNode curB = headB;
        int lenA = 0, lenB = 0;
        while (curA != null) { // 求链表A的长度
            lenA++;
            curA = curA.next;
        }
        while (curB != null) { // 求链表B的长度
            lenB++;
            curB = curB.next;
        }
        curA = headA;
        curB = headB;
        // 让curA为最长链表的头，lenA为其长度
        if (lenB > lenA) {
            //1. swap (lenA, lenB);
            int tmpLen = lenA;
            lenA = lenB;
            lenB = tmpLen;
            //2. swap (curA, curB);
            ListNode tmpNode = curA;
            curA = curB;
            curB = tmpNode;
        }
        // 求长度差
        int gap = lenA - lenB;
        // 让curA和curB在同一起点上（末尾位置对齐）
        while (gap-- > 0) {
            curA = curA.next;
        }
        // 遍历curA 和 curB，遇到相同则直接返回
        while (curA != null) {
            if (curA == curB) {
                return curA;
            }
            curA = curA.next;
            curB = curB.next;
        }
        return null;
    }
}
