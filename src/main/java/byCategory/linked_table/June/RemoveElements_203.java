package byCategory.linked_table.June;

import byCategory.linked_table.ListNode;


/**
 * 203、删除链表元素
 * @author xiaohu
 */
public class RemoveElements_203 {
    /**
     * 先生成一个哑节点，声明为哨兵，站岗的，最后只需要返回sentry.next
     * 操作的都是head,不会涉及到哨兵节点
     * @param head
     * @param val
     * @return
     */
    public ListNode removeElements(ListNode head, int val){
        ListNode sentry = new ListNode(0,head);
        ListNode pre = sentry;
        ListNode cur = head;
        while (cur != null){ // 当前节点不为空
            int curVal = cur.val;
            if (curVal == val){
                pre.next  = cur.next; // 将当前节点删除
            }
            else
                pre = pre.next;
            cur = cur.next;
        }
        return sentry.next;
    }


}
