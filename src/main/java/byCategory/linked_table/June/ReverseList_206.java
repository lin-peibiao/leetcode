package byCategory.linked_table.June;

import byCategory.linked_table.ListNode;

/**
 * 203、反转链表
 * @author xiaohu
 */
public class ReverseList_206 {

    /**
     * 从头到尾遍历，取出每一个节点放在节点头部
     * @param head
     * @return
     */
    public ListNode reverseList(ListNode head) {
        // 构建一个新节点
        ListNode sentry = null;
        // 取出每一个节点放在sentry的前面
        while (head != null){
            ListNode tem = head.next;
            head.next = sentry;
            sentry = head;
            head = tem;
        }
        return sentry;
    }
}
