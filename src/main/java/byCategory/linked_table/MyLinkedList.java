package byCategory.linked_table;

/**
 * @author xiaohu
 * @date 2023/06/07/ 15:39
 * @description
 */
public class MyLinkedList {
    private ListNode dummyHead;
    private int size;

    public MyLinkedList(){
        dummyHead = new ListNode();
        size = 0;
    }
    public void addToHead(int val){
        addToIndex(0, val);
    }

    public void addToTail(int val){
        addToIndex(size, val);
    }

    public void deleteIndex(int index){
        if (index < 0 || index >= size){
            return;
        }
        if (index == 0){
            dummyHead = dummyHead.next;
        }
        ListNode pre = dummyHead;
        for (int i = 0; i < index; i++) {
            pre = pre.next;
        }
        pre.next = pre.next.next;
        size --;

    }
    public int get(int index){
        if (index < 0 || index >= size){
            return -1;
        }
        ListNode pre = dummyHead;
        for (int i = 0; i < index; i++) {
            pre = pre.next;
        }
        return pre.next.val;
    }

    public void addToIndex(int index, int val){
        ListNode pre = dummyHead;
        for (int i = 0; i < index; i++) {
            pre = pre.next;
        }
        ListNode toAdd = new ListNode(val);
        toAdd.next = pre.next;
        pre.next = toAdd;
        size ++;
    }

}
