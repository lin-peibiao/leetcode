package byCategory.string;

import org.junit.Test;

/**
 * @author xiaohu
 * @date 2022/07/22/ 18:01
 * @description 剑指offer 58 左旋字符串2
 */
public class ReverseLeftWords_of58 {
    /**
     * api调用法
     * @param s
     * @param n
     * @return
     */
    public String reverseLeftWords(String s, int n){
        if (s == null || s.length() == 0 || n <= 0 || n >= s.length()) return s;
        String sub1 = s.substring(0,n);
        String sub2 = s.substring(n,s.length());
        return sub2 + sub1;
    }

    /**
     * 分三步：
     * 1、翻转0-n的字符
     * 2、翻转n-末尾的字符
     * 3、翻转所有的字符
     * @param n
     * @param s
     * @return
     */
    public String reverseLeftWords(int n, String s) {
        int len=s.length();
        StringBuilder sb=new StringBuilder(s);
        reverseString(sb,0,n-1);
        reverseString(sb,n,len-1);
        return sb.reverse().toString();
    }

    /**
     * 翻转字符串的某一段
     * @param sb
     * @param start
     * @param end
     */
    public void reverseString(StringBuilder sb, int start, int end) {
        while (start < end) {
            char temp = sb.charAt(start);
            sb.setCharAt(start, sb.charAt(end));
            sb.setCharAt(end, temp);
            start++;
            end--;
        }
    }

    @Test
    public void test() {
        String s = "abcdefghijk";
        System.out.println(s);
        System.out.println(reverseLeftWords(s, 3));
    }
}
