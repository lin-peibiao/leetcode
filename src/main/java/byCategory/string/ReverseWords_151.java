package byCategory.string;

/**
 * 151、翻转字符串中的单词
 */
public class ReverseWords_151 {
    public String reverseWords(String s){
        if (s == null || s.length() == 0) return "";
        StringBuilder sb = new StringBuilder();
        for (int i = s.length() - 1; i >= 0;) {
            char ch = s.charAt(i);
            if (ch == ' '){
                i--;
            }
            else {
                int r = i;
                int l = 0;
                while (r >= 0 && s.charAt(r) != ' ') --r;
                l = r + 1;
                sb.append(s.substring(l,i+1) + " ");
                i = r;
            }
        }
        return sb.substring(0,sb.length()-1);
    }
}
