package byCategory.string;

/**
 * 剑指offer第五题、替换空格
 * 将字符串中出现的空格全部替换为20%
 * 思路： 扩容 + 替换 模拟
 * 当然也可以看看String自带的API replace()方法的源码，虽然看不太懂
 */
public class ReplaceSpace_Of05 {
    public String replaceSpace(String s){
        if (s == null || s.length() == 0) return "";
        // 遍历s中空格的数量,用于扩充空间，每有一个空格，就多添加两个位置
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') sb.append("  ");
        }
        // 没有空格返回s
        if (sb.length() == 0) return s;

        String str = s + sb;
        int r = str.length() - 1; // 扩容数组最后一个位置
        int l = s.length() - 1; // 原字符串最后一个字符
        char[] sA = str.toCharArray();

        // 替换
        while (l >= 0){
            if (sA[l] == ' '){
                sA[r--] = '0';
                sA[r--] = '2';
                sA[r--] = '%';
            }else
                sA[r--] = sA[l];
            --l;
        }
        return new String(sA);
    }
}
