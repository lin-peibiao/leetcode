package byCategory.string;

/**
 * @author xiaohu
 * @date 2022/07/22/ 18:41
 * @description 28、实现strStr()函数，也就是java种indexOf()方法
 */
public class StrStr_28 {

    // 纯粹的模仿
    public int strStr(String haystack, String needle) {
        if (needle == null || needle.length() == 0)
            return 0;
        if (haystack == null || haystack.length() == 0)
            return -1;
        int index = 0;
        for (int i = 0; i < haystack.length(); i++) {
            if (haystack.charAt(i) == needle.charAt(0)){
                int index1 = i;
                int index2 = 0;
                while (index1 < haystack.length()   // 不超出范围
                        && index2 < needle.length() // 不超出范围
                        && haystack.charAt(index1) == needle.charAt(index2)){ // 字符相同
                    ++index1;
                    ++index2;
                }
                if (index2 == needle.length())
                    return i;
            }
        }

        return -1;
    }
}
