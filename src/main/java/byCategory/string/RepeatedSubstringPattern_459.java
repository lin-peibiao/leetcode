package byCategory.string;

import org.junit.Test;

/**
 * @author xiaohu
 * @date 2022/07/24/ 0:27
 * @description 459、判断某字符串能否由其子串组成
 */
public class RepeatedSubstringPattern_459 {
    public boolean repeatedSubstringPattern(String s){
        if (s == null) return true;
        int len = s.length();
        int[] next = new int[len];

        int j = 0;
        for (int i = 1; i < len; i++) {
            char ch = s.charAt(i);
            // 先处理不匹配的情况
            while (j > 0 && s.charAt(j) != ch) j = next[j];
            if (s.charAt(j) == ch){
                ++j;
                next[i] = next[i-1] + 1;
            }
        }

        return next[len-1] != 0 && len % (len - next[len-1]) == 0;
    }

    /**
     * 大佬的题解
     * @param s
     * @return
     */
    public boolean repeatedSubstringPattern1(String s){
        if (s == null) return true;
        int len = s.length();
        int[] next = new int[len+1];
        s = " " + s;
        char[] chars = s.toCharArray();
        // 构造 next 数组过程，j从0开始(空格)，i从2开始
        for (int i = 2, j = 0; i <= len; i++) {
            // 匹配不成功，j回到前一位置 next 数组所对应的值
            while (j > 0 && chars[i] != chars[j + 1]) j = next[j]; // 不匹配的情况
            // 匹配成功，j往后移
            if (chars[i] == chars[j + 1]) j++; // 相等的情况
            // 更新 next 数组的值
            next[i] = j;
        }

        return next[len] != 0 && len % (len - next[len]) == 0;
    }

    @Test
    public void test(){
        System.out.println(5 % 0);
    }
}
