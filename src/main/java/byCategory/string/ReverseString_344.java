package byCategory.string;

import org.junit.Test;

import java.util.Arrays;

/**
 * 344、翻转字符串 通过原地算法
 * @author xiaohu
 */
public class ReverseString_344 {
    public void reverseString(char[] s){
        if (s == null || s.length == 0)
            return ;
        int len = s.length;
        for (int i = 0; i < len / 2; i++) {
            char tem = s[i];
            s[i] = s[len - i -1 ];
            s[len - i - 1] = tem;
        }
    }

    /**
     * 大佬题解
     * 看过一个非常类似的题目:如何在不知都同事的工资情况下取得所有同事的平均工资
     * @param s
     */
    public void reverseString1(char[] s) {
        int l = 0;
        int r = s.length - 1;
        while (l < r) {
            s[l] ^= s[r];  //构造 a ^ b 的结果，并放在 a 中
            s[r] ^= s[l];  //将 a ^ b 这一结果再 ^ b ，存入b中，此时 b = a, a = a ^ b
            s[l] ^= s[r];  //a ^ b 的结果再 ^ a ，存入 a 中，此时 b = a, a = b 完成交换
            l++;
            r--;
        }
    }

    /**
     * 541、翻转数组2，这道题理解就花了我很多时间，他妈的傻逼翻译。
     */


    /**
     * 大佬题解
     * 两个很精妙的点
     * 一个在for循环，+= 2k,与end指针巧妙合作解决足够k不够2k的情况
     * 一个就是end指针的取值，解决了不够k的情况
     * @param s
     * @param k
     * @return
     */
    public String reverseStr2(String s, int k) {
        char[] ch = s.toCharArray();
        for(int i = 0; i < ch.length; i += 2 * k){
            int start = i;
            //这里是判断尾数够不够k个来决定end指针的位置
            int end = Math.min(ch.length - 1, start + k - 1);
            //用异或运算反转
            while(start < end){
                ch[start] ^= ch[end];
                ch[end] ^= ch[start];
                ch[start] ^= ch[end];
                start++;
                end--;
            }
        }
        return new String(ch);
    }

    public String reverseStr(String s, int k){
        int len = s.length();
        String subS = "";

        // 先是计算不到k的，翻转所有
        if (len < k)
            return subReverse(s);
        // 能计算到4k的，只需要翻转前k个

        // 能计算到2k，但是计算不到4k的，再翻转剩余的前k个
        if (len - 2 * k > 2 * k){
            subReverse(s.substring(0,k));
        }else{

        }
        return "";
    }

    private String subReverse(String sub){
        int r = sub.length() - 1;
        int l = 0;
        char[] subArray = sub.toCharArray();
        while (r > l){
            subArray[l] ^= subArray[r];
            subArray[r] ^= subArray[l];
            subArray[l] ^= subArray[r];
            --r;
            ++l;
        }
        return Arrays.toString(subArray);
    }

    @Test
    public void test(){
        subReverse("");
    }

}
