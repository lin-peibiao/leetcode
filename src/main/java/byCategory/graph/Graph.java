package byCategory.graph;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * @author xiaohu
 * @date 2022/12/09/ 13:20
 * @description 图构建 两种遍历方式
 * 使用邻接链表实现
 */
public class Graph {
    /**
     * 表示每一个顶点
     */
    private List<List<Integer>> graph;

    /**
     * 表示每一个顶点是否被访问过
     */
    private boolean[] isVisited;

    public Graph(int n){
        // 顶点数量
        this.graph = new ArrayList<>(n);
        // 新建链表保存顶点关系
        for (int i = 0; i < n; i++){
            this.graph.add(new ArrayList<>());
        }
        isVisited = new boolean[n];
    }

    /**
     * 添加边
     * @param start 起始顶点
     * @param end 终点顶点
     */
    public void addEdge(int start, int end){
        graph.get(start).add(end);
    }

    public void removeEdge(int start, int end){
        graph.get(start).remove(end);
    }

    /**
     * TODO
     * 添加顶点到图中
     * @param start 表示出度到的顶点
     * @param end 表示入度至此的顶点
     * @param value 表示要插入的顶点值
     */
    public void addVertex(int start, int end, int value){

    }


    /**
     * 深度优先遍历
     * @param graph
     */
    public void dfs(Graph graph){
        for (int i = 1; i < graph.graph.size(); i++){
            if (!graph.isVisited[i]){
                // 未被访问过则遍历它
                dfsTraversal(graph, i);
            }
        }
    }

    private void dfsTraversal(Graph graph, int v) {
        if (graph.isVisited[v]){
            return;
        }
        graph.isVisited[v] = true;
        System.out.print(v + "->");
        // 遍历该顶点的邻接节点
        for (Integer neighbor : graph.graph.get(v)){
            if (!graph.isVisited[neighbor]){
                dfsTraversal(graph, neighbor);
            }
        }
    }

    /**
     * 广度优先遍历
     * @param graph
     */
    public void bfs(Graph graph){
        for (int i = 1; i < graph.graph.size(); i++){
            if (!graph.isVisited[i]){
                // 未被访问过则遍历它
                bfsTraversal(graph, i);
            }
        }
    }

    private void bfsTraversal(Graph graph, int i) {
        if (graph.isVisited[i]){
            return;
        }
        // 层次遍历
        System.out.print(i + "->");
        graph.isVisited[i] = true;
        Deque<List<Integer>> queue = new LinkedList<>();
        queue.offerFirst(graph.graph.get(i));
        while (!queue.isEmpty()){
            // 队列不为空时，弹出
            List<Integer> neighbor = queue.pop();
            for (Integer next : neighbor){
                // 入队
                if (!graph.isVisited[next]) {
                    graph.isVisited[next] = true;
                    queue.offerFirst(graph.graph.get(next));
                    System.out.print(next + "->");
                }
            }
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(7);
        List<Integer> v0 = graph.graph.get(1);
        v0.add(2);
        v0.add(4);
        List<Integer> v1 = graph.graph.get(2);
        v1.add(5);
        List<Integer> v2 = graph.graph.get(3);
        v2.add(6);
        v2.add(5);
        List<Integer> v3 = graph.graph.get(4);
        v3.add(2);
        List<Integer> v4 = graph.graph.get(5);
        v4.add(4);
        List<Integer> v5 = graph.graph.get(6);
        v5.add(6);
        System.out.println("bfs");
        graph.bfs(graph);
    }

}
