package byCategory.stack;

import java.util.Deque;
import java.util.LinkedList;

/**
 * @author xiaohu
 * @date 2022/08/03/ 22:24
 * @description
 */

/**
 * 思路：实现一个叫单调队列的东西，单调队列就是递增或者递减队列。
 * 该递减单调队列的实现方式：
 * pop: 如果将要push进去的元素比队列出口元素大，则将之前的所有元素都pop
 * push: 如果满足将要push的元素比队尾元素小，直接push，否则以上
 * 遍历数组，首先将前k个元素添加到队列。如果滑出元素等于队列出口元素，将出口元素添加到返回结果
 */
public class MaxSlidingWindow_239 {
    public int[] maxSlidingWindow(int[] nums, int k) {
        if (nums.length == 1) {
            return nums;
        }
        int len = nums.length - k + 1;
        //存放结果元素的数组
        int[] res = new int[len];
        int num = 0;
        //自定义队列
        MyQueue1 myQueue = new MyQueue1();
        //先将前k的元素放入队列
        for (int i = 0; i < k; i++) {
            myQueue.add(nums[i]);
        }
        res[num++] = myQueue.peek();
        for (int i = k; i < nums.length; i++) {
            //滑动窗口移除最前面的元素，移除是判断该元素是否放入队列
            myQueue.poll(nums[i - k]);
            //滑动窗口加入最后面的元素
            myQueue.add(nums[i]);
            //记录对应的最大值
            res[num++] = myQueue.peek();
        }
        return res;
    }
}
class MyQueue1 {
    Deque<Integer> deque = new LinkedList<>();
    //弹出元素时，比较当前要弹出的数值是否等于队列出口的数值，如果相等则弹出
    //同时判断队列当前是否为空
    void poll(int val) {
        if (!deque.isEmpty() && val == deque.peek()) {
            deque.poll();
        }
    }
    //添加元素时，如果要添加的元素大于入口处的元素，就将入口元素弹出
    //保证队列元素单调递减
    //比如此时队列元素3,1，2将要入队，比1大，所以1弹出，此时队列：3,2
    void add(int val) {
        while (!deque.isEmpty() && val > deque.getLast()) {
            deque.removeLast();
        }
        deque.add(val);
    }
    //队列队顶元素始终为最大值
    int peek() {
        return deque.peek();
    }
}
