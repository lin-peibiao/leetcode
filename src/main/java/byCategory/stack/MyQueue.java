package byCategory.stack;


import java.util.EmptyStackException;
import java.util.Stack;

/**
 * @author xiaohu
 * @date 2022/07/24/ 15:36
 * @description 使用栈实现先入先出队列
 */
public class MyQueue {

    private Stack<Integer> popStack = new Stack<>();
    private Stack<Integer> pushStack = new Stack<>();

    public Stack<Integer> getPopStack() {
        return popStack;
    }

    public void setPopStack(Stack<Integer> popStack) {
        this.popStack = popStack;
    }

    public Stack<Integer> getPushStack() {
        return pushStack;
    }

    public void setPushStack(Stack<Integer> pushStack) {
        this.pushStack = pushStack;
    }

    public MyQueue() {}

    public MyQueue(Stack<Integer> pushStack, Stack<Integer> popStack){
        this.popStack = popStack;
        this.pushStack = pushStack;
    }

    public void push(int x) {
        pushStack.push(x);
    }


    /**
     * pop()和push()代码差不多。
     * 考虑代码外提封装为方法，提高代码复用。
     * @return
     */
    public int pop() {
        transfer();
        return popStack.pop();
    }

    public int peek() {
        transfer();
        return popStack.peek();
    }

    public boolean empty() {
        return popStack.isEmpty() && pushStack.isEmpty();
    }

    /**
     * 转移两栈内元素
     */
    public void transfer(){
        if (popStack.isEmpty() && pushStack.isEmpty())
            throw new EmptyStackException();
        if (popStack.isEmpty()){
            while(! pushStack.isEmpty()){
                popStack.push(pushStack.pop());
            }
        }
    }
}
