package byCategory.stack;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Stack;

/**
 * @author xiaohu
 * @date 2022/07/30/ 20:19
 * @description 删除字符串中所有的相邻重复项
 */
public class RemoveDuplicates_1047 {


    public String removeDuplicates(String s){
        if (s == null || s.length() == 0)
            return "";
        Deque<Character> stack = new ArrayDeque<>();
        // 嘿嘿、我喜欢从后面来
        for (int i = s.length() - 1; i >= 0; i--) {
            char ch = s.charAt(i);
            if (!stack.isEmpty() && ch == stack.peek())
                stack.pop();
            else
                stack.push(ch);
        }
        String res = "";
        while (!stack.isEmpty()) {
            res += stack.pop();
        }

//        for (Character character : stack) {
//            res += character;
//        }
        return res;
    }

    @Test
    public void test(){
        System.out.println(removeDuplicates("aaabcbccc"));
    }

}
