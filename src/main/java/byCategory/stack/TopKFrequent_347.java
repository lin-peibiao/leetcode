package byCategory.stack;

/**
 * @author xiaohu
 * @date 2022/08/04/ 23:43
 * @description 返回数组中频率排行榜前k个
 */

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * 思路：
 * 1、统计元素出现频率 使用map
 * 2、对频率进行排序
 * 3、拿到出现频率前k高的k个元素并返回
 * 针对第2、3步，可以使用构建最小堆的方式（优先级队列）
 */
public class TopKFrequent_347 {
    public int[] topKFrequent(int[] nums, int k){
        if (nums == null || nums.length == 0 || k == 0)
            return null;
        int[] result = new int[k];
        // 1 统计
        Map<Integer, Integer> map = new HashMap<>();
        for (int num : nums) {
            if (map.containsKey(num)){
                map.put(num,map.get(num) + 1);
            }else{
                map.put(num, 1);
            }
        }

        // 2 排序
        Set<Map.Entry<Integer, Integer>> entries = map.entrySet();// 把频率拿出来
        // 根据map的value值，构建于一个大顶堆（o1 - o2: 小顶堆， o2 - o1 : 大顶堆）
        PriorityQueue<Map.Entry<Integer, Integer>> queue = new PriorityQueue<>((o1, o2) -> o2.getValue() - o1.getValue());
        for (Map.Entry<Integer, Integer> entry : entries) {
            queue.offer(entry);// 将对应的key值通过频率取出来
        }
        for (int i = k - 1; i >= 0; i--) {
            result[i] = queue.poll().getKey();
        }
        return result;

    }
}
