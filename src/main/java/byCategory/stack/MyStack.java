package byCategory.stack;

import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author xiaohu
 * @date 2022/07/26/ 21:57
 * @description 使用两个队列实现栈
 */
public class MyStack {
    // queue2做备份
    Queue<Integer> queue1 = null;
    Queue<Integer> queue2 = null;

    public MyStack() {
        this.queue1 = new ArrayDeque<>();
        this.queue2 = new ArrayDeque<>();
    }

    public void push(int x) {
        queue1.offer(x);
    }


    /**
     * @return
     */
    public int pop() {
        if (queue1.size() > 1) {
            return op();
        }

        return queue1.poll();

    }

    public int top() {
        if (queue1.size() > 1) {
            int top = op();
            queue1.offer(top);
            return top;
        }

        return queue1.peek();
    }

    public boolean empty() {
        return queue1.isEmpty();
    }


    /**
     * 有点模糊的操作，主要是 出栈 队列元素个数大于1时
     * @return
     */
    public int op() {
        // 将q1元素poll只剩一个
        while (queue1.size() != 1) {
            queue2.offer(queue1.poll());
        }
        int pop = queue1.poll();
        // 将元素push回q1
        while (!queue2.isEmpty()) {
            queue1.offer(queue2.poll());
        }
        return pop;
    }

}


/**
 * 大佬的结构  很清晰的思路
 */
class MyStack1 {

    Queue<Integer> queue1; // 和栈中保持一样元素的队列
    Queue<Integer> queue2; // 辅助队列

    /** Initialize your data structure here. */
    public MyStack1() {
        queue1 = new LinkedList<>();
        queue2 = new LinkedList<>();
    }

    /** Push element x onto stack. */
    public void push(int x) {
        queue2.offer(x); // 先放在辅助队列中
        while (!queue1.isEmpty()){
            queue2.offer(queue1.poll());
        }
        Queue<Integer> queueTemp;
        queueTemp = queue1;
        queue1 = queue2;
        queue2 = queueTemp; // 最后交换queue1和queue2，将元素都放到queue1中
    }

    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        return queue1.poll(); // 因为queue1中的元素和栈中的保持一致，所以这个和下面两个的操作只看queue1即可
    }

    /** Get the top element. */
    public int top() {
        return queue1.peek();
    }

    /** Returns whether the stack is empty. */
    public boolean empty() {
        return queue1.isEmpty();
    }
}
