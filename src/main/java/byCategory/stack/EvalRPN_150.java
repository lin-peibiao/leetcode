package byCategory.stack;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.TreeSet;

/**
 * @author xiaohu
 * @date 2022/07/30/ 21:13
 * @description 实现逆波兰表达式求值
 */
public class EvalRPN_150 {
    public int evalRPN(String[] tokens){
        if (tokens == null || tokens.length == 0)
            return 0;
        Deque<String> deque = new ArrayDeque<>();
        int res = 0;
        for (String token : tokens) {
            if (isDigit(token)){
                deque.push(token);
            }else{
                int b = Integer.parseInt(deque.pop());
                int a = Integer.parseInt(deque.pop());
                switch (token){
                    case "+" :
                        res =  a + b;
                        break;
                    case "-" :
                        res = a - b;
                        break;
                    case "*" :
                        res = a * b;
                        break;
                    case "/" :
                        res = a / b;
                        break;
                }
                deque.push("" + res);
            }
        }

        return res;
    }

    // 时间耗费在这个判断上
    public boolean isDigit(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Test
    public void test(){
        TreeSet<Integer> set = new TreeSet<Integer>();
        TreeSet<Integer> subSet = new TreeSet<Integer>();
        for(int i=606;i<613;i++){
            if(i%2==0){
                set.add(i);
            }
        }
        subSet = (TreeSet)set.subSet(608,true,611,true);
        set.add(609);
        subSet.add(611);
        System.out.println(set+" "+subSet);
    }
}
