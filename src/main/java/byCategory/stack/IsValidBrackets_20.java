package byCategory.stack;

import java.util.*;

/**
 * @author xiaohu
 * @date 2022/07/30/ 19:40
 * @description 判断是否为有效的括号组合
 */
public class IsValidBrackets_20 {
    public boolean isValid(String s){
        if (s.length() < 2) return false;

        Map<Character, Character> map = new HashMap<>();
        map.put(')', '(');
        map.put(']', '[');
        map.put('}', '{');

        Stack<Character> stack = new Stack<>();
        // 遍历字符串s的所有字符
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            // 判断是否为右括号
            if (ch == ')' || ch == ']' || ch == '}'){
                if (stack.isEmpty()) return false;
                // 判断是否与栈顶元素匹配
                if (map.get(ch) == stack.peek())
                    stack.pop();
                else
                    return false;
            }else
                stack.push(ch);
        }

        return stack.size() == 0;
    }

    /**
     * 大佬的题解也是挺巧妙的
     * @param s
     * @return
     */
    public boolean isValid1(String s) {
        Deque<Character> deque = new LinkedList<>();
        char ch;
        for (int i = 0; i < s.length(); i++) {
            ch = s.charAt(i);
            //碰到左括号，就把相应的右括号入栈
            if (ch == '(') {
                deque.push(')');
            }else if (ch == '{') {
                deque.push('}');
            }else if (ch == '[') {
                deque.push(']');
            } else if (deque.isEmpty() || deque.peek() != ch) {
                return false;
            }else {//如果是右括号判断是否和栈顶元素匹配
                deque.pop();
            }
        }
        //最后判断栈中元素是否匹配
        return deque.isEmpty();
    }
}
