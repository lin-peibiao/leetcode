package byCategory.Array.may;

import org.junit.Test;

/**
 * 162、寻找数组中的峰值，要求时间复杂度为O(log n)
 * 明显就是要用二分法查找
 *
 * @author xiaohu
 */
public class FindPeakElement {
    /**
     * 二分查找 满足时间复杂度的要求
     * 由于默认下标为-1，n为负的无穷大，所以只要数组长度不为零，就有峰值
     * 当nums[i] > nums[i+1]时，i之前一定存在峰值
     * 当nums[i] < nums[i+1]时，i+1之后一定存在峰值
     * @param nums
     * @return
     */
    public int findPeakElement(int nums[]){
        int left = 0,right = nums.length-1;
        int res = nums[0];
        while(left < right){
            int mid =(right + left) >> 1;
            if (mid + 1 < nums.length && nums[mid] < nums[mid+1]){
                left = mid + 1;
            }else if (mid + 1 < nums.length && nums[mid] > nums[mid+1]){
                right = mid;
            }
            res = nums[mid] > nums[mid+1] ? mid : mid + 1;
        }
        return res;
    }

    @Test
    public void test(){
        int nums[] = new int[]{1,2,1,3,5,6,4};
        System.out.println(findPeakElement(nums));
    }
}
