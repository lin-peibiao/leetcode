package byCategory.Array.may;

import org.junit.Test;

/**
 * 189、轮转数组
 *
 * @author xiaohu
 */
public class Rotate_189 {

    /**
     * 官方思路 ，翻转数组
     * 先将整个数组翻转，再分别翻转0—k%len -1,k%len - len 之间的元素
     * @param nums
     * @param k
     */
    public void rotate1(int[] nums, int k) {
        k %= nums.length;
        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.length - 1);
    }

    public void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start += 1;
            end -= 1;
        }
    }


    /**
     * 错误的思路，花了我一个多小时
     * @param nums
     * @param k
     */
    public void rotate(int nums[], int k){
        if (k == 0 || k == nums.length)
            return;
        int len = nums.length;
        k = k % len;
        int start = len - k;
        for (int i = 0; i < len -1 ; i++) {
            if (start % len == 0)
                start = len - k + 1;
            int tem = nums[i];
            nums[i] = nums[start];
            nums[start] = tem;
            ++start;
        }
    }

    @Test
    public void test(){
        int nums[] = new int[]{1,2,3,4,5,6,7};
        rotate(nums,4);
        for (int num : nums) {
            System.out.print("\t"+num);
        }
    }

}
