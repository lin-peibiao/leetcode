package byCategory.Array.may;


import org.junit.Test;

import java.util.Stack;

/**
 * 150、逆波兰表达式求值（也叫后缀表达式）
 *
 * @author xiaohu
 */
public class EvalRPN {
    /**
     * 使用栈来操作
     *
     * 后面识别运算符号换成switch会更快
     * @param tokens
     * @return
     */
    public int evalRPN(String[] tokens) {
        int res = 0;
        if (tokens.length == 0)
            return 0;
        Stack<Integer> holder = new Stack<>();

        for (String token : tokens) {
            if (isDigit(token))
                holder.push(Integer.parseInt(token));
            else {
                int b = holder.pop(),a = holder.pop();
                switch (token){
                    case "+":
                        holder.push(a+b);
                        break;
                    case "-":
                        holder.push(a-b);
                        break;
                    case "*":
                        holder.push(a*b);
                        break;
                    case "/":
                        holder.push(a/b);
                        break;
                }
            }

        }

/*
        for (int i = 0; i < tokens.length; i++) {
            if (isDigit(tokens[i]))
                holder.push(Integer.parseInt(tokens[i]));
            else{
                int b = holder.pop(),a = holder.pop();
                if (tokens[i].equals("+")){
                    holder.push(a+b);
                    continue;

                }
                if (tokens[i].equals("-")) {
                    holder.push(a - b);
                    continue;
                }
                if (tokens[i].equals("*")) {
                    holder.push(a * b);
                    continue;
                }
                if (tokens[i].equals("/")){
                    holder.push(a/b);
                    continue;
                }

            }
        }

 */
        return holder.pop();
        // ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
    }

    /**
     * 使用正则表达式判断字符串是否为数字
     * 速度更快
     * @param str
     * @return
     */
    public  boolean isInteger(String str) {
        //?:0或1个, *:0或多个, +:1或多个
        return str.matches("-?[0-9]+.？[0-9]*");
    }

    public boolean isNumber(String token) {
        return !("+".equals(token) || "-".equals(token) || "*".equals(token) || "/".equals(token));
    }

    /**
     * 使用类型转换判断是否为数字
     */
    public boolean isDigit(String str) {
        try {
            Integer.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Test
    public void test(){
        String tokens[] = new String[]{"10","6","9","3","+","-11","*","/","*","17","+","5","+"};
        System.out.println(evalRPN(tokens));
//        isInteger("-11");
    }
}

