package byCategory.Array.may;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * 169、多数元素
 *
 * @author xiaohu
 */
public class MajorityElement_169 {

    /**
     * 摩尔投票算法
     * @param nums
     * @return
     */
    public int majorityElement1(int[] nums) {
        int count = 0;
        Integer candidate = null;

        for (int num : nums) {
            if (count == 0) {
                candidate = num;
            }
            count += (num == candidate) ? 1 : -1;
        }

        return candidate;
    }

    /**
     * 原本的思路时间复杂度是O(n)，但是由于map的是否包含某元素的api调用，时间复杂度就高了许多。
     * 运行时间变长
     * @param nums
     * @return
     */
    public int majorityElement(int[] nums){
        int num = 0;
        int ans = 0;
        Map<Integer,Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(nums[i])) {
                int tem = map.get(nums[i]);
                map.put(nums[i], tem + 1);
                if (num < tem){
                    num = tem;
                    ans = nums[i];
                }

            }
            else
                map.put(nums[i],1);
        }

        return ans;
    }

    @Test
    public void test(){
        int[] nums = new int[]{3,2,3};
        System.out.println(majorityElement(nums));
    }
}
