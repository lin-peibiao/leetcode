package byCategory.Array.may;

/**
 * 152、寻找最大子串乘积
 *
 * @author xiaohu
 */
public class MaxProduct_152 {

    /**
     * 评论题解
     * @param nums
     * @return
     */
    public int maxProduct1(int[] nums) {
        int max = Integer.MIN_VALUE, imax = 1, imin = 1; //一个保存最大的，一个保存最小的。
        for(int i=0; i<nums.length; i++){
            if(nums[i] < 0){ int tmp = imax; imax = imin; imin = tmp;} //如果数组的数是负数，那么会导致最大的变最小的，最小的变最大的。因此交换两个的值。
            imax = Math.max(imax*nums[i], nums[i]);
            imin = Math.min(imin*nums[i], nums[i]);

            max = Math.max(max, imax);
        }
        return max;
    }

    /**
     * 自己写的垃圾
     * @param nums
     * @return
     */
    public int maxProduct(int nums[]){
        if (nums.length == 0)
            return 0;
        int res = Integer.MIN_VALUE;
        for (int i = 1; i < nums.length; i++) {
            res = Math.max(res,nums[i]*res);
        }
        return nums.length == 1 ? nums[0] : res;
    }
}
