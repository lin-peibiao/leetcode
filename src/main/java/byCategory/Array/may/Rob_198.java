package byCategory.Array.may;

/**
 * 198、打家劫舍
 *
 * @author xiaohu
 */
public class Rob_198 {

    /**
     * 方式二：动态规划
     */
    public int rob2(int[] nums) {
        int n = nums.length;
        if (n == 0) {
            return 0;
        }
        // memo[i] 表示考虑抢劫 nums[i...n-1] 所能获得最大收益（不是说一定从 i 开始抢劫）
        int[] memo = new int[n];
        // 先考虑最简单的情况
        memo[n - 1] = nums[n - 1];
        for (int i = n - 2; i >= 0; i--) {
            // memo[i] 的取值在考虑抢劫 i 号房子和不考虑抢劫之间取最大值
            memo[i] = Math.max(nums[i] + (i + 2 >= n ? 0 : memo[i + 2]), nums[i + 1] + (i + 3 >= n ? 0 : memo[i + 3]));
        }
        return memo[0];
    }

    /**
     * 自己写的动态规划优化
     * @param nums
     * @return
     */
    public int rob3(int[] nums){
        int len = nums.length;
        if (len == 0)
            return 0;
        if (len == 1)
            return nums[0];
        int pre = nums[0];
        int mid = Math.max(nums[1],nums[0]);
        int cur = mid;
        for (int i = 2; i < len; i++) {
            cur = Math.max(pre + nums[i],mid);
            pre = mid;
            mid = cur;

        }
        return cur;
    }

    /**
     * 官方优化
     * @param nums
     * @return
     */
    private int myRob(int[] nums) {
        int pre = 0, cur = 0, tmp;
        for(int num : nums) {
            tmp = cur;
            cur = Math.max(pre + num, cur);
            pre = tmp;
        }
        return cur;
    }


    /**
     * 动态规划优化版
     * @param nums
     * @return
     */
    public int rob1(int[] nums) {
        if (nums == null || nums.length == 0) {
            return 0;
        }
        int length = nums.length;
        if (length == 1) {
            return nums[0];
        }
        int first = nums[0], second = Math.max(nums[0], nums[1]);
        for (int i = 2; i < length; i++) {
            int temp = second;
            second = Math.max(first + nums[i], second);
            first = temp;
        }
        return second;
    }

    /**
     * 自己都知道是错的思路
     * @param nums
     * @return
     */
    public int rob(int nums[]){
        // 作为偶数和
        int even = 0;
        // 作为奇数和
        int odd = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i % 2 == 0)
                even += nums[i];
            else
                odd += nums[i];
        }
        return Math.max(even,odd);
    }

}
