package byCategory.Array.may;

import org.junit.Test;

/**
 * 167、两数之和2
 *
 * @author xiaohu
 */
public class TwoSum_167 {

    /**
     * 双指针解法
     * @param numbers
     * @param target
     * @return
     */
    public int[] twoSum1(int[] numbers, int target) {
        for (int i = 0, j = numbers.length - 1; i < j;) {
            int sum = numbers[i] + numbers[j];
            if (sum == target) return new int[] {i + 1, j + 1};
            else if (sum > target) j--;
            else i++;
        }
        return null;
    }

    public int[] twoSum(int nums[],int target){
        int[] res = new int[]{-1,-1};
        for (int i = 0; i < nums.length; i++) {
            int index2;
            if ((index2 = binaryFind(nums,target-nums[i],i+1,nums.length-1)) != -1){
                res[0] = i;
                res[1] = index2;
                return res;
            }
        }
        return res;
    }


    /**
     * 二分法查找(改造)
     * @param nums
     * @param target
     * @return
     */
    private int binaryFind(int[] nums,int target,int left,int right){
        if (nums.length == 0)
            return -1;
        while (left <= right){
            int mid = (left + right) >> 1;
            if (nums[mid] == target)
                return mid;
            if (nums[mid] < target)
                left = mid + 1;
            else
                right = mid - 1;
        }
        return -1;
    }

    @Test
    public void test(){
        int nums[] = new int[]{-1,-1,11,15};
        for (int i : twoSum(nums,-2)) {
            System.out.println(i);
        }
    }

}
