package byCategory.Array.may;

/**
 * 153、从旋转数组中获得最小值
 * 要求时间复杂度是log n
 *
 * @author xiaohu
 */
public class FindMin {
    /**
     * 直接循环版，节省空间
     * @param nums
     * @return
     */
    public int findMin1(int[] nums) {
        int left = 0;
        int right = nums.length - 1;
        while (left < right) {
            int mid = left + (right - left) / 2;
            if (nums[mid] > nums[right]) {
                left = mid + 1;
            } else {
                right = mid;
            }
        }
        return nums[left];
    }



    public int findMin(int nums[]){
        if (nums.length == 1)
            return nums[0];
        return binaryFind(0,nums.length,nums);
    }

    /**
     * 二分查找改造
     * @param left
     * @param right
     * @param nums
     * @return
     */
    private int binaryFind(int left,int right, int nums[]){
        if (left == right - 1)
            return Math.min(nums[left],nums[right]);
        int mid = (left + right) >> 1;
        if (nums[mid] < nums[left])
            return binaryFind(left,mid,nums);
        if (nums[mid] > nums[right])
            return binaryFind(mid,right,nums);

        // 能执行到这说明数组是有序的
        return nums[left];

    }
}
