package daily.oct;

import org.junit.Test;

import java.util.*;

/**
 * @author xiaohu
 * @date 2022/10/05/ 0:40
 * @description 811、域名计数
 */
public class SubdomainVisits_811 {
    public List<String> subdomainVisits(String[] cpdomains) {
        // 使用map来做，简直不要太easy
        Map<String, Integer> res = new HashMap();

        for (String s : cpdomains){
            // 要将字符串分隔为=---==---数字和域名;
            String[] s1 = s.split(" ");
            // 将域名分割
            String[] domains = s1[1].split("\\.");
            // 要将域名添加到map 并设置访问次数
            StringBuilder sb = new StringBuilder();
            for (int i = domains.length - 1; i >= 0; i--){
                sb.append(domains[i] + ".");
                // 判断map中是否已经存在域名，如果存在则进行+=操作，否则直接设置访问次数
                if (res.containsKey(sb.toString())){
                    res.put(sb.toString(), Integer.valueOf(s1[0]) + res.get(sb.toString()));
                }else{
                    res.put(sb.toString(), Integer.valueOf(s1[0]));
                }
            }
        }
        List<String> list = new ArrayList<>();
        // 将map转换为List集合
        for (Map.Entry<String, Integer> entry : res.entrySet()){
            list.add(entry.getValue() + " " + entry.getKey());
        }
        return list;
    }

    @Test
    public void test(){
        String s = "900 www.baidu.com";
        String[] s1 = s.split(" ");
        int count = Integer.valueOf(s1[0]);
        String[] domains = s1[1].split("\\.");
        System.out.println(count);
        for (String domain : domains) {
            System.out.println(domain);
        }
    }

    @Test
    public void ArrayTest(){
        int[] arr = null;
        PriorityQueue<int[]> queue = new PriorityQueue<>((a, b) -> a[0] - b[0]);
        PriorityQueue<int[]> pq = new PriorityQueue<>((a, b) -> (arr[a[0]]/arr[a[1]] - (arr[b[0]]/arr[b[1]])));
        System.out.println(queue.size());
        int[] nums = new int[]{1,3,5,7,9,11};

    }
}
class MyComparator implements Comparator<Integer> {

    @Override
    public int compare(Integer o1, Integer o2) {
        return o2 - o1;
    }
}