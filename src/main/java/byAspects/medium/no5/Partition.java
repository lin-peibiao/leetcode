package byAspects.medium.no5;

public class Partition {
    public static void main(String[] args) {
        Partition p = new Partition();
        ListNode head = new ListNode(1,new ListNode(4,new ListNode(3,new ListNode(2
        ,new ListNode(5,new ListNode(2))))));
        System.out.println(p.partition(head, 3));
    }


    /**
     * 极其简单，只是想不出来而已
     * @param head
     * @param x
     * @return
     */
    public ListNode partition(ListNode head,int x){
        if (head == null)
            return head;
        ListNode small = new ListNode(0);
        ListNode large = new ListNode(0);
        ListNode smallNode = small;
        ListNode largeNode = large;
        while(head != null){
            if (head.val < x){
                small.next = head;
                small = small.next;
            }else{
                large.next = head;
                large = large.next;
            }
            head = head.next;
        }
        large.next = null;
        small.next = largeNode.next;
        return smallNode.next;
    }
}

