package byAspects.medium.no5;

import java.util.ArrayList;
import java.util.List;

/**
 * 89、各类编码
 * 返回各类编码合集
 */
public class GrayCode {

    public static void main(String[] args) {

    }

    /*
    一位格雷码只有两个元素，【1， 0】
    因为格雷码 n 每增加1，包含的数字会翻倍，这里我们设n位格雷码包含c个数，前一个n为n'，所以c = 2c'
    所以这时n中的前c'个数是n'中的所有数字前面补0，相当于全部都是n`中的数字
    n = 2  [ 00,  01,  11,  10]
    n = 3  [000, 001, 011, 010] (前四个数)
    这时n中的后c'个数是n'中的所有数字前面补1，然后变为逆序
    n = 2  [ 00,  01,  11,  10]
    补   1 [100, 101, 111, 110]
    逆  序 [110, 111, 101, 100] （后四个数）
    结果拼接
    n = 3  [000, 001, 011, 010, 110, 111, 101, 100]
     */

    /**
     * 时间复杂度为2的n次幂
     * @param n
     * @return
     */
    public List<Integer> grayCode(int n){
        List<Integer> ans = new ArrayList<>();
        ans.add(0);
        //对称添加
        for (int i = 1; i < n; i++) {
            int m = ans.size();
            for (int j = m-1;j >= 0;--j){
                ans.add(ans.get(j) | (1 << (i-1)));//利用或运算添加二进制格式的符合格雷编码的元素,每次循环和一个数或
            }
        }
        return ans;
    }
}
