package byAspects.medium.no5;

/**
 * 96、不同的搜索二叉树1
 * 返回所有可能的搜索二叉树数目
 */
public class NumsTrees {
    public static void main(String[] args) {
        NumsTrees n = new NumsTrees();
        System.out.println(n.numTrees1(4));
    }

    /**
     * 官方题解 动态规划
     * @param n
     * @return
     */
    public int numTrees1(int n) {
        int[] G = new int[n + 1];
        G[0] = 1;
        G[1] = 1;

        for (int i = 2; i <= n; ++i) {
            for (int j = 1; j <= i; ++j) {
                G[i] += G[j - 1] * G[i - j];
            }
        }
        return G[n];
    }

    /*
    作者：LeetCode-Solution
    链接：https://leetcode-cn.com/problems/unique-binary-search-trees/solution/bu-tong-de-er-cha-sou-suo-shu-by-leetcode-solution/
    来源：力扣（LeetCode）
    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

     */

    public int numTrees(int n) {
        return numTrees(1,n);
    }

    public int numTrees(int start,int end) {
        if (start > end)
            return 0;

        else if (start == end)
            return 1;

        else{
            int left = 0,right = 0;
            for (int i = start; i < end; i++) {
                left += numTrees(start,i);
                right += numTrees(i+1,end);
            }
            return left + right;
        }
    }
}
