package byAspects.medium.no5;

import org.junit.Test;

public class ReverseBetween {

    /**
     * 官方精选解法  清晰明了
     * @param head
     * @param m
     * @param n
     * @return
     */
    public ListNode reverseBetween1(ListNode head,int m,int n){
        // 定义一个dummyHead, 方便处理
        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;

        // 初始化指针
        ListNode g = dummyHead;
        ListNode p = dummyHead.next;

        // 将指针移到相应的位置
        for(int step = 0; step < m - 1; step++) {
            g = g.next; p = p.next;
        }

        // 头插法插入节点
        for (int i = 0; i < n - m; i++) {
            ListNode removed = p.next;
            p.next = p.next.next;

            removed.next = g.next;
            g.next = removed;
        }

        return dummyHead.next;

    }

    public ListNode reverseBetween(ListNode head,int left,int right){
        int size = right - left + 1;
        int nums[] = new int[size];
        ListNode node = new ListNode(0,head);
        ListNode cur = node.next;
        int len = 0;
        while(cur != null && len <= right){
            ++len;
            if (len >= left && len <= right){
                nums[len-left] = cur.val;
            }
            cur = cur.next;
        }
        for (int num : nums) {
            System.out.println(num);
        }

        len = 0;
        while(head != null && len <= right){
            ++len;
            if (len >= left && len <= right){
                head.val = nums[--size];
            }
            head = head.next;

        }

        return node.next;

    }

    public int getLen(ListNode head){
        int len = 0;
        while(head != null){
            ++len;
            head = head.next;
        }
        return len;
    }
    @Test
    public void test(){
        ReverseBetween r = new ReverseBetween();
        ListNode head = new ListNode(1,new ListNode(2,new ListNode(3,new ListNode(4,new ListNode(5)))));
        ListNode node = r.reverseBetween(head,2,4);
        System.out.println("++++++++++++++++++++++");
        System.out.println(node);
    }
}
