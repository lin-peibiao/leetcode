package byAspects.medium.no5;

/**
 * 100、判断是否为相同的树
 * 后序遍历二叉树口诀：左右根
 */
public class SameTrees {
    boolean ans = true;
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if (p == null && q == null)
            return true;
        //后序遍历比较两棵树的结构和值
        if (ans && null != q && null != p && ans){
            ans = p.val == q.val;
            isSameTree(p.left,q.left);
            isSameTree(p.right,q.right);
        }else{
            ans = false;
            return false;
        }

        return ans;
    }

    /**
     * 官方题解
     * @param p
     * @param q
     * @return
     */
    public boolean isSameTree1(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        } else if (p == null || q == null) {
            return false;
        } else if (p.val != q.val) {
            return false;
        } else {
            return isSameTree1(p.left, q.left) && isSameTree1(p.right, q.right);
        }
    }

    /**
     * 后序遍历二叉树
     * @param tree
     */
    private TreeNode preTraverse(TreeNode tree){
        if (null != tree){
            preTraverse(tree.left);
            preTraverse(tree.right);
        }
        return tree;
    }
}
