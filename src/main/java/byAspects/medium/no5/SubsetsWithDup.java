package byAspects.medium.no5;

import java.util.*;

/**
 * 90、求子集2
 * 有重复的元素
 */
public class SubsetsWithDup {

    public static void main(String[] args) {
        int nums[] = new int[]{
                1,1,2
        };
        SubsetsWithDup s = new SubsetsWithDup();
        System.out.println(s.subsetsWithDup(nums));
    }
    public List<List<Integer>> subsetsWithDup(int[] nums) {
        List<List<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<>());
        Deque<Integer> path = new ArrayDeque<>();
        Arrays.sort(nums);
        dfs(nums,0,path,res);
        return res;
    }

    //只需要加一个判断条件
    private void dfs(int[] nums, int begin, Deque<Integer> path, List<List<Integer>> res) {
        //结束条件为集合元素到数组最后一个元素
        //不用写这个也行，因为永远不会执行到这里
        if (begin >= nums.length)
            return;
        for (int i = begin; i < nums.length; i++) {
            if (i > begin && nums[i] == nums[i-1])
                continue;
            path.addLast(nums[i]);
            res.add(new ArrayList<>(path));
            dfs(nums,i+1,path,res);
            path.removeLast();
        }
    }
}
