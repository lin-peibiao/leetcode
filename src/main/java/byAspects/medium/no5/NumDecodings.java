package byAspects.medium.no5;

/**
 * 91、将数字解码成字母，返回可能的解码方式总数
 */
public class NumDecodings {

    /**
     * 官方解法1 动态规划。
     * @param s
     * @return
     */
    public int numDecodings(String s) {
        int n = s.length();
        int[] f = new int[n + 1];
        f[0] = 1;
        for (int i = 1; i <= n; ++i) {
            if (s.charAt(i - 1) != '0') {
                f[i] += f[i - 1];
            }
            if (i > 1 && s.charAt(i - 2) != '0' && ((s.charAt(i - 2) - '0') * 10 + (s.charAt(i - 1) - '0') <= 26)) {
                f[i] += f[i - 2];
            }
        }
        return f[n];
    }

    /**
     * 优化，将空间复杂度降为O(1)
     * @param s
     * @return
     */
    public int numDecodings2(String s){
        int len = s.length();
        int a = 0,b = 1, c = 0;
        for (int i = 1; i <= len; i++) {
            c = 0;
            if (i > 0 && s.charAt(i-1) != '0')
                c += b;
            if (i > 1 && s.charAt(i-2) != '0' && ((s.charAt(i-2) - '0') * 10 + (s.charAt(i-1) - '0') < 27))
                c += a;

            a = b;
            b = c;
        }
        return c;
    }
}
