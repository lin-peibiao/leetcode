package byAspects.medium.no5;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * 将字符串转化成IP地址
 */
public class RestoreIpAddresses {
    public List<String> restoreIpAddresses1(String s){
        int len = s.length();
        List<String> res = new ArrayList<>();
        if (len < 4 || len > 12)
            return res;
        Deque<String> path = new ArrayDeque<>();
        dfs1(s,0,len,path,res);
        return res;
    }

    private void dfs1(String s, int i, int len, Deque<String> path, List<String> res) {

        if (path.size() == 4){
//            System.out.println(path);

            if (i >= len){
                res.add(String.join(".", path));
                return;
            }else
                return;
        }

        for (int j = 1; j <= 3; j++) {
            if (j + i > len) {
                break;
            }
            if (judgeIfIpSegment1(s,i,i+j) != -1){
                path.addLast(s.substring(i,i+j));
                dfs1(s,i+j,len,path,res);
                path.removeLast();
            }
        }
    }

    /**
     *
     * @param s
     * @param left
     * @param right
     * @return
     */
    private int judgeIfIpSegment1(String s, int left, int right) {
        int len = right - left;

        // 大于 1 位的时候，不能以 0 开头
        if (len > 1 && s.charAt(left) == '0') {
            return -1;
        }

        // 转成 int 类型
        int res = 0;
        for (int i = left; i < right; i++) {
            res = res * 10 + s.charAt(i) - '0';
        }

        if (res > 255) {
            return -1;
        }
        return res;
    }


    /**
     * 官方精选解法
     * @param s
     * @return
     */
    public List<String> restoreIpAddresses(String s){
    int len = s.length();
    List<String> res = new ArrayList<>();
    // 如果长度不够，不搜索
        if (len < 4 || len > 12) {
        return res;
    }

    Deque<String> path = new ArrayDeque<>(4);
    int splitTimes = 0;
    dfs(s, len, splitTimes, 0, path, res);
        return res;
}

    /**
     * 判断 s 的子区间 [left, right] 是否能够成为一个 ip 段
     * 判断的同时顺便把类型转了
     *
     * @param s
     * @param left
     * @param right
     * @return
     */
    private int judgeIfIpSegment(String s, int left, int right) {
        int len = right - left + 1;

        // 大于 1 位的时候，不能以 0 开头
        if (len > 1 && s.charAt(left) == '0') {
            return -1;
        }

        // 转成 int 类型
        int res = 0;
        for (int i = left; i <= right; i++) {
            res = res * 10 + s.charAt(i) - '0';
        }

        if (res > 255) {
            return -1;
        }
        return res;
    }

    private void dfs(String s, int len, int split, int begin, Deque<String> path, List<String> res) {
        if (begin == len) {
            if (split == 4) {
                res.add(String.join(".", path));
            }
            return;
        }

        // 看到剩下的不够了，就退出（剪枝），len - begin 表示剩余的还未分割的字符串的位数
        if (len - begin < (4 - split) || len - begin > 3 * (4 - split)) {
            return;
        }

        for (int i = 0; i < 3; i++) {
            if (begin + i >= len) {
                break;
            }

            int ipSegment = judgeIfIpSegment(s, begin, begin + i);
            if (ipSegment != -1) {
                // 在判断是 ip 段的情况下，才去做截取
                path.addLast(ipSegment + "");
                dfs(s, len, split + 1, begin + i + 1, path, res);
                path.removeLast();
            }
        }
    }
/*
作者：liweiwei1419
        链接：https://leetcode-cn.com/problems/restore-ip-addresses/solution/hui-su-suan-fa-hua-tu-fen-xi-jian-zhi-tiao-jian-by/
        来源：力扣（LeetCode）
        著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。


 */
    @Test
    public void test(){
        RestoreIpAddresses r = new RestoreIpAddresses();
        System.out.println(r.restoreIpAddresses("101023"));
        System.out.println(r.restoreIpAddresses1("101023"));
        System.out.println("101023".substring(3, 6));
    }

}
