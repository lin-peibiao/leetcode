package byAspects.medium.middle_no2;

/**
 * 从一个升序的的数组中寻找target
 * 返回该元素的第一个下标，和最后一个下标
 * 如果不存该元素返回[-1,-1]
 */
public class FindStartEnd {

    public static void main(String[] args) {
        int nums[] = new int[]{
                1,2,3,4,4,5,6
        };
        FindStartEnd f = new FindStartEnd();
        int ans[] = f.searchRange1(nums,4);
        for (int i : ans) {
            System.out.println(i);
        }

    }

    /**
     * 官方解法
     * @param nums
     * @param target
     * @return
     */
    public int[] searchRange1(int[] nums, int target) {
        int leftIdx = binarySearch(nums, target, true);
        int rightIdx = binarySearch(nums, target, false) - 1;
        if (leftIdx <= rightIdx && rightIdx < nums.length && nums[leftIdx] == target && nums[rightIdx] == target) {
            return new int[]{leftIdx, rightIdx};
        }
        return new int[]{-1, -1};
    }

    public int binarySearch(int[] nums, int target, boolean lower) {
        int left = 0, right = nums.length - 1, ans = nums.length;
        while (left <= right) {
            int mid = (left + right) / 2;
            if (nums[mid] > target || (lower && nums[mid] == target)) {
                right = mid - 1;
                ans = mid;
            } else {
                left = mid + 1;
            }
        }
        return ans;
    }


    public int[] searchRange(int[] nums, int target) {
        int ans[] = new int[]{
                -1,-1
        };
        if (nums.length == 0)
            return ans;
        int mid = findTarget(nums,target);
        if (mid == -1)
            return ans;
        else{
            int start = mid,end = mid;
            while(start >= 0 && nums[start] == target)
                --start;
            while(end < nums.length && nums[end] == target)
                ++end;
                ans[0] = start + 1;
                ans[1] = end - 1;
        }

        return ans;


    }

    public int findTarget(int[] nums,int target){
        int left = 0,right = nums.length - 1,mid = 0;
        while(left <= right){
            mid = (left + right)/2;
            if (nums[mid] == target)
                return mid;
            else if (nums[mid] > target)
                right = mid - 1;
            else
                left = mid + 1;
        }
        return -1;
    }
}
