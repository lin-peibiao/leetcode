package byAspects.medium.middle_no2;

import java.util.Arrays;

/**
 * 31、输出下一个队列
 * 给定的数组
 */
public class OutputNextQ {
    public static void main(String[] args) {
        OutputNextQ o = new OutputNextQ();
        int nums[] = new int[]{
                3,2,1
        };
        o.nextPermutation(nums);
        for (int i : nums) {
            System.out.println(i);
        }
    }

    /**
     * 解法和我一样的官方解法
     * @param nums
     */
    public void nextPermutation1(int[] nums) {
        int i = nums.length - 2;
        while (i >= 0 && nums[i] >= nums[i + 1]) {
            i--;
        }
        if (i >= 0) {
            int j = nums.length - 1;
            while (j >= 0 && nums[i] >= nums[j]) {
                j--;
            }
            swap(nums, i, j);
        }
        reverse(nums, i + 1);
    }
    public void reverse(int[] nums, int start) {
        int left = start, right = nums.length - 1;
        while (left < right) {
            swap(nums, left, right);
            left++;
            right--;
        }
    }

    /**
     * 我的解法
     * @param nums
     */
    public void nextPermutation(int[] nums) {
        int right = nums.length - 2;
        while(right>=0 && nums[right] >= nums[right+1])
            --right;
        int index = 0;
        if (right >= 0){
            index = nums.length - 1;
            while(index >= 0 && nums[right] >= nums[index]){
                --index;
            }
            swap(nums,right,index);
        }

        Arrays.sort(nums, right + 1,nums.length);


    }

    /**
     * 交换位置
     * @param nums
     * @param left
     * @param right
     * @return
     */
    public void swap(int nums[],int left,int right){
        int tem = nums[left];
        nums[left] = nums[right];
        nums[right] = tem;
    }

    /**
     * 为交换位置之后的元素排序
     * @param nums
     * @param left
     * @param right
     */
    public void sort(int nums[],int left,int right){

    }
}
