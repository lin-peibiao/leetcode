package byAspects.medium.middle_no2;

import java.util.*;

/**
 * 39、组合总数
 * 给定一个整数数组和target
 * 找到所有符合元素之间相加等于target的组合
 * 元素可以重复使用
 */
public class CombinationSum {

    public static void main(String[] args) {
        int nums[] = new int[]{
                2,3,6,7
        };
        CombinationSum c = new CombinationSum();
        System.out.println(c.combinationSum2(nums, 7));
    }

    /**
     * 官网精选解法
     * @param candidates
     * @param target
     * @return
     */
    public List<List<Integer>> combinationSum2(int[] candidates, int target) {
        int len = candidates.length;
        List<List<Integer>> res = new ArrayList<>();
        if (len == 0) {
            return res;
        }

        // 排序是剪枝的前提
        Arrays.sort(candidates);
        Deque<Integer> path = new ArrayDeque<>();
        dfs(candidates, 0, len, target, path, res);
        return res;
    }

    private void dfs(int[] candidates, int begin, int len, int target, Deque<Integer> path, List<List<Integer>> res) {
        // 由于进入更深层的时候，小于 0 的部分被剪枝，因此递归终止条件值只判断等于 0 的情况
        if (target == 0) {
            res.add(new ArrayList<>(path));
            return;
        }

        for (int i = begin; i < len; i++) {
            // 重点理解这里剪枝，前提是候选数组已经有序，
            if (target - candidates[i] < 0) {
                break;
            }

            path.addLast(candidates[i]);
            dfs(candidates, i, len, target - candidates[i], path, res);
            path.removeLast();
        }
    }


    /**
     * 官方解法
     * @param candidates
     * @param target
     * @return
     */
    public List<List<Integer>> combinationSum1(int[] candidates, int target) {
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        List<Integer> combine = new ArrayList<Integer>();
        dfs(candidates, target, ans, combine, 0);

        return ans;
    }

    public void dfs(int[] candidates, int target, List<List<Integer>> ans, List<Integer> combine, int idx) {
        if (idx == candidates.length) {
            return;
        }
        if (target == 0) {
            ans.add(new ArrayList<Integer>(combine));
            return;
        }
        // 直接跳过
        dfs(candidates, target, ans, combine, idx + 1);
        // 选择当前数
        if (target - candidates[idx] >= 0) {
            combine.add(candidates[idx]);
            dfs(candidates, target - candidates[idx], ans, combine, idx);
            combine.remove(combine.size() - 1);
        }
    }


    List<List<Integer>> ans = new ArrayList<>();
    public List<List<Integer>> combinationSum(int[] candidates, int target) {

        Arrays.sort(candidates);
        if (candidates[0] > target || candidates.length == 0)
            return ans;
        int right = candidates.length - 1,index = 0;
        while(0 <= right && candidates[right] > target || candidates[right] + candidates[0] > target){
            --right;
        }
        if (right > 0) {
            for (int i = right; i >= 0; --i) {
                index = i;
                findSubTarget(candidates,index,target);
            }
        }
        return ans;
    }

    private int findSubTarget(int[] nums, int index, int target) {
        int sum = findSubTarget(nums,index,target);
        if (nums[index] == target){
            return target;
        }
        else{
            findSubTarget(nums,index-1,target - nums[index]);
        }

        return 0;
    }

}
