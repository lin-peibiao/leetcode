package byAspects.medium.middle_no2;

/**
 * 图像旋转
 * 给出一个n阶的二维矩阵，将矩阵90度旋转
 */
public class PhotoSpin {

    public void rotate(int[][] matrix) {
        int len = matrix.length;
        for (int i = 0; i < len/2; i++) {
            for (int j = 0; j < (len + 1)/2; j++) {
                int temp = matrix[i][j];
                matrix[i][j] = matrix[len - 1 -j][i];
                matrix[len - 1 -j][i] = matrix[len - i - 1][len - j - 1];
                matrix[len - i - 1][len - j - 1] = matrix[j][len - 1 - j];
                matrix[j][len - 1 - j] = temp;
            }
        }
    }
}
