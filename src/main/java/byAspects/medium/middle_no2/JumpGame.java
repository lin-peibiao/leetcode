package byAspects.medium.middle_no2;

/**
 * 跳跃游戏
 * 非负整数数组，从下标为一的元素开始，跳元素值大小的步数直到数组的最后一个位置
 * 返回跳到最后一个位置所用最少的步数
 */
public class JumpGame {

    public static void main(String[] args) {
        JumpGame jg = new JumpGame();
        int nums[] = new int[]{
                2,3,1,2,1,4
        };
        System.out.println(jg.jump2(nums));
    }

    /**
     * 官方解法
     * @param nums
     * @return
     */
    public int jump2(int[] nums) {
        int length = nums.length;
        int end = 0;// 上次跳跃可达范围右边界（下次的最左起跳点）
        int maxPosition = 0;// 目前能跳到的最远位置
        int steps = 0;
        for (int i = 0; i < length - 1; i++) {
            maxPosition = Math.max(maxPosition, i + nums[i]);
            // 到达上次跳跃能到达的右边界了
            if (i == end) {
                end = maxPosition;// 目前能跳到的最远位置变成了下次起跳位置的右边界
                steps++;          // 进入下一次跳跃
            }
        }
        return steps;
    }

    public int jump1(int nums[]){
        int steps = 0;
        int minDis = nums.length - 1;
        for (int i = 0; i < nums.length; i++) {
            for (int j = 0; j < nums[i]; j++) {
                if (nums[i+j]+i+j >= minDis)
                    return steps + 1;
                minDis = Math.min(minDis,nums.length-1-nums[i+j]-i-j);
            }
            ++steps;
        }
        return steps;
    }

    public int jump(int nums[]){
        int dis = nums.length - 1;
//        if (nums[0] >= dis)
//            return 1;
        int index = 0;
        int largeNum = 0;
        int res = 0;
        while (index <= dis) {
            if (nums[index] >= dis - index)
                return res + 1;
            int idx = index;
            for (int i = 0; i < nums[idx]; ++i ){
                if (dis - largeNum > dis - nums[index + i] - index - i){
                    largeNum = nums[index + i];
                    index += nums[idx];
                }else
                    index += idx;
            }
            ++res;
        }
        return -1;
    }
}
