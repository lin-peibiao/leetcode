package byAspects.medium.middle_no2;

/**
 * 实现pow(x,n)方法
 */
public class PowFunc {
    public static void main(String[] args) {
        PowFunc o = new PowFunc();
        System.out.println(o.myPow(0.00001, 2147483647));
    }

    /**
     * 官方精选解法
     * @param x
     * @param n
     * @return
     */
    public double myPow1(double x, int n) {
        double res = 1.0;
        for(int i = n; i != 0; i /= 2){
            if(i % 2 != 0){
                res *= x;
            }
            x *= x;
        }
        return  n < 0 ? 1 / res : res;
    }

    /**
     * 没想到竟然要算这么久，时间复杂度太高，超出时间范额
     * @param x
     * @param n
     * @return
     */
    public double myPow(double x, int n){
        if (n == 0)
            return 1.0;
        if (x == 0)
            return 0.0;
        if (n < 0){
            n = -n;
            return 1/calculate(x,n,1);
        }
        else
            return calculate(x,n,1);
    }

    private double calculate(double x, int n, int i) {
        if (i == n)
            return x;
        double tem = x;
        while(i << 1 < n){
            tem *= tem;
            i = i << 1;
        }
//        i = i>>1;

        return tem * calculate(x,n - i,1);
    }


}
