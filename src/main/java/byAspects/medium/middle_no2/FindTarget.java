package byAspects.medium.middle_no2;

/**
 * 搜索旋转排序数组
 * 一个数组中分为两段
 * 两段都是升序序列
 * 给出一个target，要求返回其在数组中的下标
 * 如果不存在该元素则返回-1
 * 将数组一分为二，其中一定有一个是有序的，另一个可能是有序，也能是部分有序。
 * 此时有序部分用二分法查找。无序部分再一分为二，其中一个一定有序，另一个可能有序，可能无序。就这样循环.
 */
public class FindTarget {
    public static void main(String[] args) {
        int nums[] = new int[]{
                1,3
        };
        FindTarget f = new FindTarget();
        System.out.println(f.search(nums,1));
    }

    /**
     * 官方解法 时间复杂度为O(logn)
     * 空间复杂度为O(1)
     * 将数组一分为二，其中一定有一个是有序的，另一个可能是有序，也能是部分有序。
     * 此时有序部分用二分法查找。无序部分再一分为二，其中一个一定有序，另一个可能有序，可能无序。就这样循环.
     * @param nums
     * @param target
     * @return
     */
    public int search1(int[] nums, int target) {
        int n = nums.length;
        if (n == 0) {
            return -1;
        }
        if (n == 1) {
            return nums[0] == target ? 0 : -1;
        }
        int l = 0, r = n - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            if (nums[0] <= nums[mid]) {//判断是否为有序部分
                if (nums[0] <= target && target < nums[mid]) {//二分查找
                    r = mid - 1;
                } else {//再从部分有序中查找
                    l = mid + 1;
                }
            } else {//另一个有序部分
                if (nums[mid] < target && target <= nums[n - 1]) {
                    l = mid + 1;
                } else {//再从部分有序中查找
                    r = mid - 1;
                }
            }
        }
        return -1;
    }

    /**
     *
     * @param nums
     * @param target
     * @return
     */
    public int search(int[] nums, int target) {
        int ans = -1;
        int len = nums.length;
        if (len == 0)
            return ans;

        if (nums[len - 1] == target)
            return len - 1;

        int index = len - 2;
        while(index > 0 && nums[index] < nums[index + 1]){
            if(nums[index] == target)
                return index;
            --index;
        }

        ans = findEle(nums,0,index,target);
        return ans;
    }

    private int findEle(int[] nums, int left, int right,int target) {
        while(left <= right){
            int mid = (left + right) / 2;
            if (nums[mid] == target)
                return mid;
            else if(nums[mid] < target)
                left = mid + 1;
            else
                right = mid - 1;
        }
        return -1;
    }
}
