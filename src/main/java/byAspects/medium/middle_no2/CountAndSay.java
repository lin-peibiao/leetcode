package byAspects.medium.middle_no2;

/**
 * 38、外观数列
 * 数列从第一项1开始，后一项是对前一项的描述
 * 给出一个整数n，返回第n项
 */
public class CountAndSay {
    public static void main(String[] args) {
        String s = "1234";
        CountAndSay c = new CountAndSay();
        System.out.println("答案是： "+c.countAndSay(8));
    }

    /**
     * 官方解法 思路和我的一模一样啊
     * 时间复杂度：O(N×M)，其中 N 为给定的正整数，M 为生成的字符串中的最大长度。
     * 空间复杂度：O(M)。其中 M 为生成的字符串中的最大长度。
     * @param n
     * @return
     */
    public String countAndSay2(int n){
        String str = "1";
        for (int i = 2; i <= n; ++i) {
            StringBuilder sb = new StringBuilder();
            int start = 0;
            int pos = 0;

            while (pos < str.length()) {
                while (pos < str.length() && str.charAt(pos) == str.charAt(start)) {
                    pos++;
                }
                sb.append(Integer.toString(pos - start)).append(str.charAt(start));
                start = pos;
            }
            str = sb.toString();
        }

        return str;
    }

    /**
     * 网友题解 大概也是模拟的过程，但是比我的代码耗时长
     * 代码比较简洁
     * @param n
     * @return
     */
    public String countAndSay1(int n) {
        String ans = "1";
        for (int i = 2; i <= n; i++) {
            String cur = "";
            int m = ans.length();
            for (int j = 0; j < m; ) {
                int k = j + 1;
                while (k < m && ans.charAt(j) == ans.charAt(k)) k++;
                int cnt = k - j;
                cur += cnt + "" + ans.charAt(j);
                j = k;
            }
            ans = cur;
        }
        return ans;
    }

    public String countAndSay(int n){
        if (n == 1)
            return "1";
        String pre = "11";
        int count = 2;
        while(count < n){
            StringBuilder tem = new StringBuilder();

            for (int i = 0; i < pre.length(); i++) {
                int num = 1;
                while (i < pre.length() - 1 && pre.charAt(i) == pre.charAt(i + 1)){
                    ++num;
                    ++i;
                }
                tem.append(num + "" + (pre.charAt(i) - '0'));
            }
            pre = tem.toString();
            ++count;
        }

        return pre;
    }
}
