package byAspects.medium.middle_no2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 46、全排列
 * 给出一个无重复元素的整数数组
 * 进行全排列，返回所有的全排列组合
 */
public class Permute {
    public static void main(String[] args) {
        int nums[] = new int[]{
                1,1,2
        };
        Permute p = new Permute();
        System.out.println(p.permute1(nums));
    }


    /**
     * 全排列2的官方解法
     * 用全局变量调用更快
     */
    boolean[] vis;

    public List<List<Integer>> permuteUnique(int[] nums) {
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        List<Integer> perm = new ArrayList<Integer>();
        vis = new boolean[nums.length];
        Arrays.sort(nums);
        backtrack(nums, ans, 0, perm);
        return ans;
    }

    public void backtrack(int[] nums, List<List<Integer>> ans, int idx, List<Integer> perm) {
        if (idx == nums.length) {
            ans.add(new ArrayList<Integer>(perm));
            return;
        }
        for (int i = 0; i < nums.length; ++i) {
            if (vis[i] || (i > 0 && nums[i] == nums[i - 1] && !vis[i - 1])) {
                continue;
            }
            perm.add(nums[i]);
            vis[i] = true;
            backtrack(nums, ans, idx + 1, perm);
            vis[i] = false;
            perm.remove(idx);
        }
    }


    /**
     * 全排列第二版，数组中存在相同的元素
     * @param nums
     * @return
     */
    public List<List<Integer>> permute1(int nums[]){
        Arrays.sort(nums);
        List<List<Integer>> res = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        int len = nums.length;
        if (len == 0)
            return res;
        boolean used[] = new boolean[len];
        dfs(nums,0,path,res,used);

        return res;
    }

    private void dfs(int[] nums, int depth, List<Integer> path, List<List<Integer>> res,boolean used[]) {
        if (depth == nums.length){
            res.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            //只消在此处将重复的元素跳过。
            if (i > 0 && nums[i] == nums[i-1] && used[i-1])
                continue;
            if (!used[i]){
                path.add(nums[i]);
                used[i] = true;
                //接着继续递归回溯所有可能的序列
                dfs(nums,depth+1,path,res,used);
                //将上一个添加到path的元素删除
                path.remove(path.size()-1);
                used[i] = false;
            }
        }
    }

    /**
     * 不用回溯递归的方法，从提交的状况看
     * 效率与官方的解法差不多
     * @param nums
     * @return
     */
    public List<List<Integer>> permute(int nums[]){
        List<List<Integer>> ans = new ArrayList<>();
        List<Integer> ele = null;
        Arrays.sort(nums);
        int time = 1;
        for (int i = 2; i <= nums.length; i++) {
            time *= i;
        }
        while(time-- > 0){
            int eles[] = swapPosition(nums);
            ele = new ArrayList<>();
            for (int e : eles) {
                ele.add(e);
            }
            ans.add(ele);
        }
        return ans;
    }

    public int[] swapPosition(int nums[]){
        int right = nums.length - 2;
        while(right>=0 && nums[right] >= nums[right+1])
            --right;
        int index = 0;
        if (right >= 0){
            index = nums.length - 1;
            while(index >= 0 && nums[right] >= nums[index]){
                --index;
            }
            swap(nums,right,index);
        }

        Arrays.sort(nums, right + 1,nums.length);
        return nums;
    }

    public void swap(int nums[],int left,int right){
        int tem = nums[left];
        nums[left] = nums[right];
        nums[right] = tem;
    }
}
