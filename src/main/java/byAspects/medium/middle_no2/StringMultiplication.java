package byAspects.medium.middle_no2;

/**
 * 字符串乘法，返回的结果也应为字符串
 */
public class StringMultiplication {

    public static void main(String[] args) {
        StringMultiplication sm = new StringMultiplication();
        System.out.println(sm.multiply1("12", "12"));
    }

    public String multiply1(String s1,String s2){
        if (s1.charAt(0) == '0' || s2.charAt(0) == '0')
            return "0";
        int len1 = s1.length(),len2 = s2.length();
        //每一位相乘的结果先放到数组中
        int resArr[] = new int[len1 + len2];
        for (int i = len1 - 1; i >= 0 ; --i) {
            int m1 = s1.charAt(i) - '0';
            for (int j = len2 - 1; j >= 0 ; --j) {
                int m2 = s2.charAt(j) - '0';
                resArr[i+j+1] += m1 * m2;
            }
        }
        for (int i = resArr.length-1; i > 0 ; --i) {
            resArr[i-1] += resArr[i] / 10;
            resArr[i] %= 10;
        }
        int i = resArr[0] == 0 ? 1 : 0;
        StringBuilder res = new StringBuilder();
        while (i < resArr.length){
            res.append(resArr[i]);
            ++i;
        }
        return res.toString();
    }

    /**
     * 官方精选解法
     * @param num1
     * @param num2
     * @return
     */
    public String multiply2(String num1, String num2) {
        if (num1.equals("0") || num2.equals("0")) {
            return "0";
        }
        int m = num1.length(), n = num2.length();
        int[] ansArr = new int[m + n];
        for (int i = m - 1; i >= 0; i--) {
            int x = num1.charAt(i) - '0';
            for (int j = n - 1; j >= 0; j--) {
                int y = num2.charAt(j) - '0';
                ansArr[i + j + 1] += x * y;
            }
        }
        for (int i = m + n - 1; i > 0; i--) {
            ansArr[i - 1] += ansArr[i] / 10;
            ansArr[i] %= 10;
        }
        int index = ansArr[0] == 0 ? 1 : 0;
        StringBuffer ans = new StringBuffer();
        while (index < m + n) {
            ans.append(ansArr[index]);
            index++;
        }
        return ans.toString();
    }

    public String multiply(String num1, String num2) {
        if (num1.charAt(0) == '0' || num2.charAt(0) == '0')
            return "0";
        int res = 0;
        int len1 = num1.length(),len2 = num2.length();
        for (int i = 0; i < len1; i++) {
            int m1 = (num1.charAt(len1 - 1 -i) - '0') * (int)Math.pow(10,i);
            if (m1 == 0)
                continue;
            for (int j = 0; j < len2; j++) {
                int m2 = (num2.charAt(len2 - 1 - j) - '0') * (int)Math.pow(10,j);
                if (m2 == 0)
                    continue;
                res += m1 * m2;
            }
        }
        return res+"";
    }
}
