package byAspects.medium.middle_no2;

import java.util.HashSet;
import java.util.Set;

/**
 * 判断是否为合法的数度矩阵
 */
public class JudgeIsValidSuduko {
    public static void main(String[] args) {
        char board[][] = new char[][]{
                {'8','3','.','.','7','.','.','.','.'},
                {'6','.','.','1','9','5','.','.','.'},
                {'.','9','8','.','.','.','.','6','.'},
                {'1','.','.','.','6','.','.','.','3'},
                {'4','.','.','8','.','3','.','.','1'},
                {'7','.','.','.','2','.','.','.','6'},
                {'.','6','.','.','.','.','2','8','.'},
                {'.','.','.','4','1','9','.','.','5'},
                {'.','.','.','.','8','.','.','7','9'}
        };

//        [".",".",".",".","5",".",".","1","."],
//        [".","4",".","3",".",".",".",".","."],
//        [".",".",".",".",".","3",".",".","1"],
//        ["8",".",".",".",".",".",".","2","."],
//        [".",".","2",".","7",".",".",".","."],
//        [".","1","5",".",".",".",".",".","."],
//        [".",".",".",".",".","2",".",".","."],
//        [".","2",".","9",".",".",".",".","."],
//        [".",".","4",".",".",".",".",".","."]

//        [".",".",".",".","5",".",".","1","."],
//        [".","4",".","3",".",".",".",".","."],
//        [".",".",".",".",".","3",".",".","1"],
//        ["8",".",".",".",".",".",".","2","."],
//        [".",".","2",".","7",".",".",".","."],
//        [".","1","5",".",".",".",".",".","."],
//        [".",".",".",".",".","2",".",".","."],
//        [".","2",".","9",".",".",".",".","."],
//        [".",".","4",".",".",".",".",".","."]

        JudgeIsValidSuduko j = new JudgeIsValidSuduko();
        System.out.println(j.isValidSudoku(board));
    }

    /**
     * 优化解法
     * @param board
     * @return
     */
    public boolean isValidSudoku1(char[][] board) {
        int [][]row  =new int[9][10];
        int [][]col  =new int[9][10];
        int [][]box  =new int[9][10];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (board[i][j]=='.'){
                    continue;
                }
                int curNum = board[i][j]-'0';
                if (row[i][curNum]==1){
                    return false;
                }if (col[j][curNum]==1){
                    return false;
                }
                if (box[j/3 + (i/3) * 3][curNum]==1){
                    return false;
                }
                row[i][curNum]=1;
                col[j][curNum]=1;
                box[j/3 + (i/3) * 3][curNum]=1;
            }
        }
        return true;
    }

    /**
     * 暴力循环遍历
     * @param board
     * @return
     */
    public boolean isValidSudoku(char[][] board) {
        /*
        for (int i = 0; i < board.length; i++) {
            int nums1 = 0;
            Set<Character> one = new HashSet<>();
            int nums2 = 0;
            Set<Character> two = new HashSet<>();
            for (int j = 0; j < board.length; j++) {

                if(Character.isDigit(board[i][j])){
                    one.add(board[i][j]);
                    ++nums1;
                    if(one.size() != nums1)
                        return false;
                }


                if(Character.isDigit(board[j][i])){

                    two.add(board[j][i]);
                    ++nums2;
                    if(two.size() != nums2)
                        return false;
                }

            }
        }

         */


        for (int index = 0; index < board.length; index += 3) {
            for (int k = 0; k < board.length; k += 3) {
                int nums3 = 0;
                Set<Character> three = new HashSet<>();
                for (int i = index; i < 3 + index; i++) {

                    for (int j = k; j < 3 + k; j++) {

                        if(Character.isDigit(board[i][j])){
                            three.add(board[i][j]);
                            ++nums3;
                            if(three.size() != nums3)
                                return false;
                        }
                    }
                }
            }


        }

        return true;
    }
}
