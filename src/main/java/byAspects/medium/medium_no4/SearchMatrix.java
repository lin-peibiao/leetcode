package byAspects.medium.medium_no4;

/**
 * 73、寻找二维数组
 * 满足：1、元素从左至右递增
 *      2、每一行第一个元素要比前一行最后一个数大
 *      3、包含目标元素
 *
 * 妈蛋，理解错了题意，应该是给出的二维数组具有上边1、2特征，用高效的方法判断是否存在目标元素。
 */
public class SearchMatrix {
    public static void main(String[] args) {
        SearchMatrix s = new SearchMatrix();
        int matrix[][] = new int[][]{
                {1,3,5,7},
                {10,11,16,20},
                {23,30,34,50}

        };
        System.out.println(s.searchMatrix(matrix,51));

    }
    public boolean searchMatrix(int[][] matrix, int target) {
        if (matrix[0][0] > target)
            return false;
        int m = matrix.length,n = matrix[0].length;
        boolean flag = false;
        //第一次二分法查找第一个比目标值大的元素
        int start = 0,end = m-1,mid = 0;
        while(m > 1 && start <= end){
            mid = (start+end)/2;
            if (matrix[mid][0] == target)
                return true;
            if (mid > 0 && matrix[mid][0] > target && matrix[mid-1][0] <= target) {
                flag = true;
                break;
            }
            if (matrix[mid][0] < target){
                start = mid + 1;
            }else if (matrix[mid][0] > target)
                end = mid - 1;
        }
        //mid，0就是第一个比目标元素大的下标
        //第二次使用二分法查找目标元素
        mid = flag ? mid : mid + 1;
        start = 0;
        end = n - 1;
        mid = mid > 0 ? --mid : mid;
        while(start <= end){
            int midd = (start+end)/2;
            if (matrix[mid][midd] == target)
                return true;
            if (matrix[mid][midd] > target)
                end = midd - 1;
            else
                start = midd + 1;
        }

        return false;
    }
}
