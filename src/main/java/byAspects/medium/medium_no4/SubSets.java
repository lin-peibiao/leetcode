package byAspects.medium.medium_no4;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * 返回数组所有子集
 */
public class SubSets {

    public static void main(String[] args) {
        int nums[] = new int[]{
                1,2,3,4
        };
        SubSets s = new SubSets();
        System.out.println(s.subSets(nums));
    }

    public List<List<Integer>> subSets(int nums[]){
        List<List<Integer>> res = new ArrayList<>();
        res.add(new ArrayList<>());
        if (nums == null)
            return res;
        Deque<Integer> path = new ArrayDeque<>();
        dfs(nums,0,path,res);
        return res;
    }

    private void dfs(int nums[],int begin,Deque<Integer> path,List<List<Integer>> res){
//        if (begin >= nums.length){
//            return;
//        }

        for (int i = begin; i < nums.length; i++) {
            path.addLast(nums[i]);
            res.add(new ArrayList<>(path));
            dfs(nums,i+1,path,res);
            path.removeLast();
        }
    }
}
