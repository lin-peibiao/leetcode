package byAspects.medium.medium_no4;

/**
 * 81、在旋转数组中寻找指定元素
 */
public class FindEleInRotate {


    /**
     * 官方解法
     * @param nums
     * @param target
     * @return
     */
    public boolean search1(int[] nums, int target) {
        int n = nums.length;
        if (n == 0) {
            return false;
        }
        if (n == 1) {
            return nums[0] == target;
        }
        int l = 0, r = n - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (nums[mid] == target) {
                return true;
            }

            if (nums[l] == nums[mid] && nums[mid] == nums[r]) {
                ++l;
                --r;
            } else if (nums[l] <= nums[mid]) {
                if (nums[l] <= target && target < nums[mid]) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            } else {
                if (nums[mid] < target && target <= nums[n - 1]) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
        }
        return false;
    }

    //吐血啊我吐血
    public static void main(String[] args) {
        FindEleInRotate f = new FindEleInRotate();
        int nums[] = new int[]{
//                7,7,8,8,9,13,13,14,-1,0,1,2,4,6,6,6,6,6,6,6
                1,2,2,2,0
        };
        System.out.println(f.findBorder(nums, 0, nums.length-1));
    }

    public boolean search(int[] nums, int target) {
        int border = findBorder(nums,0,nums.length);
        if (border == -1)
            return binaryFind(nums,target,0,nums.length-1);//说明不是旋转数组
        else{
            if (target >= nums[0])//第一个元素比目标值小
                return binaryFind(nums,target,0,border-1);
            else
                return binaryFind(nums,target,border,nums.length-1);
        }
    }

    /**
     * 寻找旋转数组的分界点
     * @param rotate
     * @param start
     * @param end
     * @return
     */
    private int findBorder(int[] rotate,int start,int end){

        if (start < end){
            int mid = (start + end) >> 1;
            if (mid < rotate.length-1 && rotate[mid] > rotate[mid+1])
                return mid;
            else{
                //递归在mid两边进行查询
                int left = findBorder(rotate,start,mid-1);
                if (left == -1)
                    return findBorder(rotate,mid,end);
                else
                    return left;
            }
        }
        /*
        if(start <= end){
            int mid = (start+end)/2;
            if (mid > 0 && rotate[mid-1] > rotate[mid]){//说明mid就是旋转数组得边界
                return mid;
            }else if (mid != start && mid != end){
                int left = findBorder(rotate,start,mid);
                if ( left == -1){
                    return findBorder(rotate,mid+1,end);
                }else
                    return left;
            }
        }

         */
        return -1;//说明不是旋转数组
    }

    /**
     * 二分法查找
     * @param nums
     * @param target
     * @param left
     * @param right
     * @return
     */
    private boolean binaryFind(int nums[],int target,int left,int right){
        int mid = 0;
        while(left <= right){
            mid = (left+right) >> 1;
            if (nums[mid] == target)
                return true;
            else if (nums[mid] < target)
                left = mid + 1;
            else
                right = mid - 1;
        }
        return false;
    }

}
