package byAspects.medium.medium_no4;

/**
 * 79、单词搜索
 *
 */
public class SearchWord {

    public static void main(String[] args) {
        char[][] board = new char[][]{
                {'a','b','e'},
                {'b','c','d'},
//                {'a','a','a','a','b','a'},
//                {'b','a','b','b','a','b'},
//                {'a','b','b','a','b','a'},
//                {'b','a','a','a','a','b'}

//                ,'c','d','e'
//                ,'c','d','e'
//                ,'c','d','e'
//                ,'c','d','e'
        };
        SearchWord s = new SearchWord();
        System.out.println(s.exist(board, "abcdeb"));
    }

    public boolean exist(char[][] board, String word) {
        if (board == null || board[0].length == 0 || word.length() > board.length*board[0].length)
            return false;

        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                boolean used[][] = new boolean[board.length][board[0].length];
                if (dfs(board,word,used,i,j,0))
                    return true;
            }
        }
        return false;

    }

    /**
     * 自己想的题解，比官方牛逼
     * @param board
     * @param word
     * @param used
     * @param i
     * @param j
     * @param no
     * @return
     */
    private boolean dfs(char[][] board,String word,boolean[][] used,int i,int j,int no){
        if (i <0 || j < 0 || i >= board.length || j >= board[0].length)
            return false;
        if (!used[i][j] && board[i][j] == word.charAt(no)){
            used[i][j] = true;
            if (no == word.length()-1){
//                for (boolean used1[] : used) {
//                    for (boolean used2 : used1) {
//                        System.out.print(used2+"\t");
//                    }
//                    System.out.println();
//                }

                return true;
            }else if (
                        //右
                        dfs(board,word,used,i,j+1,no+1) ||
                        //下
                        dfs(board,word,used,i+1,j,no+1) ||
                        //左
                        dfs(board,word,used,i,j-1,no+1) ||
                        //上
                        dfs(board,word,used,i-1,j,no+1)
            )
                return true;
            else
                used[i][j] = false;
        }


        return false;
    }


    /**
     * 官方题解
     * @param board
     * @param word
     * @return
     */
    public boolean exist1(char[][] board, String word) {
        int h = board.length, w = board[0].length;
        boolean[][] visited = new boolean[h][w];
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                boolean flag = check(board, visited, i, j, word, 0);
                if (flag) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean check(char[][] board, boolean[][] visited, int i, int j, String s, int k) {
        if (board[i][j] != s.charAt(k)) {
            return false;
        } else if (k == s.length() - 1) {
            return true;
        }
        visited[i][j] = true;
        int[][] directions = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};
        boolean result = false;
        for (int[] dir : directions) {
            int newi = i + dir[0], newj = j + dir[1];
            if (newi >= 0 && newi < board.length && newj >= 0 && newj < board[0].length) {
                if (!visited[newi][newj]) {
                    boolean flag = check(board, visited, newi, newj, s, k + 1);
                    if (flag) {
                        result = true;
                        break;
                    }
                }
            }
        }
        visited[i][j] = false;
        return result;
    }

}
