package byAspects.medium.medium_no4;

/**
 * 80、移除数组中的相同元素
 * 给你一个有序数组 nums ，请你 原地 删除重复出现的元素，使每个元素 最多出现两次 ，返回删除后数组的新长度。
 *
 * 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 *
 * 来源：力扣（LeetCode）
 * 链接：https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii
 * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
 */
public class RemoveTheSame {
    public int removeDuplicates(int[] nums) {
        if(nums.length == 0)
            return 0;
        int i = 0,j = 1,num;

        for (i = 1; i < nums.length;++i){
            boolean flag =false;
            while (i > 0 && i < nums.length && nums[i] == nums[i - 1]){
                if (!flag){//相同的数还没有到达两个
                    flag = !flag;
                    nums[j] = nums[i];
                    ++j;
                }
                //相同元素数量两个或以上
                ++i;
            }
            if (i < nums.length && nums[i] != nums[i-1]){
                nums[j] = nums[i];
                ++j;
            }

        }
        return j;
    }
}
