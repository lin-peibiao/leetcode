package byAspects.medium.medium_no4;

/**
 * 82、删除链表中有相同的元素
 * 只要是有相同的，所有元素都删除
 */
public class DeleteSameEle2 {
    public static void main(String[] args) {
        DeleteSameEle2 d = new DeleteSameEle2();
        ListNode list = new ListNode(1,new ListNode(2,new ListNode(2,new ListNode(3,new ListNode(
                4,new ListNode(5)
        )))));
        System.out.println(d.deleteDuplicates(list));
    }

    public ListNode deleteDuplicates(ListNode head) {
        if (head == null)
            return head;
        ListNode node = new ListNode(0,head);
        ListNode cur = node;
        while(cur.next != null && cur.next.next != null){
            if (cur.next.val == cur.next.next.val){
                int x = cur.next.val;
                while(cur.next != null && x == cur.next.val){
                    cur.next = cur.next.next;
                }
            }else
                cur = cur.next;
        }
        return node.next;
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }

    @Override
    public String toString() {
        return " val= " + val +" "+ next ;
    }
}
