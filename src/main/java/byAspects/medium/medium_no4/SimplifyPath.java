package byAspects.medium.medium_no4;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * 简化文件目录路径
 */
public class SimplifyPath {
    public String simplifyPath(String path){
        //通过“/”分割
        String[] names = path.split("/");
        Deque<String> stack = new ArrayDeque<String>();
        for (String name : names) {
            if ("..".equals(name)) {
                if (!stack.isEmpty()) {
                    stack.pollLast();//非空出栈栈顶元素
                }
            } else if (name.length() > 0 && !".".equals(name)) {
                stack.offerLast(name);//入栈目录名
            }
        }
        StringBuffer ans = new StringBuffer();
        if (stack.isEmpty()) {
            ans.append('/');
        } else {
            while (!stack.isEmpty()) {
                ans.append('/');
                ans.append(stack.pollFirst());//从栈底到栈顶用“/”连接直到全部目录名连接完整
            }
        }
        return ans.toString();
    }
}
