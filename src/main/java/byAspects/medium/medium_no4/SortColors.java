package byAspects.medium.medium_no4;

/**
 * 74、颜色分类
 * 题意：将数组原地排序
 */
public class SortColors {

    /**
     * 快速排序
     * @param nums
     */
    public void sortColors2(int[] nums){
        quickSort(nums,0,nums.length-1);
    }

    /**
     *
     * @param nums
     * @param begin
     * @param end
     */
    private void quickSort(int[] nums,int begin,int end){
        if (begin > end)
            return;
        int tem = nums[begin];
        int i= begin,j = end;
        while(i != j){
            while(nums[j] > tem && j > i)
                --j;
            while(nums[i] <= tem && j > i)
                ++i;
            if (j > i){
                int t = nums[i];
                nums[i] = nums[j];
                nums[j] = t;
            }
        }
        nums[begin] = nums[i];
        nums[i] = tem;
        quickSort(nums,begin,i-1);
        quickSort(nums,i+1,end);
    }


    /**
     * 插入排序
     * @param nums
     */
    public void sortColors1(int[] nums){
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            int cur = nums[i+1];
            int index = i;
            while(index >= 0 && cur < nums[index]){
                nums[index+1] = nums[index];
                --index;
            }
        }
    }

    /**
     * 冒泡排序
     * @param nums
     */
    public void sortColors(int[] nums){
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            for (int j = i + 1; j < len; j++) {
                if (nums[i] >= nums[j]){
                    int tem = nums[i];
                    nums[i] = nums[j];
                    nums[j] = tem;
                }
            }
        }
    }
}
