package byAspects.medium.no8;

/**
 * 137、只出现一次的数字2
 * 除了目标值外，其余所有的的元素都出现三次
 *
 * @author xiaohu
 */
public class SingleNumber {

    /**
     * 将每一个元素都看作32位的二进制数，因为出目标值外的元素都有三个
     * 那么在32为中每一位上的0或1相加为 3i 或者 3i+1 及是否为3的倍数
     * 如果是三的倍数，则说明目标值在该位没有贡献，如果不是，说明在该位上有贡献
     * 此方法很通用，只需把32位里有贡献的|（或）起来就是答案。
     * @param nums
     * @return
     */
    public int singleNumber(int nums[]){
        int ret = 0;
        for (int i = 0; i < 32; i++) {
            int mask = 1 << i;
            int cnt = 0;
            for (int j = 0; j < nums.length; j++) {
                if ((nums[j] & mask) != 0) {
                    cnt++;
                }
            }
            if (cnt % 3 != 0) {
                // 二进制自增可以这么来： 用位运算和异或运算相结合
                ret |= mask;
            }
        }
        return ret;
    }
}
