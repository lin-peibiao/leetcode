package byAspects.medium.no8;

import org.junit.Test;

/**
 * 134、加油站
 *
 * @author xiaohu
 */
public class CanCompleteCircuit {

    /**
     * 官方精选题解
     * @param gas
     * @param cost
     * @return
     */
    public int canCompleteCircuit2(int[] gas, int[] cost) {
        int len = gas.length;
        int spare = 0;
        int minSpare = Integer.MAX_VALUE;
        int minIndex = 0;

        for (int i = 0; i < len; i++) {
            spare += gas[i] - cost[i];
            if (spare < minSpare) {
                minSpare = spare;
                minIndex = i;// 最低点
            }
        }

        return spare < 0 ? -1 : (minIndex + 1) % len;
    }
/*
    作者：cyaycz
    链接：https://leetcode.cn/problems/gas-station/solution/shi-yong-tu-de-si-xiang-fen-xi-gai-wen-ti-by-cyayc/
    来源：力扣（LeetCode）
    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

我研究了一整天这个题解终于理解了，提供一个思路：

首先判断总gas能不能大于等于总cost，如果总gas不够，一切都白搭对吧（总（gas- cost）不用单独去计算，和找最低点时一起计算即可，只遍历一次）；

再就是找总（gas-cost）的最低点，不管正负（当然如果最低点都是正的话那肯定能跑完了）；

找到最低点后，如果有解，那么解就是最低点的下一个点，因为总（gas-cost）是大于等于0的，所以前面损失的gas我从最低点下一个点开始都会拿回来！（此处@小马哥！“我要争一口气，不是想证明我了不起。我是要告诉人家，我失去的东西一定要拿回来！”），别管后面的趋势是先加后减还是先减后加，最终结果我是能填平前面的坑的。

希望能有所帮助；）
 */
    /**
     * 精选评论题解,一次遍历
     * 时间复杂度为O（n）
     * @param gas
     * @param cost
     * @return
     */
    public int canCompleteCircuit1(int gas[], int cost[]){
        // 假设从编号为0站开始，一直到 i 站都正常，在开往 i+1 站时车子没油了。这时，应该将起点设置为 i+1 站。
        int rest = 0, run = 0, start = 0;
        for (int i = 0; i < gas.length; ++i){
            run += (gas[i] - cost[i]);
            rest += (gas[i] - cost[i]);
            if (run < 0){
                start = i + 1;
                run = 0; // run相当是一个由负转正的转折点
            }
        }
        return rest < 0 ? -1: start;
    }

    /**
     * 不出意外的超出时间限额,时间复杂度为O(n^2)
     * @param gas
     * @param cost
     * @return
     */
    public int canCompleteCircuit(int gas[], int cost[]){

        for (int i = 0; i < gas.length; i++) {
            int res = canFinish(gas,cost,i);
            if (res != -1)
                return res;
        }
        return -1;
    }

    /**
     * 判断能否完成环绕
     * @param gas
     * @param cost
     * @param start
     * @return8
     */
    private int canFinish(int[] gas, int[] cost, int start) {
        int curr = 0;
        for (int next = start,i = 0; i < gas.length; ++i,next = next == gas.length-1 ? 0 : next + 1) {
            if (curr + gas[next] < cost[next])
                return -1;

            curr += gas[next] - cost[next];
        }
        return start;
    }

    @Test
    public void test(){
        int gas[] = new int[]{1,2,3,4,5};
        int cost[] = new int[]{3,4,5,1,2};
        System.out.println(canCompleteCircuit(gas, cost));
    }
}
