package byAspects.medium.no8;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 133、克隆无向图
 *
 * @author xiaohu
 */
public class CloneGraph {

    /**
     * 官方题解 dfs
     * 时间空间复杂度都是O（n）n为无向图的节点数
     */
    private HashMap<Node, Node> visited = new HashMap <> ();
    public Node cloneGraph(Node node) {
        if (node == null) {
            return null;
        }

        // 如果该节点已经被访问过了，则直接从哈希表中取出对应的克隆节点返回
        if (visited.containsKey(node)) {
            return visited.get(node);
        }

        // 克隆节点，注意到为了深拷贝我们不会克隆它的邻居的列表
        Node cloneNode = new Node(node.val, new ArrayList());
        // 哈希表存储
        visited.put(node, cloneNode);

        // 遍历该节点的邻居并更新克隆节点的邻居列表
        for (Node neighbor: node.neighbors) {
            cloneNode.neighbors.add(cloneGraph(neighbor));
        }
        return cloneNode;
    }

    /**
     * 评论精选题解 同为dfs
     * 比官方题解快一丢丢
     */
    private final Map<Integer, Node> map = new HashMap<>();
    public Node cloneGraph1(Node node) {
        return node == null ? null : help(node);
    }

    private Node help(Node node) {
        Node copy = map.getOrDefault(node.val, new Node());
        if (copy.val == 0) {
            copy.val = node.val;
            map.put(copy.val, copy);
            for (Node n : node.neighbors) {
                copy.neighbors.add(help(n));
            }
        }
        return copy;
    }
    /*
    作者：LeetCode-Solution
    链接：https://leetcode.cn/problems/clone-graph/solution/ke-long-tu-by-leetcode-solution/
    来源：力扣（LeetCode）
    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
     */
}
