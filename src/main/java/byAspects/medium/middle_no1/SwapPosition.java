package byAspects.medium.middle_no1;

import java.util.Stack;

/**
 * 两两交换链表中相邻位置的节点，不改变值
 * 只改变指向
 */
public class SwapPosition {
    public static void main(String[] args) {
        ListNode head = new ListNode(1,new ListNode(2,new ListNode(3,new ListNode(4))));
        SwapPosition s = new SwapPosition();
        s.swapPairs(head);
        while (head.next != null){
            System.out.println(head.val);
            head = head.next;
        }
    }

    /**
     * 和我的思路一样的解法，比较普通常见的思路
     * 我的问题就是 ：不会准确的地转码
     * @param head
     * @return
     */
    public ListNode swapPairs1(ListNode head){
        ListNode dummyHead = new ListNode(0,head);
        ListNode temp = dummyHead;
        while (temp.next != null && temp.next.next != null) {
            ListNode node1 = temp.next;
            ListNode node2 = temp.next.next;
            temp.next = node2;
            node1.next = node2.next;
            node2.next = node1;
            temp = node1;
        }
        return dummyHead.next;
    }

    /**
     * 利用栈，我他妈是傻逼想到这个
     * @param head
     * @return
     */
    public ListNode swapPairs(ListNode head) {
        int len = getSize(head);
        ListNode dummy = new ListNode(0);
        ListNode cur = head;
        Stack<ListNode> stack1 = new Stack<>();
        Stack<ListNode> stack2 = new Stack<>();
        Stack<ListNode> stack = new Stack<>();
        while(cur != null){
            stack.push(cur);
            cur = cur.next;
        }
        for (int i = 0; i < len; i++) {
            if(i % 2 == 0){
                stack1.push(stack.pop());
            }else{
                stack2.push(stack.pop());
            }
        }

        for (int i = 0; i < len; i++) {
            if(i % 2 == 0){
                dummy.next = stack1.pop();
                dummy = dummy.next;
            }else{
                dummy.next = stack2.pop();
                dummy = dummy.next;
            }
        }
        ListNode ans = dummy.next;
        return ans;
    }

    /**
     * 获取链表的长度
     * @param list
     * @return
     */
    public int getSize(ListNode list){
        int size = 0;
        while(list != null){
            list = list.next;
            ++size;
        }
        return size;
    }

}

