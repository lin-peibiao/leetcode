package byAspects.medium.middle_no1;

/**
 * 给出一个非负数整数数组，每一个元素都代表一个坐标（i,ai）
 * 求出由每一个元素组成的面积中的最大值
 */
public class MaxArea {

    /**
     * 官方解法1
     * 由于容纳的水量是由
     * 两个指针指向的数字中较小值 * 指针之间的距离
     * 决定的。如果我们移动数字较大的那个指针，那么前者「两个指针指向的数字中较小值」不会增加，
     * 后者「指针之间的距离」会减小，那么这个乘积会减小。
     * 因此，我们移动数字较大的那个指针是不合理的。因此，我们移动 数字较小的那个指针。
     *
     * 为什么对应的数字较小的那个指针不可能再作为容器的边界了？
     *
     * 在上面的分析部分，我们对这个问题有了一点初步的想法。这里我们定量地进行证明。
     *
     * 考虑第一步，假设当前左指针和右指针指向的数分别为 xx 和 yy，不失一般性，我们假设 x \leq yx≤y。同时，两个指针之间的距离为 tt。那么，它们组成的容器的容量为：
     *
     * \min(x, y) * t = x * t
     * min(x,y)∗t=x∗t
     *
     * 我们可以断定，如果我们保持左指针的位置不变，那么无论右指针在哪里，这个容器的容量都不会超过 x * tx∗t 了。注意这里右指针只能向左移动，因为 我们考虑的是第一步，也就是 指针还指向数组的左右边界的时候。
     *
     * 我们任意向左移动右指针，指向的数为 y_1y
     * 1
     * ​
     *  ，两个指针之间的距离为 t_1t
     * 1
     * ​
     *  ，那么显然有 t_1 < tt
     * 1
     * ​
     *  <t，并且 \min(x, y_1) \leq \min(x, y)min(x,y
     * 1
     * ​
     *  )≤min(x,y)：
     *
     * 如果 y_1 \leq yy
     * 1
     * ​
     *  ≤y，那么 \min(x, y_1) \leq \min(x, y)min(x,y
     * 1
     * ​
     *  )≤min(x,y)；
     *
     * 如果 y_1 > yy
     * 1
     * 那么 \min(x, y_1) = x = \min(x, y)min(x,y
     * 因此有：
     * min(x, y_t) * t_1 < \min(x, y) * t
     *
     * 作者：LeetCode-Solution
     * 链接：https://leetcode-cn.com/problems/container-with-most-water/solution/sheng-zui-duo-shui-de-rong-qi-by-leetcode-solution/
     * 来源：力扣（LeetCode）
     * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
     * @param height
     * @return
     */
    public int maxArea1(int[] height){
        int maxArea = 0,i = 0,j = height.length - 1;
        while(i < j){
            maxArea = Math.max(maxArea,(j - i) * Math.min(height[i],height[j]) );
            if(height[i] < height[j])
                ++i;
            else
                --j;
        }
        return maxArea;
    }
    public int maxArea(int[] height){
        int area = 0;

        for (int i = 0; i < height.length; ++i) {
            for (int j = height.length-1; j > i ; --j) {
                area = Math.max(area,(j - i) * Math.min(height[i],height[j]) );
            }
        }
        return area;
    }

    public static void main(String[] args) {
        MaxArea ma = new MaxArea();
        int height[] = new int[]{1,2,1};
        System.out.println(ma.maxArea1(height));
    }
}

