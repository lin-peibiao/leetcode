package byAspects.medium.middle_no1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 18、四数之和
 */
public class SumOfFourNums {
    public static void main(String[] args) {
        SumOfFourNums sum = new SumOfFourNums();
        int nums[] = new int[]{
                333,387,387,470,492
        };
        System.out.println(sum.fourSum(nums, 1682));
    }

    /**
     * 官方解法
     * @param nums
     * @param target
     * @return
     */
    public List<List<Integer>> fourSum1(int[] nums, int target) {
        List<List<Integer>> quadruplets = new ArrayList<List<Integer>>();
        if (nums == null || nums.length < 4) {
            return quadruplets;
        }
        Arrays.sort(nums);
        int length = nums.length;
        for (int i = 0; i < length - 3; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            //这两个判断提高了很大效率
            if ((long) nums[i] + nums[i + 1] + nums[i + 2] + nums[i + 3] > target) {
                break;
            }
            if ((long) nums[i] + nums[length - 3] + nums[length - 2] + nums[length - 1] < target) {
                continue;
            }

            for (int j = i + 1; j < length - 2; j++) {
                if (j > i + 1 && nums[j] == nums[j - 1]) {
                    continue;
                }
                if ((long) nums[i] + nums[j] + nums[j + 1] + nums[j + 2] > target) {
                    break;
                }
                if ((long) nums[i] + nums[j] + nums[length - 2] + nums[length - 1] < target) {
                    continue;
                }
                int left = j + 1, right = length - 1;
                while (left < right) {
                    int sum = nums[i] + nums[j] + nums[left] + nums[right];
                    if (sum == target) {
                        quadruplets.add(Arrays.asList(nums[i], nums[j], nums[left], nums[right]));
                        while (left < right && nums[left] == nums[left + 1]) {
                            left++;
                        }
                        left++;
                        while (left < right && nums[right] == nums[right - 1]) {
                            right--;
                        }
                        right--;
                    } else if (sum < target) {
                        left++;
                    } else {
                        right--;
                    }
                }
            }
        }
        return quadruplets;
    }


    /**
     * 自己的解法，双指针, 优化对比，不用去重
     * @param nums
     * @param target
     * @return
     */
    public List<List<Integer>> fourSum(int[] nums, int target) {
        Arrays.sort(nums);
        int len = nums.length;
        List<List<Integer>> ans = new ArrayList<>();
        if (len < 4)
            return ans;

        for (int i = 0; i < len - 3; i++) {
            if (i > 0 && nums[i] == nums[i - 1]) {
                continue;
            }
            for (int j = i+1; j < len - 2; j++) {
                if (j > i + 1 && nums[j] == nums[j - 1]) {
                    continue;
                }
                int left = j+1,right = len-1;
                while(left < right){
                    int sum = nums[i] + nums[j] + nums[left] + nums[right];
                    if (sum == target ){
                        List<Integer> ele = new ArrayList<>();
                        ele.add(nums[i]);
                        ele.add(nums[j]);
                        ele.add(nums[left]);
                        ele.add(nums[right]);
                        ans.add(ele);
                        while(left < right && nums[left] == nums[left+1]) ++left;
                        while(left < right && nums[right] == nums[right-1]) --right;
                        ++left;
                        --right;
                    }else if (sum < target) {
                        ++left;
                    }else{
                        --right;
                    }
                }
            }
        }

        /*
        //去重
        int num = 0;
        for (int i = 0; i < ans.size() -1; i++) {
            for (int j = ans.size()-1; j > i; --j) {
                if(isRepeat(ans.get(i),ans.get(j))){
                    ans.remove(j);
                    ++num;
                }
            }
        }
        System.out.println(num);

         */
        return ans;
    }

    /**
     * 判断两个List集合的元素是否一样（考虑顺序）
     * @param list1
     * @param list2
     * @return
     */
    public boolean isRepeat(List<Integer> list1, List<Integer> list2 ){
        if(list1.size() != list2.size())
            return false;
        int repeat = 0;
        for (int k = 0; k < list1.size(); k++) {
            //需要注意的是：很重要
            //Integer对象用双等号作比较时只在[-128，127]起作用，因为该区间的数值被保存到常量池中
            //能够直接比较，所以还是用equals比较稳当
            if(list1.get(k).equals(list2.get(k))){
                repeat++;
            }
        }
        if (repeat == list1.size())
            return true;
        return false;
    }
}
