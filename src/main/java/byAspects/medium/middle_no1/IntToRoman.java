package byAspects.medium.middle_no1;


/**
 * 将阿拉伯数字转化成罗马数字
 *  哎呀，一开始搞得那么复杂，搞了几个数据结构
 *  最后我直接用条件语句解决  时间复杂度低，空火箭复杂度较高
 */
public class IntToRoman {
    public IntToRoman(){

    }

    /**
     * 官方解法1
     * 时间复杂度空间复杂度都是O(1)
     */
    int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
    String[] symbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

    public String intToRoman1(int num) {
        StringBuffer roman = new StringBuffer();
        for (int i = 0; i < values.length; ++i) {
            int value = values[i];
            String symbol = symbols[i];
            while (num >= value) {
                num -= value;
                roman.append(symbol);
            }
            if (num == 0) {
                break;
            }
        }
        return roman.toString();
    }

    /**
     * 官方解法2
     * 简单明了易理解
     * 时间复杂度空间复杂度都是O(1)
     */
    String[] thousands = {"", "M", "MM", "MMM"};
    String[] hundreds  = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
    String[] tens      = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
    String[] ones      = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};

    public String intToRoman2(int num) {
        StringBuffer roman = new StringBuffer();
        roman.append(thousands[num / 1000]);
        roman.append(hundreds[num % 1000 / 100]);
        roman.append(tens[num % 100 / 10]);
        roman.append(ones[num % 10]);
        return roman.toString();
    }

    /**
     * 阿拉伯数字转化成罗马数字
     * @param num
     * @return
     */
    public String intToRoman(int num) {


        StringBuilder romanFigure = new StringBuilder();
        int curr;
        if ((curr = num / 1000) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("M");
            }
            num %= 1000;
        }

        if ((curr = num / 900) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("CM");
            }
            num %= 900;
        }

        if ((curr = num / 500) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("D");
            }
            num %= 500;
        }

        if ((curr = num / 400) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("CD");
            }
            num %= 400;
        }

        if ((curr = num / 100) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("C");
            }
            num %= 100;
        }

        if ((curr = num / 90) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("XC");
            }
            num %= 90;
        }

        if ((curr = num / 50) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("L");
            }
            num %= 50;
        }

        if ((curr = num / 40) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("XL");
            }
            num %= 40;
        }

        if ((curr = num / 10) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("X");
            }
            num %= 10;
        }

        if ((curr = num / 9) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("IX");
            }
            num %= 9;
        }

        if ((curr = num / 5) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("V");
            }
            num %= 5;
        }

        if ((curr = num / 4) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("IV");
            }
            num %= 4;
        }

        if ((curr = num / 1) != 0){
            for (int i = 0; i < curr; i++) {
                romanFigure.append("I");
            }
        }

        return romanFigure.toString();
    }

    public static void main(String[] args) {

        IntToRoman in = new IntToRoman();
        System.out.println(in.intToRoman(3));
    }
}
