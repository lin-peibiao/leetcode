package byAspects.medium.middle_no1;

import java.util.Arrays;

/**
 * 16、从给定数组中寻找三个数相加最接近目标数值的相加值
 */
public class FindThreeClosest {


    /**
     * 网上找的代码，思路和我一样
     * @param nums
     * @param target
     * @return
     */
    public int threeSumClosest1(int nums[],int target){
        Arrays.sort(nums);
        int ans = nums[0] + nums[1] + nums[2];
        for(int i=0;i<nums.length;i++) {
            int start = i+1, end = nums.length - 1;
            while(start < end) {
                int sum = nums[start] + nums[end] + nums[i];
                if(Math.abs(target - sum) < Math.abs(target - ans))
                    ans = sum;
                if(sum > target)
                    end--;
                else if(sum < target)
                    start++;
                else
                    return ans;
            }
        }
        return ans;
    }
    /**
     * 代码写得不对 ，修修补补终于搞定了
     * @param nums
     * @param target
     * @return
     */
    public int threeSumClosest(int[] nums, int target) {
        int len = nums.length;
        Arrays.sort(nums);
        int sum;
        int finalSum = nums[0] + nums[1] + nums[2];
        for (int i = 0; i < len; i++) {
            int left = i+1,right = len -1;
            if (i>0 && nums[i] == nums[i-1]) {
                continue;
            }

            while(left < right){
                //以下代码不用才对，上次这么写是因为避免出现重复组合答案，是有前提条件的，在本题中需要遍历所有元素
//                while(left < right && nums[left] == nums[left + 1]) ++left;
//                while(left < right && nums[right] == nums[right] - 1) --right;

                sum = nums[left] + nums[right] + nums[i];
                if (Math.abs(sum - target) < Math.abs(finalSum - target))
                    finalSum = sum;
                if (sum > target)
                    --right;
                else if(sum < target)
                    ++left;
                else
                    return finalSum;

            }
        }
        return finalSum;
    }

    public static void main(String[] args) {
        FindThreeClosest f = new FindThreeClosest();
        int nums[] = new int[]{
                -1,0,1,1,55
        };
        System.out.println(f.threeSumClosest(nums, 3));
    }
}
