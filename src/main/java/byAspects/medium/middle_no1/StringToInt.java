package byAspects.medium.middle_no1;

/**
 * 将字符串转化为int类型
 */
public class StringToInt {
    public static void main(String[] args) {
        System.out.println(myAtoi2("   -42"));
    }

    public static int myAtoi2(String s){
        s = s.trim();
        int len = s.length();
        if(len == 0)
            return 0;
        if(!Character.isDigit(s.charAt(0)) && s.charAt(0) != '-' && s.charAt(0) != '+')
            return 0;
        int MAX = Integer.MAX_VALUE;
        int MIN = Integer.MIN_VALUE;
        int res = 0;
        boolean jud = s.charAt(0) == '-';
        int i = !Character.isDigit(s.charAt(0)) ? 1 : 0;
        while(i<len && Character.isDigit(s.charAt(i))){
            int tem = ((jud ? MIN : MIN+1) + s.charAt(i) - '0')/10;
            if(tem > res)
                return jud ? MIN : MAX;
            //按照负数形式转化
            res = res * 10 - s.charAt(i++) + '0';
        }
        return jud ? res : -res;
    }

    /**
     * 普通解法
     * @param str
     * @return
     */
    public static int myAtoi1(String str){
        str = str.trim();//去除空格
        if (str.length() == 0) return 0;
        if (!Character.isDigit(str.charAt(0))
                && str.charAt(0) != '-' && str.charAt(0) != '+')
            return 0;
        int ans = 0;
        boolean neg = str.charAt(0) == '-';
        int i = !Character.isDigit(str.charAt(0)) ? 1 : 0;
        while (i < str.length() && Character.isDigit(str.charAt(i))) {
            int tmp = ((neg ? Integer.MIN_VALUE : Integer.MIN_VALUE + 1) + (str.charAt(i) - '0')) / 10;
            if (tmp > ans) {
                return neg ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            }
            ans = ans * 10 - (str.charAt(i++) - '0');
        }
        return neg ? ans : -ans;
    }
    /**
     * 我自己写的放弃了，我好笨
     * @param s
     * @return
     */
    public static int myAtoi(String s){
        int num = 0;
        int MIN = Integer.MIN_VALUE;
        int MAX = Integer.MAX_VALUE;
        s = s.replaceAll(" ","");
        int len = s.length();

        for (int i = 0; i < len; i++) {

            if(s.substring(0,1).equals("-")){
                char cha = s.charAt(i+1);
                int ch = cha - '0';
                if(ch >= 0 && ch <= 9 && num > MIN/10 - ch){
                    num = num * 10 - ch;
                    continue;
                }
                else if (num < MIN/10 - ch ){
                    num = MIN;
                    break;
                }
                else
                    break;
            }

            else{
                char cha = s.charAt(i);
                int ch = cha - '0';
                if(ch >= 0 && ch <= 9 && num < MAX/10 - ch){
                    num = num * 10 + ch;
                    continue;
                }
                else if (num > MAX/10 - ch){
                    num = MAX;
                    break;
                }
                else {
                    break;
                }
            }



        }
        return num ;
    }
}
