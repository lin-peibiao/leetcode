package byAspects.medium.middle_no1;

/**
 * 力扣寻找最长的回文子串
 */
public class Longesthuiwen {

    public static void main(String[] args) {
        String s = "大波美人鱼人美波大";
        Longesthuiwen l = new Longesthuiwen();
        System.out.println(l.findHuiWenString(s));
    }

    public String findHuiWenString(String s){
        if (isHuiWen(s))
            return s;
        String oriS ;
        String maxS = "";
        String os = s;
        //每次循环都要将原本的字符串第一个删掉
        while(s.length() > 1 ){
            oriS = s;
            while (oriS.length()>0) {

                if(isHuiWen(oriS)){
                    maxS = oriS.length() <= maxS.length() ? maxS : oriS;//哪个回文的长度长就返回谁
                    break;
                }
                oriS = oriS.substring(0,oriS.length()-1);//将原字符串最后一个字符删除

            }

            s = s.substring(1,s.length());
        }
        return maxS == "" ? os.charAt(0)+"" : maxS;
//        return maxS;
    }


    /**
     * 判断字符串是否为回文
     * @param s
     * @return 如果是回文返回true，否则返回false。
     */
    public boolean isHuiWen(String s){
        if(s == "" )
            return false;
        if(s.length() == 1)
            return true;
        String revS ="";
        //只要翻转后的字符串长度比起原始字符串长度长就说明已经反转超过一般的字符了。
        //需要考虑字符串长度奇偶问题
        while (revS.length()<s.length()){
            revS += s.substring(s.length() - 1);
            s = s.substring(0,s.length() - 1);
        }

        return s.equals(revS) || s.equals(revS.substring(0,revS.length()-1));
    }


}
