package byAspects.medium.middle_no1;

/**
 * 删除链表倒数第n个元素
 */
public class RemoveNthFromEnd {


    public static void main(String[] args) {
        RemoveNthFromEnd r = new RemoveNthFromEnd();
        ListNode list = new ListNode(1);
        System.out.println(r.getSize(list));
    }

    /**
     * 官方解法
     * @param head
     * @param n
     * @return
     */
    public ListNode removeNthFromEnd1(ListNode head, int n) {
        ListNode dummy = new ListNode(0, head);
        int length = getSize(head);
        ListNode cur = dummy;
        for (int i = 1; i < length - n + 1; ++i) {
            cur = cur.next;
        }
        cur.next = cur.next.next;
        ListNode ans = dummy.next;
        return ans;
    }


    /**
     * 自己的解法
     * @param head
     * @param n
     * @return
     */
    public ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode tem = head;
        int target = getSize(head) - n ;
        int i = 1;
        if (target == 0){
            return head.next;
        }
        while(tem != null){
            if(i == target){
                tem.next = tem.next.next;
                break;
            }
            tem = tem.next;
            i++;
        }
        return head;
    }

    public int getSize(ListNode list){
        int size = 0;
        while(list != null){
            list = list.next;
            ++size;
        }
        return size;
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}