package byAspects.medium.middle_no1;

import java.util.*;

/**
 * 17、列出字母的所有组合
 * 从给定的数字字符串，返回所有可能的字母组合，数字对应手机
 * 九键输入法的字母
 */
public class WordsCombination {
    public static void main(String[] args) {
        WordsCombination w = new WordsCombination();
        System.out.println(w.letterCombination("322"));
    }

    public List<String> letterCombination(String digits){
        int len = digits.length();
        List<String> ans = new ArrayList<>();
        if (len == 0)
            return ans;
        for (int i = 0; i < len; i++) {
            char ch = digits.charAt(i);
            if (ans.isEmpty()){
                ans.addAll(get(ch));
            }else{
                List<String> temp = new ArrayList<>();
                for (String s : ans) {
                    //s与下一个数字的字母进行组合
                    for (String s1 : get(ch)) {
                        temp.add(s + s1);
                    }
                }
                ans = temp;
            }
        }
        return ans;
    }
    public List<String> get(char input){
        switch (input) {
            case '2':
                return Arrays.asList("a", "b", "c");
            case '3':
                return Arrays.asList("d", "e", "f");
            case '4':
                return Arrays.asList("g", "h", "i");

            case '5':
                return Arrays.asList("j", "k", "l");

            case '6':
                return Arrays.asList("m", "n", "o");

            case '7':
                return Arrays.asList("p", "q", "r","s");
            case '8':
                return Arrays.asList("t", "u", "v");
            case '9':
                return Arrays.asList("w", "x", "y","z");
        }
        return Arrays.asList("");
    }


    /**
     * 官方解法
     * @param digits
     * @return
     */
    public List<String> letterCombinations(String digits) {
        List<String> combinations = new ArrayList<String>();
        if (digits.length() == 0) {
            return combinations;
        }
        Map<Character, String> phoneMap = new HashMap<Character, String>() {{
            put('2', "abc");
            put('3', "def");
            put('4', "ghi");
            put('5', "jkl");
            put('6', "mno");
            put('7', "pqrs");
            put('8', "tuv");
            put('9', "wxyz");
        }};
        backtrack(combinations, phoneMap, digits, 0, new StringBuffer());
        return combinations;
    }

    /**
     *
     * @param ans
     * @param phoneMap
     * @param digits
     * @param index
     * @param tem
     */
    public void backtrack(List<String> ans, Map<Character, String> phoneMap, String digits, int index, StringBuffer tem) {
        if (index == digits.length()) {
            ans.add(tem.toString());
        } else {
            char digit = digits.charAt(index);
            String letters = phoneMap.get(digit);
            int lettersCount = letters.length();
            for (int i = 0; i < lettersCount; i++) {
                tem.append(letters.charAt(i));
                backtrack(ans, phoneMap, digits, index + 1, tem);
                tem.deleteCharAt(index);
            }
        }
    }

}
