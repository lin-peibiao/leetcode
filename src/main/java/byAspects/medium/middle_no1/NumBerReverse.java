package byAspects.medium.middle_no1;

/**
 * 将给出的数字进行翻转
 * 题目要求不能用64位的数字，也就是不能用long
 */
public class NumBerReverse {
    public static void main(String[] args) {
        NumBerReverse n = new NumBerReverse();
        System.out.println(n.reverse1(1534236469));
    }
    public int reverse(int x) {
        long reN = 0;
        int n = x;
        if(x<0)
            n = -x;
        while(n>0){
            reN *= 10;
            reN += n % 10;
            n /= 10;
        }
        if(reN >= Math.pow(2,31) - 1 || reN <= Math.pow(2,-31)){
            return 0;
        }
        int revN = (int) reN;
        return x >= 0 ? revN : -revN;
    }

    public int reverse1(int x) {
        int rev = 0;
        while (x != 0) {
            //放入反转数字前检查反转数字会不会超过32位
            if (rev < Integer.MIN_VALUE / 10 || rev > Integer.MAX_VALUE / 10) {
                return 0;
            }
            int digit = x % 10;
            x /= 10;
            rev = rev * 10 + digit;
        }
        return rev;
    }
}
