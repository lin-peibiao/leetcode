package byAspects.medium.middle_no1;

/**
 * 29、实现整数的除法运算
 * 要求结果在整数范围之内，如果溢出
 * 则返回最大的int类型的数
 */
public class DivisionOperation {

    public static void main(String[] args) {
        DivisionOperation d = new DivisionOperation();
//        System.out.println(d.divide1(50,9));//MAX_VALUE
        System.out.println(~-3);
    }



    /**
     * 优化解法
     * 就是将减数翻倍去比较。
     * @param dividend
     * @param divisor
     * @return
     */
    public int divide1(int dividend,int divisor){
        if (dividend == Integer.MIN_VALUE) {
            if (divisor == 1) {
                return Integer.MIN_VALUE;
            }
            if (divisor == -1) {
                return Integer.MAX_VALUE;
            }
        }
        int count = 0;
        int sgn = 0;
        if ((dividend ^ divisor) < 0) sgn = 1;//判断两个数是否为同号
        dividend = dividend < 0 ? dividend : -dividend;//将两个数都取负数
        divisor = divisor < 0 ? divisor : -divisor;
        count = div(dividend, divisor);
        return (sgn == 0) ? count : ~count + 1;//直接 -count即可
    }

    /**
     *
     * @param a 是负数
     * @param b 是负数
     * @return
     */
    public int div(int a, int b) {
        int count, tb = b;
        if (a <= b)
            count = 1;
        else return 0;
        while (a < (tb << 1) && (tb << 1) < 0) {
            tb = tb << 1;
            count  = count << 1;
        }
        return count + div(a - tb, b);
    }
    

    /**
     *
     * @param dividend
     * @param divisor
     * @return
     */
    public int divide0(int dividend, int divisor) {
        boolean flag = dividend < 0;
        boolean flag2 = divisor < 0;
        dividend = - Math.abs(dividend);
        divisor = - Math.abs(divisor);
        int ans = 0;
        while(dividend <= divisor){
            dividend -= divisor;
            --ans;
            if (ans < Integer.MIN_VALUE + 1){
                ans = flag == flag2 ? Integer.MIN_VALUE + 1 : ans;
                break;
            }
        }
        return flag == flag2 ? -ans : ans;
    }
}
