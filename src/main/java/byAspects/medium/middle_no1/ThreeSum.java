package byAspects.medium.middle_no1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 给到一个整数数组，找出所有符合任意三个元素相加为零的组合
 * 每一个组合都是不同的
 * 重点是如何去重，如果一个数组中一个或者两个元素相同
 * 那么我的方法是可以做对的，只是时间复杂度空间复杂度比较高。
 */
public class ThreeSum {

    /**
     * 网上找的解法
     */
    public List<List<Integer>> threeSum0(int[] nums) {
        List<List<Integer>> lists = new ArrayList<>();
        //排序
        Arrays.sort(nums);
        //双指针
        int len = nums.length;
        for(int i = 0;i < len;++i) {
            if(nums[i] > 0)
                return lists;

            if(i > 0 && nums[i] == nums[i-1])
                continue;

            int curr = nums[i];
            int L = i+1, R = len-1;
            while (L < R) {
                int tmp = curr + nums[L] + nums[R];
                if(tmp == 0) {
                    List<Integer> list = new ArrayList<>();
                    list.add(curr);
                    list.add(nums[L]);
                    list.add(nums[R]);
                    lists.add(list);
                    while(L < R && nums[L+1] == nums[L])
                        ++L;
                    while (L < R && nums[R-1] == nums[R])
                        --R;
                    ++L;
                    --R;
                } else if(tmp < 0) {
                    ++L;
                } else {
                    --R;
                }
            }
        }
        return lists;
    }

    /**
     * 找出所有任意三个元素相加和为零的组合
     * @param nums 整数数组
     * @return 二维数组（集合）
     */
    public List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> group = new ArrayList<>();
        List<List<Integer>> group1 = new ArrayList<>();
        List<Integer> ele0 = new ArrayList<>();
        ele0.add(0);
        ele0.add(0);
        ele0.add(0);
        group1.add(ele0);
        if(nums.length == 0){
            return group;
        }

        //暴力解法三个循环
        for (int i = 0; i < nums.length; i++) {
            for (int j = i+1; j < nums.length; j++) {
                for (int k = j+1; k < nums.length; k++) {
                    if (nums[i] + nums[j] + nums[k] == 0){
                        List<Integer> ele = new ArrayList<>();
                        ele.add(nums[i]);
                        ele.add(nums[j]);
                        ele.add(nums[k]);
                        for (int l = 0; l < group.size(); l++) {
                            if (!isRepeat(group.get(l),ele))
                                group.add(ele);
                        }
                    }
                }
            }
        }
        return (group.size() == 0 ? group1 : group);
    }

    /**
     * 判断两个数组元素是否相同（不考虑顺序）,相同就将其中一个元素删除
     * @param list1
     * @param list2
     * @return
     */
    public boolean isRepeat(List<Integer> list1, List<Integer> list2 ){
        if(list1.size() != list2.size())
            return false;
        if (list1.get(0) == list2.get(0) && list1.get(1) == list2.get(1) && list1.get(2) == list2.get(2))
            return true;
        int repeat = 0;
        int i = -1;
        for (int k = 0; k < list1.size(); k++) {
            for (int l = 0; l < list2.size(); l++) {
                if (l == i)
                    continue;
                if(list1.get(k) == list2.get(l)){
                    repeat++;
                    i = l;
                    break;
                }
            }
        }
        if (repeat == list1.size())
            return true;
        return false;
    }

    /**
     *
     * @param list1
     * @param list2
     * @return
     */
    public boolean isRepeat1(List<Integer> list1, List<Integer> list2 ){
        if(list1.size() != list2.size())
            return false;
        int repeat = 0;
        for (int k = 0; k < list1.size(); k++) {
                if(list1.get(k) == list2.get(k)){
                    repeat++;
                }
        }
        if (repeat == list1.size())
            return true;
        return false;
    }

    public static void main(String[] args) {
        int nums[] = new int[]{
                0,0,0,0
        };
        ThreeSum t = new ThreeSum();
        System.out.println(t.threeSum0(nums));

    }
}
