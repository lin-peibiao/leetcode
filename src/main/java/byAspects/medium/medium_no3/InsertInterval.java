package byAspects.medium.medium_no3;

import java.util.ArrayList;
import java.util.List;

/**
 * 57、插入区间
 * 给出有序、无重叠的区间集合，
 * 插入一个新区间，返回一个有序、无重叠的新区间集合
 */
public class InsertInterval {

    /**
     * 官方解法，我是傻掉了
     * @param intervals
     * @param newInterval
     * @return
     */
    public int[][] insert1(int[][] intervals, int[] newInterval) {
        int left = newInterval[0];
        int right = newInterval[1];
        boolean placed = false;//表示是否已经放置新区间
        List<int[]> ansList = new ArrayList<int[]>();
        for (int[] interval : intervals) {
            if (interval[0] > right) {
                // 在插入区间的右侧且无交集
                if (!placed) {
                    ansList.add(new int[]{left, right});
                    placed = true;
                }
                ansList.add(interval);
            } else if (interval[1] < left) {
                // 在插入区间的左侧且无交集
                ansList.add(interval);
            } else {
                // 与插入区间有交集，计算它们的并集
                left = Math.min(left, interval[0]);
                right = Math.max(right, interval[1]);
            }
        }
        if (!placed) {
            ansList.add(new int[]{left, right});
        }
        //没想到这样直接返回要运行多1毫秒！
        return ansList.toArray(new int[ansList.size()][]);
        /*
        int[][] ans = new int[ansList.size()][2];
        for (int i = 0; i < ansList.size(); ++i) {
            ans[i] = ansList.get(i);
        }
        return ans;

         */
    }

    public int[][] insert(int[][] intervals,int[] newInterval){
        List<int[]> res = new ArrayList<>();
        int len = intervals.length;
        if (len == 0){
            res.add(newInterval);
            return res.toArray(new int[1][]);
        }
        //先将所有区间存到链表，便于删减
        for (int i = 0; i < len; i++) {
            res.add(intervals[i]);
        }
        for (int i = 0; i < res.size()-1; i++) {
            int L = res.get(i)[0],R = res.get(i)[1];
            int nextL = res.get(i+1)[0],nextR = res.get(i+1)[1];
            if (newInterval[0] > R && newInterval[1] < nextL){
                res.add(i+1,newInterval);
                break;
            }
        }

        return res.toArray(new int[res.size()][]);
    }

    public static void main(String[] args) {
        int intervals[][] = new int[][]{
                {1,2},
                {3,4},
                {7,8},
                {9,11}
        };
        int interval[] = new int[]{
                5,6
        };
        InsertInterval i = new InsertInterval();
        intervals = i.insert(intervals,interval);
    }
}
