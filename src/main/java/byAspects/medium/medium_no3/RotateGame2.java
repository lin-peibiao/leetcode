package byAspects.medium.medium_no3;

/**
 * 螺旋矩阵2
 * 给出正整数，构造一个n平方矩阵
 * 螺旋填充1~n平方的数
 */
public class RotateGame2 {
    public static void main(String[] args) {
        RotateGame2 r = new RotateGame2();
        int[][] matrix = r.generateMatrix(4);
        for ( int array[] : matrix) {
            for (int i : array) {
                System.out.println(i);
            }
        }
    }


    public int [][] generateMatrix(int n){
        int res[][] = new int[n][n];
        if (n == 0)
            return res;
        int val = 1,size = n*n;
        int left = 0,right = n-1,top = 0,button = n-1;
        while(val <= size){
            for (int i = left; i <= right && val <= size; i++) {
                res[top][i] = val * val;
                ++val;
            }
            ++top;

            for (int i = top; i <= button && val <= size; i++) {
                res[i][right] = val * val;
                ++val;
            }
            --right;

            for (int i = right; i >= left && val <= size; i--) {
                res[button][i] = val * val;
                ++val;
            }
            --button;

            for (int i = button; i >= top && val <= size; i--) {
                res[i][left] = val * val;
                ++val;
            }
            ++left;
        }
        return res;
    }

}
