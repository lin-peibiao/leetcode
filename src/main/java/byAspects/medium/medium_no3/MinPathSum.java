package byAspects.medium.medium_no3;

/**
 * 64、不同路径最小和
 * 求不同路径中值最小的一个
 */
public class MinPathSum {

    /**
     * 官方解法 动态规划
     * 时间复杂度和空间复杂度都是O(mn)
     * @param grid
     * @return
     */
    public int minPathSum1(int [][] grid){
        if (grid == null || grid.length == 0 || grid[0].length == 0) {
            return 0;
        }
        int rows = grid.length, columns = grid[0].length;
        int[][] dp = new int[rows][columns];
        dp[0][0] = grid[0][0];
        for (int i = 1; i < rows; i++) {
            dp[i][0] = dp[i - 1][0] + grid[i][0];
        }
        for (int j = 1; j < columns; j++) {
            dp[0][j] = dp[0][j - 1] + grid[0][j];
        }
        for (int i = 1; i < rows; i++) {
            for (int j = 1; j < columns; j++) {
                dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
            }
        }
        return dp[rows - 1][columns - 1];
    }

    public int minPathSum(int[][] grid) {
        //动态规划
        if (grid == null || grid.length == 0 || grid[0].length == 0)
            return 0;
        int m = grid.length,n = grid[0].length;
        int dp[][] = new int[m][n];
        dp[0][0] = grid[0][0];
        //先将第一排第一列的路径处理出来
        for (int i = 1; i < m; i++) {
            dp[i][0] = dp[i-1][0] + grid[i][0];
        }
        for (int i = 1; i < n; i++) {
            dp[0][i] = dp[0][i-1] + grid[0][i];
        }

        //接着将最小路径和表格填充完整
        for (int i = 1; i < m; i++) {
            for (int j = 1; j < n; j++) {
                dp[i][j] = Math.min(dp[i-1][j],dp[i][j-1]) + grid[i][j];
            }
        }

        return dp[m-1][n-1];
    }
}
