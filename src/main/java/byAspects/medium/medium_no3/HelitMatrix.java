package byAspects.medium.medium_no3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 54、螺旋矩阵
 * 比较巧妙的方法就是将第一行添加后将矩阵逆时针旋转90度，再削掉已经添加的那行。
 */
public class HelitMatrix {
    public static void main(String[] args) {
        int matrix[][] = new int[][]{
                {1,2,3,1},
                {4,5,6,1},
                {7,8,9,1},
                {10,11,12,1}
        };
        HelitMatrix h = new HelitMatrix();
        System.out.println(h.spiralOrder1(matrix));

    }

    /**
     * 官方精选评论解法
     * 和我的思路差不多
     * @param matrix
     * @return
     */
    public List<Integer> spiralOrder1(int matrix[][]){
        List<Integer> result = new LinkedList<>();
        if(matrix==null||matrix.length==0) return result;
        int left = 0;
        int right = matrix[0].length - 1;
        int top = 0;
        int bottom = matrix.length - 1;
        int numEle = matrix.length * matrix[0].length;
        while (numEle >= 1) {
            for (int i = left; i <= right && numEle >= 1; i++) {
                result.add(matrix[top][i]);
                numEle--;
            }
            top++;
            for (int i = top; i <= bottom && numEle >= 1; i++) {
                result.add(matrix[i][right]);
                numEle--;
            }
            right--;
            for (int i = right; i >= left && numEle >= 1; i--) {
                result.add(matrix[bottom][i]);
                numEle--;
            }
            bottom--;
            for (int i = bottom; i >= top && numEle >= 1; i--) {
                result.add(matrix[i][left]);
                numEle--;
            }
            left++;
        }
        return result;
    }

    public List<Integer> spiralOrder(int[][] matrix) {
        int len1 = matrix.length,len2 = matrix[0].length,size = len1 * len2;
        List<Integer> res = new ArrayList<>();
        int i = 0,j = 0,edg1 = 0,edg2 = 0;
        while(res.size() < size){
            while(i == 0 && j < len2){
                res.add(matrix[i][j++]);
            }
            edg1 = i + 1;
            j -= 1;
            while(j == len2 - 1 && i < len1){
                res.add(matrix[i++][j]);
            }
            len2 = j;
            i -= 1;
            while(i == len1 - 1 && j>= edg2){
                res.add(matrix[i][j--]);
            }
            len1 = i;
            while (j == edg2 && i >= edg1){
                res.add(matrix[i--][j]);
            }
            edg2 = j + 1;
        }
        return res;
    }
}
