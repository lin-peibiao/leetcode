package byAspects.medium.medium_no3;

/**
 * 跳跃游戏
 * 给定一个数组，每个元素的值表示可以跳跃的最大格数，判断是否能到达最后一个位置
 */
public class JumpGame {
    public static void main(String[] args) {
        int nums[] = new int[]{
                1,1,2,2,0,1,1
        };
        JumpGame jg = new JumpGame();
        System.out.println(jg.canJump(nums));
    }

    /**
     * 官方题解
     * @param nums
     * @return
     */
    public boolean canJump1(int[] nums) {
        int n = nums.length;
        int rightmost = 0;
        for (int i = 0; i < n; ++i) {
            if (i <= rightmost) {
                rightmost = Math.max(rightmost, i + nums[i]);
                if (rightmost >= n - 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean canJump(int nums[]){
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] >= nums.length - 1 - i)
                return true;
        }
        return false;
    }
}
