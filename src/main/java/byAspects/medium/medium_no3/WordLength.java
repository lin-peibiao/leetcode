package byAspects.medium.medium_no3;

/**
 * 从一个含有单词和空格的字符串找到最后一个单词
 * 返回最后一个单词的字母个数
 */
public class WordLength {

    public static void main(String[] args) {
        WordLength w = new WordLength();
        System.out.println(w.wordLength("Hello World"));
    }

    public int wordLength(String s){
        int ans = 0;
        int index = s.length()-1;
        while(index >= 0 && s.charAt(index) == ' '){//原来char型也能为空格
            --index;
        }
        while(index >= 0 && s.charAt(index) != ' '){
            ++ans;
            --index;
        }
        return ans;
    }

    public int lengthOfLastWord(String s) {
        int ans = 0;
        s = s.replaceAll(" ","1");
        for (int i = s.length()-1; i >= 0 ; --i) {
            if (s.charAt(i) != '1'){
                while(i >=0 && s.charAt(i) == '1'){
                    ++ans;
                    --i;
                }
                break;
            }
        }
        return ans;
    }
}
