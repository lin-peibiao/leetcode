package byAspects.medium.medium_no3;

import java.util.*;

/**
 * 56、合并区间数组
 * 一个二维数组，保存的都是长度为2的区间
 * 如果出现区间重叠，就将重叠的数组合并
 * 其实就是一个集合求并集问题
 */
public class Merge {
    public Merge(){

    }

    public static void main(String[] args){
        Merge m = new Merge();
        int intervals[][] = new int[][]{
                {1,4},
                {0,5},
                {3,5},
                {6,9}
        };
        System.out.println(m.merge(intervals));

    }
    public int[][] merge(int[][] intervals) {
        if (intervals.length == 0) {
            return new int[0][2];
        }
        Arrays.sort(intervals, new Comparator<int[]>() {
            public int compare(int[] interval1, int[] interval2) {
                return interval1[0] - interval2[0];
            }
        });
        //java中不能创建没有长度的数组，所以得先用List集合保存元素
        //以后再遇到这种问题就能够确定点使用集合，不要担心空间复杂度
        List<int[]> merged = new ArrayList<int[]>();
        for (int i = 0; i < intervals.length; ++i) {
            int L = intervals[i][0], R = intervals[i][1];
            if (merged.size() == 0 || merged.get(merged.size() - 1)[1] < L) {
                merged.add(new int[]{L, R});
            } else {
                merged.get(merged.size() - 1)[1] = Math.max(merged.get(merged.size() - 1)[1], R);
            }
        }
        return merged.toArray(new int[merged.size()][]);
    }
}
