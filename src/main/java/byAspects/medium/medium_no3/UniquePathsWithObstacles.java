package byAspects.medium.medium_no3;

/**
 * 63、不同路径问题进阶
 * 在二维平面里假设有障碍物，求出到达目的地的路径总数
 */
public class UniquePathsWithObstacles {
    public static void main(String[] args) {
        int [][] obstacles = new int[][]{
                {0,0,0},
                {0,1,0},
                {0,0,0}
        };
        UniquePathsWithObstacles u = new UniquePathsWithObstacles();
        System.out.println(u.uniquePathsWithObstacles(obstacles));

    }

    /**
     * 空间复杂度为O(mn)的比较好理解的解法
     * @param obstacleGrid
     * @return
     */
    public int uniquePathsWithObstacles2(int [][] obstacleGrid){
        int m = obstacleGrid.length;
        int n = obstacleGrid[0].length;
        int f[][] = new int[m][n];
        if(obstacleGrid[0][0] == 1 ) return 0;
        //初始化f[0][0]=1
        f[0][0] = 1;

        for(int i = 0; i < m; i++){
            for(int j = 0; j < n; j++){
                //有障碍 不可到达
                if(obstacleGrid[i][j]== 1){
                    f[i][j] = 0;
                }else{
                    if(j-1 >= 0 ){//看看左面有没有元素
                        f[i][j] += f[i][j-1];
                    }

                    if(i - 1 >=0) {
                        f[i][j] += f[i-1][j]; //看看上面有没有元素
                    }
                }
            }
        }
        return f[m-1][n-1];
    }

    /**
     * 官方解法，动态规划
     * 时间复杂度O(mn)(m、n分别是二维表格的长和宽)
     * 空间复杂度O(m)
     * @param obstacleGrid
     * @return
     */
    public int uniquePathsWithObstacles1(int[][] obstacleGrid) {
        int n = obstacleGrid.length, m = obstacleGrid[0].length;
        int[] f = new int[m];

        f[0] = obstacleGrid[0][0] == 0 ? 1 : 0;
        for (int i = 0; i < n; ++i) {
            for (int j = 0; j < m; ++j) {
                if (obstacleGrid[i][j] == 1) {//出现障碍说明此路径行不通，路径数归零
                    f[j] = 0;
                    continue;
                }
                if (j - 1 >= 0 && obstacleGrid[i][j - 1] == 0) {
                    f[j] += f[j - 1];//
                }
            }
        }

        return f[m - 1];
    }

    public int uniquePathsWithObstacles(int[][] obstacleGrid) {
        int len = obstacleGrid.length;
        int width = obstacleGrid[0].length;
        if (obstacleGrid[len-1][width-1] == 1)
            return 0;
        return dfs(0,0,obstacleGrid,len,width,1);

    }

    /**
     * 这个深度优先还是可以完成任务，但是运行时间太长了，超出时间限额。
     * @param x
     * @param y
     * @param obstacleGrid
     * @param len
     * @param width
     * @param ans
     * @return
     */
    private int dfs(int x, int y, int[][]obstacleGrid, int len,int width, int ans) {
        if (obstacleGrid[x][y] == 1)
            return 0;
        if (x == len-1 && y == width-1)
            return ans++;
        else if (x < len-1 && y < width-1) {
            return dfs(x + 1, y,obstacleGrid, len, width, ans) + dfs(x, y + 1,obstacleGrid, len, width, ans);
        }
        else if (x == len-1 && y < width-1)
            return dfs(x,y+1,obstacleGrid,len,width,ans);
        else
            return dfs(x+1,y,obstacleGrid,len,width,ans);

    }
}
