package byAspects.medium.medium_no3;

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }

}

public class RotateRight {

    public static void main(String[] args) {
        ListNode l1 = new ListNode(1,new ListNode(2,new ListNode(3,new ListNode(4))));
        RotateRight r = new RotateRight();
        r.rotateRight(l1,1);

    }

    /**]
     * 官方解法 闭合成环
     * @param head
     * @param k
     * @return
     */
    public ListNode rotateRight1(ListNode head,int k){
        if (k == 0 || head == null || head.next == null) {
            return head;
        }
        int n = 1;
        ListNode iter = head;
        while (iter.next != null) {
            iter = iter.next;
            n++;
        }
        int add = n - k % n;
        if (add == n) {
            return head;
        }
        iter.next = head;
        while (add-- > 0) {
            iter = iter.next;
        }
        ListNode ret = iter.next;
        iter.next = null;
        return ret;
    }

    /**
     * 最终版本
     * @param head
     * @param k
     * @return
     */
    public ListNode rotateRight(ListNode head, int k) {
        int size = getSize(head);
        if (size == 0 || k % size == 0)
            return head;

        ListNode temNode = head;
        //找到尾节点
        for (int i = 0; i < size - (k % size)-1; i++) {
            temNode = temNode.next;
        }
        ListNode rear = temNode;//尾节点
        ListNode first = temNode.next;//头节点
        // System.out.println("::::::::"+first.val);
        rear.next = null;//尾节点next域置空

        //遍历找到原来的尾节点
        ListNode preRear = first;
        while(preRear.next != null)
            preRear = preRear.next;
        //接到原来的头节点
        preRear.next = head;
        return first;

    }

    /**
     * 获得链表的长度
     * @param head
     * @return
     */
    public int getSize(ListNode head){
        ListNode list = head;
        int size = 0;
        while(list != null){
            ++size;
            list = list.next;
        }
        return size;
    }
}