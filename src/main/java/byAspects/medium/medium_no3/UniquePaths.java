package byAspects.medium.medium_no3;

/**
 * 62、不同路径问题
 * 给出一个二维矩阵，求出从第一行第一列到达右下角一共有多少条路径
 * 深度优先算法
 */
public class UniquePaths {
    public static void main(String[] args) {
        UniquePaths u = new UniquePaths();
        System.out.println(u.uniquePaths(51, 9));
    }

    /**
     * 官方解法 数学组合知识
     * @param m
     * @param n
     * @return
     */
    public int uniquePaths1(int m, int n) {
        long ans = 1;
        for (int x = n, y = 1; y < m; ++x, ++y) {
            ans = ans * x / y;
        }
        return (int) ans;
    }

    public int uniquePaths(int m,int n){
        return dfs(1,1,m,n,1);
    }

    //对是对，超出时间范额
    private int dfs(int startX, int startY, int m, int n, int ans) {
        if (startX == m && startY == n)
            return ans++;
        else if (startX < m && startY < n) {
            return dfs(startX + 1, startY, m, n, ans) + dfs(startX, startY + 1, m, n, ans);
        }
        else if (startX == m && startY < n)
            return dfs(startX,startY+1,m,n,ans);
        else
            return dfs(startX+1,startY,m,n,ans);

    }
}
