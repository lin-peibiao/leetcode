package byAspects.medium.no7;

import byAspects.medium.no6.TreeNode;

/**
 * 114、将二叉树展开为链表
 * 将左子树指null，右子树指向下一个节点，顺序与二叉树的先序遍历相同
 */
public class Flatten {

    TreeNode last = null;

    /**
     * 精选评论的解法，也很好理解，精妙
     * @param root
     */
    public void flatten1(TreeNode root){
        if (root == null)
            return;
        flatten1(root.right);
        flatten1(root.left);
        root.right = last;
        root.left = null;
        last = root;
    }

    /**
     * 自己的思路，但是不太对，可能是今晚状态不好
     * @param root
     */
    public void flatten(TreeNode root){
        if (root == null)
            return;
        //先捋直左右子树
        dfs(root.left);
        dfs(root.right);

        TreeNode tem = root.right;
        root.right = root.left;
        root.left = null;

        while(root.right.right != null)
            root.right = root.right.right;

        root.right = tem;

    }
    private void dfs(TreeNode root){
        if (root == null || root.left == null)
            return;
        dfs(root.left);
        TreeNode tem = root.right;
        root.right = root.left;
        root.left.left = tem;
    }
}
