package byAspects.medium.no7;

import byAspects.medium.no6.TreeNode;

import java.util.*;

/**
 * 113、二叉树路径总和2
 */
public class PathSum {
    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<List<Integer>> res = new ArrayList<>();
        if (root == null)
            return res;
        Deque<Integer> path = new ArrayDeque<>();
        addPath(root,targetSum,path,res);
        return res;
    }

    /**
     * 递归   回溯
     * @param root
     * @param targetSum
     * @param path
     * @param res
     */
    private void addPath(TreeNode root, int targetSum, Deque<Integer> path, List<List<Integer>> res) {
        if (root == null)
            return;
        if (root.left == null && root.right == null){
            if (root.val == targetSum){
                path.add(root.val);
                res.add(new ArrayList<>(path));
                //进行回溯
                path.removeLast();
            }

        }else{
            path.addLast(root.val);
            addPath(root.left,targetSum-root.val,path,res);
            addPath(root.right,targetSum-root.val,path,res);
            path.removeLast();
        }
    }
}
