package byAspects.medium.no7;

/**
 * 122、买卖股票的最大收益2
 * 可以买卖多次
 */
public class MaxProfit {
    public int maxProfit(int prices[]){
        if (prices.length == 0)
            return 0;
        int profit = 0;
        int in = 0,out = 1;
        while(in <= out && out < prices.length){
            if (prices[in] < prices[out]){
                profit += prices[out] - prices[in];
                in = out;
            }else
                ++in;
            out = in + 1;
        }

        return profit;
    }

    /**
     * 同是贪心算法，这个就简洁了
     * 只要今天的价格比昨天高就卖出
     * @param prices
     * @return
     */
    public int maxProfit1(int prices[]){
        int ans = 0;
        int n = prices.length;
        for (int i = 1; i < n; ++i) {
            ans += Math.max(0, prices[i] - prices[i - 1]);
        }
        return ans;
    }
}
