package byAspects.medium.no7;

/**
 * 117、填充二叉树的next指针2
 * 这次是普通的二叉树
 * 可以用层次遍历解决
 */
public class Connect2 {

    /**
     * 这个题解很好理解
     * 时间复杂度和空间复杂度都是O(n)
     * @param node
     * @return
     */
    public Node connect(Node node){
        if (node == null)
            return null;

        //分别设置左右子树的next指针
        if (node.left != null && node.right != null)
            node.left.next = node.right;
        if (node.left != null && node.right == null)
            node.left.next = getNext(node.next);
        if (node.right != null)
            node.right.next = getNext(node.next);

        //遍历左右子树
        //需要注意的是，先遍历右子树，这样才能满足左子树指向下一个节点（getNext()的使用）
        connect(node.right);
        connect(node.left);
        return node;
    }

    /**
     *
     * @param node
     * @return
     */
    private Node getNext(Node node){
        if (node == null)
            return null;
        if (node.left != null)
            return node.left;
        if (node.right != null)
            return node.right;
        return getNext(node.next);
    }
}
