package byAspects.medium.no7;

import org.junit.Test;

/**
 * 130、被围绕的区域
 * 和围棋一样的
 *
 * @Author xiaohu
 */
public class WeiQiGame {
    public void solve(char[][] board) {
        if (board == null || board[0].length == 0)
            return;
        int m = board.length, n = board[0].length;
        boolean used[][] = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                // 判断字符是否为O
                // 判断是否被X围绕
                if (board[i][j] == 'O' && isRound(board,i,j,used))
                    // 是将O改为X
                    board[i][j] = 'X';
                // 否则继续遍历

            }
        }
    }


    /**
     * 判断字符是否被围绕
     * @param board
     * @param m
     * @param n
     * @param used
     * @return
     */
    private boolean isRound(char[][] board, int m, int n, boolean used[][]) {
        if (used[m][n])
            return true;
        if (m+1 >= board.length || n+1 >= board[0].length || m-1 < 0 || n-1 < 0)
            return false;
        boolean left = false,right = false,up = false, down = false;
        // left
        if (board[m][n-1] == 'X')
            left = true;
        else
            left = isRound(board,m,n-1,used);
        // right
        if (board[m][n+1] == 'X')
            right = true;
        else
            right = isRound(board,m,n+1,used);
        // up
        if (board[m+1][n] == 'X')
            up = true;
        else
            up = isRound(board,m+1,n,used);
        // down
        if (board[m-1][n] == 'X')
            down = true;
        else
            down = isRound(board,m-1,n,used);
        used[m][n] = true;
        return left && right && up && down;
    }

    @Test
    public void test(){
        char[][] board = new char[][]{
                {'X','X','X','X'},
                {'X','O','O','X'},
                {'X','X','O','X'},
                {'X','O','X','X'}
        };
        solve(board);
        for (char[] chars : board) {
            for (char aChar : chars) {
                System.out.print(aChar);
            }
            System.out.println();
        }
    }


    /**
     * 官方题解
     */
    int n, m;
    public void solve1(char[][] board) {
        n = board.length;
        if (n == 0) {
            return;
        }
        m = board[0].length;
        // 从四个边界行开始递归
        for (int i = 0; i < n; i++) {
            dfs(board, i, 0);
            dfs(board, i, m - 1);
        }
        for (int i = 1; i < m - 1; i++) {
            dfs(board, 0, i);
            dfs(board, n - 1, i);
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (board[i][j] == 'A') {
                    board[i][j] = 'O';
                } else if (board[i][j] == 'O') {
                    board[i][j] = 'X';
                }
            }
        }
    }


    /**
     * 给与边界O直接或间接相连的O打标记
     * @param board
     * @param x
     * @param y
     */
    public void dfs(char[][] board, int x, int y) {
        if (x < 0 || x >= n || y < 0 || y >= m || board[x][y] != 'O') {
            return;
        }
        board[x][y] = 'A';
        dfs(board, x + 1, y);
        dfs(board, x - 1, y);
        dfs(board, x, y + 1);
        dfs(board, x, y - 1);
    }

}
