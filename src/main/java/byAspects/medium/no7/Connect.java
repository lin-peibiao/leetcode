package byAspects.medium.no7;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 116、给每一个节点填充下一个指针
 * 完美二叉树
 */
public class Connect {

    /**
     * bfs
     * @param node
     * @return
     */
    public Node connect(Node node){
        if (node == null)
            return null;
        Queue<Node> queue = new LinkedList<>();
        queue.offer(node);

        while(!queue.isEmpty()){
            int curSize = queue.size();

            for (int i = 0; i < curSize; i++) {
                Node curNode = queue.poll();

                if (curNode.left != null)
                    queue.offer(curNode.left);
                if (curNode.right != null)
                    queue.offer(curNode.right);

                if (i == curSize-1){
                    curNode.next = null;
                }else
                    curNode.next = queue.peek();

            }
        }
        return node;
    }
}


/**
 * 完美二叉树的定义
 */
class Node {
    public int val;
    public Node left;
    public Node right;
    public Node next;

    public Node() {}

    public Node(int _val) {
        val = _val;
    }

    public Node(int _val, Node _left, Node _right, Node _next) {
        val = _val;
        left = _left;
        right = _right;
        next = _next;
    }
}
