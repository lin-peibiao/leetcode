package byAspects.medium.no7;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

/**
 * 131、分割回文串
 *
 * @author xiaohu
 */
public class Partition {

    public List<List<String>> partition(String s) {
        List<List<String>> res = new ArrayList<>();
        Deque<String> path = new ArrayDeque<>();
        dfs(0,s,path,res);
        return res;
    }

    private void dfs(int start, String s, Deque<String> path, List<List<String>> res) {
        if (start == s.length()){// 所有的字符遍历完将path添加到res中。
            res.add(new ArrayList<>(path));
            return;
        }

        for (int i = start; i < s.length(); i++) {
            if (isPalindrome(s.substring(start,i+1))) {
                path.addLast(s.substring(start, i + 1));
                // 如果分割出来的子串是回文 才继续基于该子串第一个字符 深度寻找下一回文子串
                dfs(i + 1, s, path, res);
                path.removeLast();
            }
        }
    }


    /**
     * 判断是否为回文
     * @param s
     * @return
     */
    public boolean isPalindrome(String s) {
        if(s.length() == 0)
            return true;
        int left = 0, right = s.length()-1;
        while(left < right){
            if (s.charAt(left) != s.charAt(right))
                return false;
            else{
                ++left;
                --right;
            }
        }
        return true;
    }
}
