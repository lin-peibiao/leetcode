package byAspects.medium.no7;

import java.util.HashSet;
import java.util.Set;

/**
 * 123、最长连续子串
 * 不亏，学习了哈希表的使用
 * @author traveler
 */
public class LongestConsecutive {


    public int longestConsecutive(int nums[]){
        Set<Integer> numsSet = new HashSet<>();
        for (Integer num : nums) {
            numsSet.add(num);
        }
        int longest = 0;
        for (Integer num : nums) {
            //此处也可以使用哈希表的contains方法,但是提交执行时间比直接remove多了两毫秒
            if (numsSet.remove(num)) {
                // 向当前元素的左边搜索,eg: 当前为100, 搜索：99，98，97,...
                int currentLongest = 1;
                int current = num;
                while (numsSet.remove(current - 1)) current--;
                currentLongest += (num - current);
                // 向当前元素的右边搜索,eg: 当前为100, 搜索：101，102，103,...
                current = num;
                while(numsSet.remove(current + 1)) current++;
                currentLongest += (current - num);
                // 搜索完后更新longest.
                longest = Math.max(longest, currentLongest);
            }
        }
        return longest;
    }
}
