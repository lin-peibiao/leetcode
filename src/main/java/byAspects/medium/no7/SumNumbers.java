package byAspects.medium.no7;

import byAspects.medium.no6.TreeNode;
import org.junit.Test;

public class SumNumbers {
    public int sumNumbers(TreeNode root){
        if (root == null)
            return 0;

        return mySum(root,0);
    }

    private int mySum(TreeNode root, int path){
        if (root == null)
            return 0;
        path = path * 10 + root.val;
        //若是叶子节点，返回路径和
        if (root.left == null && root.right == null)
            return path;
        //递归左右子树
        int leftSum = mySum(root.left,path);
        int rightSum = mySum(root.right,path);
        return leftSum + rightSum;
    }

    @Test
    public void test(){
        TreeNode root = new TreeNode(1,new TreeNode(2),new TreeNode(3));
        System.out.println(sumNumbers(root));
    }
}
