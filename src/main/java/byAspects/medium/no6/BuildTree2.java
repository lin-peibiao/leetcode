package byAspects.medium.no6;

import java.util.HashMap;
import java.util.Map;

/**
 * 106、通过中序遍历和后序遍历构造二叉树
 */
public class BuildTree2 {

    //作为中序遍历的映射，降低时间复杂度
    private Map<Integer,Integer> mapping;

    public TreeNode buildTree(int[] inorder, int[] postorder){
        int len = inorder.length;
        mapping = new HashMap<>();
        for (int i = 0; i < len; ++i){
            mapping.put(inorder[i],i);
        }
        return myBuiltTree(inorder,postorder,0,len-1,0,len-1);
    }

    private TreeNode myBuiltTree(int[] inorder, int[] postorder, int inLeft, int inRight, int postLeft, int postRight) {
        if (postRight < postLeft){
            return null;
        }
        int rootIndex = mapping.get(postorder[postRight]);
        TreeNode root = new TreeNode(inorder[rootIndex]);

        //右子树节点的个数
        //逻辑性比较强，要算得很仔细才行啊
        int rightSubTreeSize = inRight - rootIndex;

        //构造根的左右子树
        root.right = myBuiltTree(inorder,postorder,rootIndex+1,inRight,postRight - rightSubTreeSize,postRight-1);
        root.left = myBuiltTree(inorder,postorder,inLeft,rootIndex-1,postLeft,postRight - rightSubTreeSize -1);

        return root;

    }
}
