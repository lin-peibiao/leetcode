package byAspects.medium.no6;

import java.util.*;

/**
 * 99、恢复二叉搜索树
 */
public class RecoverTree {


    /**
     * 官方题解
     * @param root
     */
    public void recoverTree1(TreeNode root) {
        List<Integer> nums = new ArrayList<Integer>();
        inorder(root, nums);
        int[] swapped = findTwoSwapped(nums);
        recover(root, 2, swapped[0], swapped[1]);
    }

    public void inorder(TreeNode root, List<Integer> nums) {
        if (root == null) {
            return;
        }
        inorder(root.left, nums);
        nums.add(root.val);
        inorder(root.right, nums);
    }

    public int[] findTwoSwapped(List<Integer> nums) {
        int n = nums.size();
        int index1 = -1, index2 = -1;
        for (int i = 0; i < n - 1; ++i) {
            if (nums.get(i + 1) < nums.get(i)) {
                index2 = i + 1;
                if (index1 == -1) {
                    index1 = i;
                } else {
                    break;
                }
            }
        }
        int x = nums.get(index1), y = nums.get(index2);
        return new int[]{x, y};
    }

    public void recover(TreeNode root, int count, int x, int y) {
        if (root != null) {
            if (root.val == x || root.val == y) {
                root.val = root.val == x ? y : x;
                if (--count == 0) {
                    return;
                }
            }
            recover(root.right, count, x, y);
            recover(root.left, count, x, y);
        }
    }
/*
    作者：LeetCode-Solution
    链接：https://leetcode-cn.com/problems/recover-binary-search-tree/solution/hui-fu-er-cha-sou-suo-shu-by-leetcode-solution/
    来源：力扣（LeetCode）
    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

 */




    public TreeNode recoverTree(TreeNode root){
        if (root == null)
            return root;
        Deque<Integer> nums = new ArrayDeque<>();
        midTraverse(root,nums);
        Object[] array = nums.toArray();
        Arrays.sort(array);
        setValue(root,array,0);
        return root;
    }
    private void midTraverse(TreeNode root, Deque<Integer> nums){
        if (root != null){
            midTraverse(root.left,nums);
            nums.add(root.val);
            midTraverse(root.right,nums);
        }
    }

    private void setValue(TreeNode root, Object[] nums, int i){
        if (root != null && i >= 0){
            setValue(root.left,nums,i);
            root.val = (int)nums[i];
            setValue(root.right,nums,i+1);
        }
    }
}
