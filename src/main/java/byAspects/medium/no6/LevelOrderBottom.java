package byAspects.medium.no6;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 二叉树的层次遍历2
 * 自底向上的层次遍历
 */
public class LevelOrderBottom {


    public List<List<Integer>> levelOrderBottom(TreeNode root){
        List<List<Integer>> tem = new ArrayList<>();
        traversal(root, tem, 0);
        List<List<Integer>> res = new ArrayList<>();
        for (List<Integer> list : tem) {
            res.add(0,list);
        }
        return res;
    }

    /**
     * 递归解法 本质上还是深度遍历
     * 深度比广度快很多
     * @param root
     * @param res
     * @param level
     */
    private void traversal(TreeNode root, List<List<Integer>> res, int level) {
        if (root == null) {
            return;
        }

        if (res.size() == level) {
            res.add(new ArrayList<Integer>());
        }

        res.get(level).add( root.val);

        traversal(root.left, res, level + 1);
        traversal(root.right, res, level + 1);
    }


    /**
     * 层次遍历
     * @param root
     * @return
     */
    private List<List<Integer>> levelOrderBottom1(TreeNode root){
        if (root == null)
            return null;
        List<List<Integer>> res = new ArrayList<>();
        //双端队列
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()){
            List<Integer> level = new ArrayList<>();
            int size = queue.size();
            while (size > 0){
                TreeNode node = queue.poll();
                if (node.left != null)
                    queue.offer(node.left);
                if ((node.right != null))
                    queue.offer(node.right);
                --size;
                level.add(node.val);
            }
            res.add(0,level);
        }

        return res;
    }
}
