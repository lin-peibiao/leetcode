package byAspects.easy.easy_no1;

/**
 * 合并两个有序链表
 * 我他妈太傻了
 * 根本就不需要获得数值，因为本来就是有序的
 * 只需要操作节点，want to fuck myself 好笨啊呜呜呜呜
 */
public class MergeTwoList {

    public class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) { this.val = val; this.next = next; }
    }

    /**
     * 官方给出的和我思路相近的方法
     * @param l1
     * @param l2
     * @return
     */
    public ListNode mergeTwoLists1(ListNode l1, ListNode l2){
       ListNode prehead = new ListNode(-1);
       ListNode preh = prehead;
       while(l1 != null && l2 != null){
           if(l1.val <= l2.val ){
               preh.next = l1;
               l1 = l1.next;
           }else{
               preh.next = l2;
               l2 = l2.next;
           }
           preh = preh.next;
       }
       preh.next = (l1 == null ? l2 : l1);
       return prehead.next;
    }

    /**
     * 官方给出的效率较高的方法
     * 其实就是一直将小的先返回
     * 时间复杂度：O(n + m)
     * 空间复杂度：O(n+m)
     * @param l1
     * @param l2
     * @return
     */
    public ListNode mergeTwoLists2(ListNode l1, ListNode l2) {
        if (l1 == null) {
            return l2;
        } else if (l2 == null) {
            return l1;
            //以上不需要解释很容易看明白
        } else if (l1.val < l2.val) {
            l1.next = mergeTwoLists(l1.next, l2);
            return l1;
        } else {
            l2.next = mergeTwoLists(l1, l2.next);
            return l2;
        }
    }

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        //so byAspects.easy
        ListNode newNode = new ListNode();

        while(list1 != null || list2 != null){
            ListNode nextNode = new ListNode();
            nextNode.val = getValue(list1,list2);
            newNode.next = nextNode;
            nextNode = newNode;

            if (list1.val >= list2.val && list1 != null) {
                list1 = list1.next;
            } else if (list1.val < list2.val && list2 != null) {
                list2 = list2.next;
            }
        }
        return newNode;
    }


    public int getValue (ListNode list1, ListNode list2){
        if(list1 == null && list2 == null){
            return 10086;
        }
        int value;
        if (list1 == null){
            value = list2.val;
        }else if (list2 == null){
            value = list1.val;
        }else{
            value = Math.min(list1.val,list2.val);
        }
        return value;
    }
}
