package byAspects.easy.easy_no1;

/**
 * 移除数组中指定的元素
 * 不能使用额外地数组空间
 * 并返回数组的长度
 */
public class RemoveEle {
    public static void main(String[] args) {
        RemoveEle r = new RemoveEle();
        int nums[] = new int[]{
            1,2,3,4,4,4,5
        };
        System.out.println(r.removeElement(nums, 4));
    }

    /**
     * 本质上就是将不等于指定数值的元素往数组前边移动
     * @param nums
     * @param val
     * @return
     */
    public int removeElement(int[] nums, int val) {
        int cur = 0;
        int len = nums.length;
        for (int i = 0; i < len; ++i) {
            if (nums[i] != val){
                nums[cur] = nums[i];
                ++cur;
            }
        }
        return cur;
    }
}
