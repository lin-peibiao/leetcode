package byAspects.easy.easy_no1;

import org.junit.Test;

/**
 * 实现strStr功能
 * 给出一字符串，匹配符合的字串，返回包含字串的第一个下标
 */
public class StrStr {

    /**
     * 官方解法1
     * 暴力解法
     * @param haystack
     * @param needle
     * @return
     */
    public int strStr(String haystack,String needle) {
        int n = haystack.length(), m = needle.length();
        for (int i = 0; i + m <= n; i++) {
            boolean flag = true;
            for (int j = 0; j < m; j++) {
                if (haystack.charAt(i + j) != needle.charAt(j)) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                return i;
            }
        }
        return -1;
    }

    @Test
    public void test(){
        long l = 1l;
    }


}
interface A{
    public void main(String arhs[]);
}