package byAspects.easy.easy_no1;

/**
 * 按照所给的方式计算从所给非负整数到0的步骤数
 * 如果所给整数为偶数除以二，如果为奇数，减去一;
 */
public class NumOfStepsToZero {

    public int numsOfSteps(int num){
        int steps = 0;
        while(num != 0){
            if(num % 2 == 1)
                num -= 1;
            else
                num /= 2;
            steps++;
        }
        return steps;
    }

    public static void main(String[] args) {
        NumOfStepsToZero n = new NumOfStepsToZero();
        System.out.println(n.numsOfSteps(14));
    }
}
