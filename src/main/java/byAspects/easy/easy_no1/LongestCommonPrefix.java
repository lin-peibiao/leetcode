package byAspects.easy.easy_no1;


/**
 * 给定一个字符串数组，找出每一个字符串的共同前缀
 */
public class LongestCommonPrefix {

    public static void main(String[] args) {
        String strs[] = new String[]{
                "flower","flow","","flower"
        };
        System.out.println(longestCommonPrefix(strs));
        System.out.println("=======================");
    }
    public static String longestCommonPrefix(String[] strs){
        String longestWord = "";
        String next = "";
        //先找到最短的字符串
        int length = strs[0].length();
        for(int i = 1; i < strs.length; i++){
            length = (length < strs[i].length() ? length : strs[i].length() );
        }
        //有空字符串直接返回空
        if(length == 0){
            return "";
        }
        //i是元素下标，m是元素的字符下标
        for (int m = 0;m < length ; m++) {
            next = strs[0].substring(m,m+1);
            for (int j = 0; j < strs.length; j++) {

                //如果前缀相同，继续比较下一个
                next = (next.equals(strs[j].substring(m,m+1)) ? next : "");
            }
            if (next.equals(""))
                return longestWord;
            longestWord += next;
        }
        return longestWord;
    }
}
