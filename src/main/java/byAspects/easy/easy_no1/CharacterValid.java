package byAspects.easy.easy_no1;

import java.util.*;

/**
 * 判断是否为正确的括号表达式
 */
public class CharacterValid {
    /**
     * 官方解法1
     * @param s
     * @return
     */
    public boolean isValid1(String s){
        int len = s.length();
        if(len % 2 != 0 || len == 0 )
            return false;
        Map<Character, Character> pairs = new HashMap<Character, Character>() {{
            put(')', '(');
            put(']', '[');
            put('}', '{');
        }};

        Deque<Character> stack = new LinkedList<>();
        for (int i = 0; i < len; i++) {
            char ch = s.charAt(i);
            if (pairs.containsKey(ch)){
                if(stack.isEmpty() || stack.peek() != pairs.get(ch))
                    return false;
                stack.pop();
            }else{
                stack.push(ch);
            }
        }
        return stack.isEmpty();
    }

    public boolean isValid(String s){
        int n = s.length();
        if (n % 2 == 1) {
            return false;
        }

        Map<Character, Character> pairs = new HashMap<Character, Character>() {{
            put(')', '(');
            put(']', '[');
            put('}', '{');
        }};
        Deque<Character> stack = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            char ch = s.charAt(i);
            if (pairs.containsKey(ch)) {
                if (stack.isEmpty() || stack.peek() != pairs.get(ch)) {
                    return false;
                }
                stack.pop();
            } else {
                stack.push(ch);
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        CharacterValid cv = new CharacterValid();
        System.out.println(cv.isValid("[][]{})("));
    }
}
