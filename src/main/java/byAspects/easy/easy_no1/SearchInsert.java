package byAspects.easy.easy_no1;

/**
 * 给定一个有序数组和一个整数
 * 找出该整数在数组中的下标
 * 如果数组中不存在此整数，返回应该插入的下标
 */
public class SearchInsert {

    public static void main(String[] args) {
        SearchInsert si = new SearchInsert();
        int nums[] = new int[]{
                1,3,5,6
        };
        System.out.println(si.searchInsert1(nums,-9));
    }

    /**
     * 官方解法1 二分查找
     * @param nums
     * @param target
     * @return
     */
    public int searchInsert1(int[] nums, int target) {
        int n = nums.length;
        int left = 0, right = n - 1, ans = n;
        while (left <= right) {
            int mid = (left + right)/2;
            if (target <= nums[mid]) {
                ans = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return ans;
    }

    /**
     *
     * @param nums
     * @param target
     * @return
     */
    public int searchInsert(int[] nums, int target) {
        return findEle(nums,0,nums.length-1,target);
    }

    public int findEle(int nums[],int left,int right,int target){
        if (left > right)
            return left;
        int middle = (left + right)/2;
        if(nums[middle] <= target)
            return middle;
        else
            return findEle(nums,left,middle-1,target);
    }
}
