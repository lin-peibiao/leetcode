package byAspects.easy.easy_no1;

/**
 * 删除数组中的重复元素
 * 不使用额外的数组内存空间
 * 空间复杂度应是O(1)
 */
public class DeleteSameEle {
    public static void main(String[] args) {
        int nums[] = new int[]{
                8,8,8,9
        };
        DeleteSameEle d = new DeleteSameEle();
        d.removeDuplicates(nums);
        for (int i : nums) {
            System.out.println(i);
        }
    }

    /**
     * 优化解法
     * @param nums
     * @return
     */
    public int removeDuplicates1(int[] nums){
        if(nums == null || nums.length == 0) return 0;
        int p = 0;
        int q = 1;
        while(q < nums.length){
            if(nums[p] != nums[q]){
                if(q - p > 1){
                    nums[p + 1] = nums[q];
                }
                p++;
            }
            q++;
        }
        return p + 1;
    }

    /**
     * 又要用到双指针
     * @param nums
     * @return
     */
    public int[] removeDuplicates(int[] nums) {
        for (int i = 0; i < nums.length; i++) {
            if (i > 0 && nums[i] == nums[i-1]){
                for (int j = i-1; j < nums.length-1; j++) {
                    int temp = nums[j];
                    nums[j] = nums[j+1];
                    nums[j+1] = temp;
                }
            }
        }
        return nums;
    }
}
