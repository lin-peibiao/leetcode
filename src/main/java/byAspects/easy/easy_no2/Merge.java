package byAspects.easy.easy_no2;

import java.util.Arrays;

/**
 * 88、合并两个有序数组
 */
public class Merge {

    public static void main(String[] args) {
        int nums1[] = new int[]{0};
        int nums2[] = new int[]{1};
        Merge m = new Merge();
        m.merge3(nums1,nums2,1,0);
        for (int i : nums1) {
            System.out.println(i);
        }

    }

    /**
     * 解法3 从后往前遍历，每次将较大者存入nums1的后方
     * @param nums1
     * @param nums2
     * @param n
     * @param m
     */
    public void merge3(int nums1[],int nums2[],int n,int m){
        int i = m-1, j = n-1;
        int len = m+n-1;
        while(i >= 0 && j >= 0){
            if (nums1[i] > nums2[j]){
                nums1[len--] = nums1[i--];
            }else{
                nums1[len--] = nums2[j--];
            }
        }
        if (j < 0){
            while(len >= 0){
                nums1[len--] = nums1[i--];
            }
        }else{
            while(len >= 0){
                nums1[len--] = nums2[j--];
            }
        }

    }

    /**
     * 方法2
     * @param nums1
     * @param nums2
     * @param n
     * @param m
     */
    public void merge1(int nums1[],int nums2[],int n,int m){
        int j = 0;
        for (int i = m; i < m+n; i++) {
            nums1[i] = nums2[j++];
        }
        Arrays.sort(nums1);

    }

    /**
     *
     * @param nums1
     * @param nums2
     * @param m 第一个数组的长度
     * @param n 第二个数组的长度
     */
    public int[] merge(int nums1[],int nums2[],int n,int m){
        int[] ans = new int[m+n];
        int len = 0;
        int i=0,j=0;
        while(i < m && j < n){
            if (nums1[i] <= nums2[j]){
                ans[len] = nums1[i];
                ++i;
            }else{
                ans[len] = nums2[j];
                ++j;
            }
            ++len;
        }

        if (j == n){
            while(len < m+n){
                ans[len++] = nums1[i++];
            }
        }else{
            while(len < m+n){
                ans[len++] = nums2[j++];
            }
        }
        return ans;
    }

}
