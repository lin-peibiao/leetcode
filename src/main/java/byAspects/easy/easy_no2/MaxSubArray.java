package byAspects.easy.easy_no2;

/**
 * 寻找最大字串和
 */
public class MaxSubArray {

    /**
     * 官方解法1
     * @param nums
     * @return
     */
    public int maxSubArray0(int nums[]){
        int pre = 0,ans = nums[0];
        for (int x : nums) {
            pre = Math.max(pre + x, x);
            ans = Math.max(pre, ans);
        }
        return ans;
    }

    /**
     * 官方解法2  分治
     * 能理解就不错了哟
     */
    public class Status {
        public int lSum, rSum, mSum, iSum;

        public Status(int lSum, int rSum, int mSum, int iSum) {
            this.lSum = lSum;
            this.rSum = rSum;
            this.mSum = mSum;
            this.iSum = iSum;
        }
    }

    public int maxSubArray(int[] nums) {
        return getInfo(nums, 0, nums.length - 1).mSum;
    }

    public Status getInfo(int[] a, int l, int r) {
        if (l == r) {
            return new Status(a[l], a[l], a[l], a[l]);
        }
        int m = (l + r) >> 1;//中位数(两数相加除以二)
        Status lSub = getInfo(a, l, m);
        Status rSub = getInfo(a, m + 1, r);
        return pushUp(lSub, rSub);
    }

    public Status pushUp(Status l, Status r) {
        int iSum = l.iSum + r.iSum;
        int lSum = Math.max(l.lSum, l.iSum + r.lSum);
        int rSum = Math.max(r.rSum, r.iSum + l.rSum);
        int mSum = Math.max(Math.max(l.mSum, r.mSum), l.rSum + r.lSum);
        return new Status(lSum, rSum, mSum, iSum);
    }

}
