package byAspects.easy.easy_no2;

/**
 * 70,爬楼梯
 * 一次只能爬一阶或者2阶楼梯
 * 计算爬n阶楼梯可以有多少种方式
 */
public class ClimbStairs {
    public static void main(String[] args) {
        ClimbStairs c = new ClimbStairs();
        System.out.println(c.climbStairs(45));
        System.out.println(c.queueCal(5,2));
    }

    public int climbStairs(int n) {
        if (n == 0)
            return 0;
        int ans = 0;
        int steps = n % 2 == 0 ? n/2 : n/2 +1;
        for (int i = n,stepsOf2 = 0; i >= steps; --i,++stepsOf2) {
            ans += cnm(i,stepsOf2);
        }
        return ans;
    }

    /**
     * 计算A n取m的排列
     * @param n
     * @param m
     * @return
     */
    public long queueCal(int n,int m){
        long res = 1;
        for (int i = n; i > n-m ; --i) {
            res = res * i;
        }
        return res;
    }

    /**
     * 求组和数优化代码
     * @param n
     * @param m
     * @return
     */
    public long cnm(int n,int m){
        long s = 1;
        int k = 1;
        if(m > n/2)
            m = n-m;
        for(int i=n-m+1;i<=n;i++) {
            s *= i;
            while(k<=m && s%k == 0) {
                s /= k;
                k++;
            }
        }
        return s;
    }
}
