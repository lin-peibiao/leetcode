package byAspects.easy.easy_no2;

/**
 * 一个存储着非负整数的数组中元素构成的整数加一
 */
public class PlusOne {

    public static void main(String[] args) {
        PlusOne p = new PlusOne();
        int nums[] = new int[]{
                9,9,9,9,9,9,9
        };
        nums = p.plusOne(nums);
        for (int i : nums) {
            System.out.println(i);
        }
    }

    public int[] plusOne(int[] digits) {
        digits = plus(digits,digits.length-1);
        if (digits[0] == 0){
            //就得重建数组啦
            int tem[] = new int[digits.length + 1];
            tem[0] = 1;
            digits = tem;
        }
        return digits;
    }

    public int[] plus(int digite[],int i){
        if (i >= 0 && digite[i] < 9)
            digite[i] += 1;
        else {
            if (i >= 0){
                plus(digite,i-1);
                digite[i] = 0;
            }
        }
        return digite;
    }
}
