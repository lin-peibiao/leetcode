package byAspects.easy.easy_no2;

/**
 * 101、判断二叉树是否对称
 */
public class IsSymmetric {
    public boolean isSymmetric(TreeNode root){
        if (root == null)
            return true;
        return dfs(root.left,root.right);
    }

    private boolean dfs(TreeNode left,TreeNode right){
        if (left == null && right == null)
            return true;
        else if (left == null || right == null)
            return false;

        else{
            if (left.val == right.val)
                return dfs(left.left,right.right) && dfs(left.right,right.left);
            return false;
        }
    }
}
