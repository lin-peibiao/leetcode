package byAspects.easy.easy_no2;

public class DeleteSameEle {
    public ListNode deleteDuplicates(ListNode head) {
        ListNode node = new ListNode(-1,head);
        while(head != null){
            ListNode tem = head.next;
            while (tem != null && tem.val == head.val){
                tem = tem.next;
            }
            head.next = tem;
            head = head.next;
        }

        return node.next;
    }
}

class ListNode {
    int val;
    ListNode next;
    ListNode() {}
    ListNode(int val) { this.val = val; }
    ListNode(int val, ListNode next) { this.val = val; this.next = next; }
}
