package byAspects.easy.easy_no2;

/**
 * 69、实现q求x的2次方根
 */
public class MySqrt {
    public static void main(String[] args) {
        MySqrt m = new MySqrt();
        System.out.println(m.mySqrt(9));
        System.out.println(2147395599);
        System.out.println(Integer.MAX_VALUE);
    }

    public int mySqrt(int x){

        return x == 0 ? 0 : sqrt(x,0);
    }

    public int sqrt(int x,int i){
        if ((i+1) > x/(i+1)){
            return i;
        }

        int j = 1;
        while (i <= (x/2) && i+j < Integer.MAX_VALUE/(i+j)){
            i += j;
            if (i * i < x)
                j = j << 1;
            else if (i * i == x){
                return i;
            }else{
                return sqrt(x,i - j);
            }
        }
        return sqrt(x,i);
    }
}
