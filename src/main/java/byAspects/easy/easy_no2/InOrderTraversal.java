package byAspects.easy.easy_no2;

import java.util.ArrayList;
import java.util.List;

/**
 * 实现二叉树的中序遍历
 */
public class InOrderTraversal {
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        dfs(root,ans);
        return ans;
    }
    public void dfs(TreeNode root,List<Integer> ans){
        if (root == null)
            return;
        dfs(root.left,ans);
        ans.add(root.val);
        dfs(root.right,ans);

        /*
        if (root.left != null){
            dfs(root.left,ans);
            ans.add(root.val);
            dfs(root.right,ans);
        }
        else{
            ans.add(root.val);
            dfs(root.right,ans);
        }
         */
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode() {}
    TreeNode(int val) { this.val = val; }
    TreeNode(int val, TreeNode left, TreeNode right) {
        this.val = val;
        this.left = left;
        this.right = right;
    }
}