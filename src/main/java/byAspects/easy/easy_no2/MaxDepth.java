package byAspects.easy.easy_no2;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 104、求二叉树的最大深度
 */
public class MaxDepth {
    public int maxDepth(TreeNode root){
        return dfs(root,0);
    }


    /**
     * 时间复杂度为O(n)
     * 空间复杂度为O(depth)  因为递归函数需要栈空间
     * @param root
     * @param depth
     * @return
     */
    private int dfs(TreeNode root, int depth) {
        if (root != null){
            return Math.max(dfs(root.left,depth + 1),dfs(root.right,depth + 1));
        }
        return depth;
    }


    /**
     * 广度优先遍历（层次遍历）
     * @param root
     * @return
     */
    public int maxDepth1(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        int ans = 0;
        while (!queue.isEmpty()) {
            int size = queue.size();
            while (size > 0) {
                TreeNode node = queue.poll();
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
                size--;
            }
            ans++;
        }
        return ans;
    }

/*
    作者：LeetCode-Solution
    链接：https://leetcode-cn.com/problems/maximum-depth-of-binary-tree/solution/er-cha-shu-de-zui-da-shen-du-by-leetcode-solution/
    来源：力扣（LeetCode）
    著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

 */
}
