package byAspects.easy.easy_no3;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class YangHuiGenerate {
    public List<List<Integer>> generate(int numRows) {
        List<List<Integer>> res = new ArrayList<>();
        if (numRows == 0)
            return res;
        List<Integer> list = null;

        for (int i = 0; i < numRows; i++) {
            list = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i)
                    list.add(1);
                else{
                    list.add(res.get(i-1).get(j) + res.get(i-1).get(j-1));
                }
            }

            res.add(list);
        }
        return res;
    }

    @Test
    public void test(){
        System.out.println(generate(6));
    }
}
