package byAspects.easy.easy_no3;

import org.junit.Test;

/**
 * 110、判断二叉树是否为高度平衡
 */
public class IsBalanced {


    /**
     * 递归判断每棵子树的长度是否大于1
     * 时间复杂度为 n的平方
     * 空间复杂度为nlogn 的平方
     * @param root
     * @return
     */
    public boolean isBalanced(TreeNode root){
        if (root == null)
            return true;
        if (Math.abs(dfs(root.left,0) - dfs(root.right,0)) <= 1)
            return isBalanced(root.left) && isBalanced(root.right);
        return false;
    }

    /**
     * 获取树的深度
     * @param root
     * @param cruDepth
     * @return
     */
    private int dfs(TreeNode root,int cruDepth){
        if (root == null)
            return cruDepth;
        return Math.max(dfs(root.left,cruDepth+1),dfs(root.right,cruDepth+1));
    }

    /**
     * 官方精选评论解法
     * @param root
     * @return
     */
    public boolean isBalanced1(TreeNode root) {
        return height(root) >= 0;
    }

    /**
     * 自底向上获取子树的高度
     * @param root
     * @return
     */
    private int height(TreeNode root) {
        if(root == null)
            return 0;
        int lh = height(root.left);//左子树高度
        int rh = height(root.right);//右子树高度
        if(lh >= 0 && rh >= 0 && Math.abs(lh - rh) <= 1) {
            return Math.max(lh, rh) + 1;
        } else {
            return -1;
        }
    }

    @Test
    public void test(){
        TreeNode root = new TreeNode();
        root.left = new TreeNode();
        root.right = new TreeNode();
        root.right.right = new TreeNode();
        root.right.right.right = new TreeNode();
        root.right.right.right.right = new TreeNode();
        System.out.println(isBalanced(root));
    }
}
