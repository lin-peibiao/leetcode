package byAspects.easy.easy_no3;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 119、杨辉三角2
 * 返回杨辉三角的第n行
 *
 * @author xiaohu
 */
public class YangHuiGenerate2 {
    public List<Integer> generate(int numRows) {
        List<List<Integer>> res = new ArrayList<>();
        if (numRows < 0)
            return null;
        List<Integer> list = null;


        //虽然空间复杂度也是O（n）但是顺序表没有数组来的快，所以执行时间比数组多一毫秒
        for (int i = 0; i <= numRows; i++) {
            list = new ArrayList<>();
            for (int j = 0; j <= i; j++) {
                if (j == 0 || j == i)
                    list.add(1);
                else{
                    list.add(res.get(i-1).get(j) + res.get(i-1).get(j-1));
                }
            }

            res.add(list);
        }
        return list;
    }

    public List<Integer> getRow(int rowIndex){
        Integer[] dp = new Integer[rowIndex + 1];
        //又学习了集合的两个API
        Arrays.fill(dp,1);
        for(int i = 2;i < dp.length;i++){
            for(int j = i - 1;j > 0;j--)
                //从后往前
                dp[j] = dp[j] + dp[j - 1];
        }
        //将数组转换成List集合
        List<Integer> res = Arrays.asList(dp);
        return res;
    }


    @Test
    public void test(){
        for (int i : getRow(3)) {
            System.out.println(i);
        }
    }

}
