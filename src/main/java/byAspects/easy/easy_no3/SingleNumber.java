package byAspects.easy.easy_no3;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * 136、返回只出现一次的数字
 *
 * @author xiaohu
 */
public class SingleNumber {

    /**
     * 使用异或
     * 任何数与0异或得到任何数,由于其他的数字都有两个，所以异或后可以抵消，最后只剩下一个单身狗。
     * @param nums
     * @return
     */
    public int singleNumber1(int nums[]){
        int res = 0;
        for (int i = 1; i < nums.length; i++) {
            res = res ^ nums[i];
        }
        return res;
    }


    /**
     * 这应该不算使用额外的空间吧
     * 集合算吗
     * @param nums
     * @return
     */
    public int singleNumber(int nums[]){
        Set<Integer> list = new HashSet<>();
        for (int i = 0; i < nums.length; i++) {
            if (list.contains(nums[i]))
                list.remove(nums[i]);
            else
                list.add(nums[i]);
        }
        return list.iterator().next();
    }

    @Test
    public void test(){
        int nums[] = new int[]{2,1,2,3,1,2};
        System.out.println(singleNumber1(nums));
    }
}
