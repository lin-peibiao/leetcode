package byAspects.easy.easy_no3;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 111、求二叉树的最小高度
 */
public class MinDepth {

    public int minDepth(TreeNode root){
        if (root == null)
            return 0;
        return dfs(root,1);
    }

    /**
     * 获取树最小深度
     * @param root
     * @param cruDepth
     * @return
     */
    private int dfs(TreeNode root,int cruDepth){
        if (root == null || (root.left == null && root.right == null))
            return cruDepth;
        return Math.min(dfs(root.left,cruDepth+1),dfs(root.right,cruDepth+1));
    }


    /**
     * 层次遍历，只需要找到第一个叶子节点
     * @param root
     * @return
     */
    private int bsf(TreeNode root){
        if (root == null) {
            return 0;
        }

        int curDepth = 1;
        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> level = new ArrayList<>();
            int currentLevelSize = queue.size();
            for (int i = 1; i <= currentLevelSize; ++i) {
                TreeNode node = queue.poll();

                //在此处判断，如果左右子树为空，直接返回depth
                if (node.left == null && node.right == null)
                    return curDepth;

                level.add(node.val);
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            ++curDepth;
        }
        return curDepth;
    }
}
