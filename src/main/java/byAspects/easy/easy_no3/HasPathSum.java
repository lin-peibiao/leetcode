package byAspects.easy.easy_no3;

public class HasPathSum {

    /**
     * 官方精炼题解  思路和我的一样是递归
     * 时间复杂度：O(N)，其中 NN 是树的节点数。对每个节点访问一次。
     * 空间复杂度：O(H)，其中 HH 是树的高度。空间复杂度主要取决于递归时栈空间的开销，最坏情况下，树呈现链状，空间复杂度为 O(N)。平均情况下树的高度与节点数的对数正相关，空间复杂度为 O(logN)。
     *
     * 作者：LeetCode-Solution
     * 链接：https://leetcode-cn.com/problems/path-sum/solution/lu-jing-zong-he-by-leetcode-solution/
     * 来源：力扣（LeetCode）
     * 著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
     * @param root
     * @param sum
     * @return
     */
    public boolean hasPathSum1(TreeNode root,int sum){
        if (root == null) {
            return false;
        }
        if (root.left == null && root.right == null) {
            return sum == root.val;
        }
        return hasPathSum1(root.left, sum - root.val) || hasPathSum1(root.right, sum - root.val);
    }



    public boolean hasPathSum(TreeNode root, int targetSum) {
        if (root == null)
            return false;
        return hasSubPath(root,targetSum,0);
    }

    private boolean hasSubPath(TreeNode root,int targetSum,int curSum){
        if (isLeaf(root) && (curSum + root.val == targetSum))
            return true;
        if (root != null) {
            curSum += root.val;
            return hasSubPath(root.left, targetSum, curSum) || hasSubPath(root.right, targetSum, curSum);
        }
        return false;
    }


    /**
     * 判断是否为叶子节点
     * @param node
     * @return
     */
    private boolean isLeaf(TreeNode node){
        if (node != null && node.left == null && node.right == null)
            return true;
        return false;
    }
}
