package byAspects.easy.easy_no3;

import org.junit.Test;

public class MaxProfit {
    public int maxProfit(int prices[]){
        if (prices.length == 0)
            return 0;
        int max = 0;
        int in = 0,out = 1;
        while(in < out && out < prices.length){
            if (prices[in] > prices[out])
                in = out;
            else
                max = Math.max(max,prices[out]-prices[in]);
            ++out;
        }
        return max;
    }


    /**
     * 官方题解，同样是遍历一次，官方题解只运行了一毫秒，我自己的要运行2毫秒
     * @param prices
     * @return
     */
    public int maxProfit1(int prices[]){
        int minprice = Integer.MAX_VALUE;
        int maxprofit = 0;
        for (int i = 0; i < prices.length; i++) {
            if (prices[i] < minprice) {
                minprice = prices[i];
            } else if (prices[i] - minprice > maxprofit) {
                maxprofit = prices[i] - minprice;
            }
        }
        return maxprofit;
    }

    @Test
    public void test(){
        int p[] = new int[]{7,8,5,3,4,9};
        System.out.println(maxProfit(p));
    }

}
