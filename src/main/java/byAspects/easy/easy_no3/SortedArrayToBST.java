package byAspects.easy.easy_no3;

/**
 * 108、将有序数组转换为二叉搜索树
 * 还要高度平衡
 */
public class SortedArrayToBST {

    public TreeNode sortedArrayToBST(int nums[]){
        if (nums.length == 0)
            return null;
        return buildTree(nums,0,nums.length-1);
    }

    /**
     * 分治
     * @param nums
     * @param left
     * @param right
     * @return
     */
    private TreeNode buildTree(int[] nums, int left, int right) {
        if (left > right)
            return null;
        int _root = (left + right) >> 1;
        TreeNode root = new TreeNode(nums[_root]);
        root.left = buildTree(nums,left,_root - 1);
        root.right = buildTree(nums,_root + 1,right);

        return root;
    }
}
