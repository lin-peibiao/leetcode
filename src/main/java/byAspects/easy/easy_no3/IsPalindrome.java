package byAspects.easy.easy_no3;


import org.junit.Test;

import static junit.framework.TestCase.assertTrue;


/**
 * 125、判断带特殊字符忽略大小写的回文串
 */
public class IsPalindrome {
    public boolean isPalindrome(String s) {
        if(s.length() == 0)
            return true;
        //将字符串去除空格，统一转换为小写字母
        s = s.replaceAll(" ","");
        s = s.toLowerCase();
        int left = 0, right = s.length()-1;
        while(left < right){
            if (!isDigitOrChar(s.charAt(left))){
                ++left;
                continue;
            }

            if (!isDigitOrChar(s.charAt(right))){
                --right;
                continue;
            }

            if (Character.toLowerCase(s.charAt(left)) != Character.toLowerCase(s.charAt(right)))
                return false;
            else{
                ++left;
                --right;
            }
        }
        return true;
    }

    /**
     * 判断字符是否为数字或字母
     * @param c
     * @return
     */
    private boolean isDigitOrChar(char c){

        return Character.isDigit(c) || (((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')));
    }

    /**
     * 官方简洁版
     * @param s
     * @return
     */
    public boolean isPalindrome1(String s) {
        int n = s.length();
        int left = 0, right = n - 1;
        while (left < right) {
            while (left < right && !Character.isLetterOrDigit(s.charAt(left))) {
                ++left;
            }
            while (left < right && !Character.isLetterOrDigit(s.charAt(right))) {
                --right;
            }
            if (left < right) {
                if (Character.toLowerCase(s.charAt(left)) != Character.toLowerCase(s.charAt(right))) {
                    return false;
                }
                ++left;
                --right;
            }
        }
        return true;
    }

    @Test
    public void test(){
        assertTrue(isPalindrome("A man, a plan, a canal: Panama"));
//        assertFalse(isPalindrome("aabcdcbaa"));
    }
}
