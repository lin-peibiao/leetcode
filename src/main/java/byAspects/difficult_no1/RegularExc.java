package byAspects.difficult_no1;

/**
 * 定义正则表达式规则
 * “.”,"*"这两个正则表达式的符号
 */
public class RegularExc {
    public static void main(String[] args) {
        System.out.println(isMatch("abbbcbbc", ".b*c*"));
    }

    /**
     * 判断正则规则是否正确
     * @param s 给定的字符串
     * @param p 给定的正则表达式规则
     * @return 正确返回true 否则返回false
     */
    public static boolean isMatch(String s, String p) {
        p = p + "Ahhhhhhh";
        boolean isMatch = true;
        if(".*".equals(p))
            return true;

        //获取p中每一个字符和s对比
        int i = 0,j = 0;
        char sc;
        char pc = p.charAt(j);
        while(i < s.length()){
            sc = s.charAt(i++);

            if(sc != pc){
                if(pc == '.'){
                    pc = p.charAt(j++);
                    continue;
                }
                else if(pc == '*'){
                    if(sc == p.charAt(j-1)){
                        continue;
                    }else if(sc == p.charAt(j+1)){
                        pc = p.charAt(j+2);
                        j++;
                        continue;
                    }
                }
                else{
                    isMatch = false;
                    break;
                }
            }
            pc = p.charAt(j++);


        }

        return isMatch;
    }
}
