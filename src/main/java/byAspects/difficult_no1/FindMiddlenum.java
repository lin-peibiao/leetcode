package byAspects.difficult_no1;

import java.util.Arrays;

/**
 * 找到两个整数数组的中位数
 * 时间复杂度位O(log(m+n))
 * 其中m和n是两个数组的大小
 */
public class FindMiddlenum {
    public static void main(String[] args) {
        int nums1[] = new int[]{1,3};
        int nums2[] = new int[]{2};
        System.out.println(findMedianSortedArrays(nums1, nums2));
    }

    public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
//        int bigLength = nums1.length > nums2.length ? nums1.length : nums2.length;
        int length = nums1.length + nums2.length;
        int nums3[] = new int[length];

        //先数组合并
        for (int i = 0; i < nums1.length ;i++) {
            nums3[i] = nums1[i];
        }

        for (int i = nums1.length,j = 0; j<nums2.length ;i++,j++) {
            nums3[i] = nums2[j];
        }

        //使用归并排序
        int nums[] = mergeSort(nums3);
        double midNum = nums.length % 2 == 0 ? (double)(nums[nums.length / 2 - 1] + nums[nums.length / 2]) / 2 : nums[nums.length / 2 ];

        return midNum;
    }

    /**
     * 2路归并算法
     * @param array
     * @return
     */
    public static int[] mergeSort(int[] array){
        if(array.length < 2){
            return array;
        }
        int mid = array.length /2;
        int[] left = Arrays.copyOfRange(array, 0, mid);
        int[] right = Arrays.copyOfRange(array, mid, array.length);
        return merge(mergeSort(left),mergeSort(right));
    }

    public static int[] merge(int[] left,int[] right){
        int[] result = new int[left.length + right.length];
        for(int index = 0,i = 0, j = 0;index < result.length;index++){
            if(i >= left.length){
                result[index] = right[j++];
            }else if(j >= right.length){
                result[index] = left[i++];
            }else if(left[i] > right[j]){
                result[index] = right[j++];
            }else{
                result[index] = left[i++];
            }
        }
        return result;

    }
}
