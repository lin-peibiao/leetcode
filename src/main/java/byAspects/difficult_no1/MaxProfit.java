package byAspects.difficult_no1;

import org.junit.Test;

/**
 * 121、买卖股票的最佳时机3
 * 只能买卖股票两次，且一天只能只能持有两股
 */
public class MaxProfit {

    /*
    执行结果：
    解答错误
            显示详情
    添加备注

    通过测试用例：
            147 / 214
    输入：
            [6,1,3,2,4,7]
    输出：
            6
    预期结果：
            7

     */

    /**
     * 这个题解倒在了以上这个案例，实在想不出应该怎么改进
     * @param prices
     * @return
     */
    public int maxProfit(int prices[]){
        int len = prices.length;
        if (len == 0)
            return 0;
        int profit[] = new int[3];
        int in = 0, out = 1;
        while (in < out && out < len){
            if (prices[in] > prices[out]){
                in = out;
                profit[2] = profit[0];
                profit[0] = profit[1];
            }
            else{
                profit[1] = profit[2];
                profit[0] = Math.max(profit[0],prices[out]-prices[in]);
            }

            ++out;
        }

        return profit[0] + profit[2];
    }

    /**
     * 官方题解，动态规划
     * @param prices
     * @return
     */
    public int maxProfit1(int[] prices) {
        int n = prices.length;
        int buy1 = -prices[0], sell1 = 0;
        int buy2 = -prices[0], sell2 = 0;
        for (int i = 1; i < n; ++i) {
            buy1 = Math.max(buy1, -prices[i]);
            sell1 = Math.max(sell1, buy1 + prices[i]);
            buy2 = Math.max(buy2, sell1 - prices[i]);
            sell2 = Math.max(sell2, buy2 + prices[i]);
        }
        return sell2;
    }

    @Test
    public void test(){
        int p[] = new int[]{2,4,1};
        System.out.println(maxProfit(p));
    }
}
