package test;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * @author xiaohu
 * @date 2023/07/14/ 21:04
 * @description 扑克逆向思维
 */
public class PokerGame {
    public static void main(String[] args) {
        Deque<Integer> queue = new ArrayDeque<>(13);
        Deque<Integer> res = new ArrayDeque<>(13);
        for (int i = 13; i >= 1; i--) {
            queue.offer(i);
        }

        // 放在队列的头部，弹出队列的时候会第一个出来。
        // queue.offerFirst(0);


        for (int i = 0; i < 13; i++) {
            res.offer(queue.poll());
            if (!res.isEmpty()){
                res.offer(res.poll());
            }
        }
        for (Integer re : res) {
            System.out.println(re);
        }

    }

}
