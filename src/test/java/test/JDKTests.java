package test;

import org.junit.Test;

import java.util.*;

/**
 * @author xiaohu
 * @date 2022/10/25/ 11:25
 * @description
 */
public class JDKTests {

    @Test
    public void test2() {
        HashSet<Integer> set = new HashSet<>();
        StringBuilder sb = new StringBuilder();
    }

    @Test
    public void test() {
        Deque<int[]> deque = new LinkedList<>();
        deque.add(new int[]{1, 2});

        deque.poll();
        int dx[] = {1, -1, 0, 0};
        int dy[] = {0, 0, 1, -1};
        Map<Integer, Integer> map = new HashMap<>();
        String s = "sas";
        final char[] chars = s.toCharArray();
        if (Character.isDigit(chars[0])) {

        }
        Set<String> set = new HashSet<>();
        int[] last = new int[128];
        String s2 = "1.11ASAS";
        s2.toLowerCase();
        System.out.println(s2 + "小写");
        s2.split("\\.");
        final String s1 = s2.replaceAll("\\.", "");
        System.out.println(s1);
        int version = Integer.valueOf(s1);
        System.out.println(version);
        /**
         * 1.1.1
         */

        System.out.println('.' - 'a');
    }

    @Test
    public void test3() {
        List<String> list = new ArrayList();
        list.addAll(Arrays.asList("a", "ss", "ass"));
        Deque de = new LinkedList();
        StringBuilder sb = new StringBuilder();
        int[][] arr = new int[8][2];
        Arrays.sort(arr, (a, b) -> b[1] - a[1]);
        String[] words = list.toString().split(",");
        System.out.println(words);
    }

    @Test
    public void test4() {
        String s = "you are a cure boy? ohh, no, i am not!";
        System.out.println(s.indexOf("zzz"));
    }

    @Test
    public void test5() {
        String s1 = new String("a") + new String("b");
        String s2 = "ab";
        s2.contains("" + 's');
        s2.contains("sds");
        s1.intern();// 串池中已经有字符串对象了就不会再入池了，所以不会将地址引用返回给s1,调换位置的话就是真
        System.out.println(s1 == s2);// false
    }

    @Test
    public void test6() {
        int[] nums = new int[10];
        Arrays.sort(nums);
        Arrays.stream(nums).max().getAsInt();
        PriorityQueue<Integer> pq = new PriorityQueue<Integer>((a, b) -> b - a);
    }

    @Test
    public void arrayTest() {
        System.out.println(Math.ceil((double) 10 / 3));
        int[][] arr = new int[2][3];
        arr[0][0] = 1;
        arr[0][1] = 2;
        arr[0][2] = 4;
        arr[1][0] = 3;
        arr[1][1] = 3;
        arr[1][2] = 1;
        PriorityQueue<int[][]> queue = new PriorityQueue<>((a, b) -> (a[0][0] - b[0][0]));
        queue.offer(arr);
        int[][] a = queue.poll();
        System.out.println(a[0][1]);
        StringBuilder sb = new StringBuilder();
    }
}
