package test;

import java.util.Scanner;
/**
 * @author xiaohu
 * @date 2023/03/28/ 20:22
 * @description
 */

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        while (in.hasNext()){
            String input = in.next();
            int[] count  = new int[5];
            boolean flag = false;
            for (char c : input.toCharArray()){
                if (c == 'B'){
                    count[0] ++;
                }else if (c == 'a'){
                    count[1] ++;
                }else if (c == 'i'){
                    count[2] ++;
                }else if (c == 'd'){
                    count[3] ++;
                }else if (c == 'u'){
                    count[4] ++;
                }
            }
            for (int i : count){
                if (i <= 0){
                    flag = true;
                    System.out.println("No");
                    break;
                }
            }
            if (!flag){
                System.out.println("Yes");
            }

        }
    }
}
