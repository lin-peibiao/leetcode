package test;


import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author xiaohu
 * @date 2023/06/26/ 22:36
 * @description
 */

public class BaoLiWei {
    static Lock lock = new ReentrantLock(true);
    static Condition redC = lock.newCondition();
    static Condition greenC = lock.newCondition();
    static Condition yellowC = lock.newCondition();
    static volatile LightState state = LightState.RED;
    static volatile Integer status = 0;

    public static void main(String[] args) throws InterruptedException {
        Thread tRed = new Thread(new Red());
        Thread tYellow = new Thread(new Yellow());
        Thread tGreen = new Thread(new Green());
        tRed.start();
        tGreen.start();
        tYellow.start();
    }

    enum LightState{
        RED,
        GREEN,
        YELLOW
    }

    static class Red implements Runnable {
        @Override
        public void run() {
            while (true) {
                lock.lock();
                try {
                    // 红灯
                    System.out.println("亮红灯");
                    Thread.sleep(2000);
                    greenC.signal();
                    redC.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        }
    }

    static class Yellow implements Runnable {

        @Override
        public void run() {
            while (true) {
                lock.lock();
                try {
                    System.out.println("亮黄灯");
                    Thread.sleep(1000);
                    redC.signal();
                    yellowC.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        }
    }

    static class Green implements Runnable {

        @Override
        public void run() {
            while (true) {
                lock.lock();
                try {
                    // 绿灯
                    System.out.println("亮绿灯");
                    Thread.sleep(2000);
                    yellowC.signal();
                    greenC.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    lock.unlock();
                }
            }
        }
    }
}

