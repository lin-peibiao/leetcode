package test;

import java.util.Scanner;

/**
 * @author xiaohu
 * @date 2023/03/28/ 20:38
 * @description
 */
public class Test2 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String s = "red";
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < n; i++){
            res.append(s.charAt(i % 3));
        }

        System.out.println(res);

    }


}
