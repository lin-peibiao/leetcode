package test;


import java.util.*;

/**
 * @author xiaohu
 * @date 2022/11/13/ 14:31
 * @description
 */
public class Sum {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        scanner.next();
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        List<Map.Entry<Character, Integer>> list = new ArrayList<Map.Entry<Character, Integer>>(map.entrySet());
        //然后通过比较器来实现排序
        Collections.sort(list,new Comparator<Map.Entry<Character, Integer>>() {
            //升序排序
            public int compare(Map.Entry<Character, Integer> o1,
                               Map.Entry<Character, Integer> o2) {
                return o1.getValue().compareTo(o2.getValue());
            }
        });
        double a = Math.pow(20, 22);
        System.out.println(a % 7.0);
        // 求1加到100的和
//        System.out.println(getSum(1, 100));
    }

    public static long getSum(int a, int b){
        int sum = 0;
        for (int i = a; i <= b; i++){
            sum += i;
        }
        int[] arr = new int[12];
        sum = Arrays.stream(arr).sum();
        return sum;


    }
}
