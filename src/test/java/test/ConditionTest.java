package test;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author xiaohu
 * @date 2023/06/27/ 15:30
 * @description
 */
public class ConditionTest {
    static Lock lock = new ReentrantLock();
    static Condition redC = lock.newCondition();
    static Condition greenC = lock.newCondition();

    public static void main(String[] args) {
        new Thread(new Red()).start();
        new Thread(new Green()).start();
    }

    static class Red implements Runnable{

        @Override
        public void run() {
            while(true){
                lock.lock();
                try {
                    // 红灯
                    System.out.println("亮红灯");
                    Thread.sleep(2000);
                    greenC.signal();
                    redC.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    lock.unlock();
                }
            }
        }
    }

    static class Green implements Runnable{

        @Override
        public void run() {
            while(true){
                lock.lock();
                try {
                    // 红灯
                    System.out.println("亮绿灯");
                    Thread.sleep(3000);
                    redC.signal();
                    greenC.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }finally {
                    lock.unlock();
                }
            }
        }
    }
}
