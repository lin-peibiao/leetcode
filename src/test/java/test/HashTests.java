package test;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xiaohu
 * @date 2023/01/16/ 14:56
 * @description
 */
public class HashTests {


    private static StringBuffer transformation(Double number) {
        //定义大写Map
        Map<Integer,String> map = new HashMap<>();
        map.put(0,"零");
        map.put(1,"壹");
        map.put(2,"贰");
        map.put(3,"叄");
        map.put(4,"肆");
        map.put(5,"伍");
        map.put(6,"陆");
        map.put(7,"柒");
        map.put(8,"捌");
        map.put(9,"玖");
        for (String value : map.values()) {
            
        }
        ArrayList<String> list = new ArrayList<>();
        char[] chars = String.valueOf(number).toCharArray();
        int whichNumber = 0;
        for (int i = 0; i < chars.length; i++) {
            //判断是否小数点
            if (!String.valueOf(chars[i]).equals(".")){
                String value = map.get(Integer.valueOf(String.valueOf(chars[i])));
                list.add(value);
            }else {
                //当执行了到这里时就知道小数点在多少位了
                whichNumber = i+1;
            }
        }
        return splicing(list, whichNumber);
    }

    private static StringBuffer splicing(ArrayList<String> list, int whichNumber) {
        //定义每位数对应的范围
        Map<Integer, String> mapValue= new HashMap<>();
        mapValue.put(1,"元");
        mapValue.put(2,"十");
        mapValue.put(3,"百");
        mapValue.put(4,"千");
        mapValue.put(5,"万");
        mapValue.put(6,"十");
        mapValue.put(7,"百");
        mapValue.put(8,"千");
        mapValue.put(9,"亿");
        mapValue.put(10,"十");
        mapValue.put(0,"角");
        mapValue.put(-1,"分");
        mapValue.put(-2,"哩");
        StringBuffer stringBuffer = new StringBuffer();
        for (String s : list) {
            //定义小数点的值,每减一次,map的key也会变小
            whichNumber --;
            if (whichNumber < 0) {
                stringBuffer = stringBuffer.append(s).append(mapValue.get(whichNumber));
            }else{
                stringBuffer.append(s).append(mapValue.get(whichNumber));
            }
        }
        return stringBuffer;
    }
}
